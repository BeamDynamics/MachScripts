#!/usr/bin/env python
"""Control of the Storage Ring injection
Usage: srinj.py [options] ON|OFF|QUERY

Options:
--version               show program's version number and exit
-h, --help              show this help message and exit
-f, --ignore-scrapers   Disable scraper motion
-w, --store-settings    Store the present settings when switching OFF
-s, --dont-stop-kickers Keep kickers in STANDBY when switching OFF
"""

from __future__ import print_function  # for python2
from builtins import object  # for python2
import time
import sys
from functools import partial
import numpy as np
from pymach.tango import Device, DeviceGroup, DevState, DevSource, DevFailed
from pymach.devutil import is_on, is_notmoving, all_on, all_notmoving
from pymach.timing import wait_for

__author__ = 'L Farvacque'
__version__ = '1.0'

SCRAPER_LIST = (
    'sr/d-scr/c4-int', 'sr/d-scr/c4-ext', 'sr/d-scr/c5-up',
    'sr/d-scr/c5-low', 'sr/d-scr/c22-up', 'sr/d-scr/c25-up',
    'sr/d-scr/c25-low')
SEPTUM_LIST = (
    'sr/ps-si/12', 'sr/ps-si/3')
CV_LIST = (
    'tl2/ps-c1/cv8', 'tl2/ps-c1/cv9')
BUMP = 'sr/ps-bump/inj'
_SEPTUM_DELAY = 1
_KICKER_DELAY = 3


class SRInj(object):
    kRUNNING = (DevState.ON, DevState.ON, DevState.ON)
    kON = (DevState.STANDBY, DevState.ON, DevState.ON)
    kSTANDBY = (DevState.STANDBY, DevState.OFF, DevState.OFF)
    kOFF = (DevState.OFF, DevState.OFF, DevState.OFF)
    kONBUG = (DevState.ON, DevState.ON, DevState.ON)
    kRUNNINGBUG = (DevState.STANDBY, DevState.ON, DevState.ON)
    #summarize = dict(
    #    ((kRUNNING, DevState.RUNNING), (kON, DevState.ON), (kSTANDBY, DevState.STANDBY),
    #     (kOFF, DevState.OFF), (kONBUG, DevState.ALARM), (kRUNNINGBUG, DevState.ALARM)))
    summarize = dict(
        ((kRUNNING, DevState.RUNNING), (kON, DevState.ON), (kSTANDBY, DevState.STANDBY),
         (kOFF, DevState.OFF)))
    returncode = dict(((DevState.ON, 2), (DevState.STANDBY, 3), (DevState.OFF, 3), (DevState.UNKNOWN, 4)))
    scrclean = dict(((0, ('sr/d-scr/c5-up', 'sr/d-scr/c5-low')), (1, ('sr/d-scr/c25-up', 'sr/d-scr/c25-low')),
                     (2, ('sr/d-scr/c22-up',))))
    machstat = Device('sys/machstat/tango')
    idservers = DeviceGroup('IDs', ['id/id/{0}'.format(n) for n in machstat.get_property('ActiveID')['ActiveID']])

    @staticmethod
    def _try(cmd, *args, **kwargs):
        """
        Executes a command and warns in case of timeout, DevFailed exception or other error
        :param cmd: command to be executed
        :param args: command arguments
        :param kwargs: header for the error message
        :return:
        """
        message = kwargs.pop('message', 'action')
        defval = kwargs.pop('default', None)
        try:
            return cmd(*args, **kwargs)
        except DevFailed as tangoerror:
            print('Error in {0}:'.format(message), file=sys.stderr)
            print(tangoerror[0].desc, file=sys.stderr)
        return defval

    def __init__(self):
        self.bump = Device(BUMP, source=DevSource.DEV)
        self.cv8cv9 = DeviceGroup('cv8cv9', CV_LIST)
        self.magnets = DeviceGroup('magnets', SEPTUM_LIST, self.cv8cv9)
        # noinspection PyUnresolvedReferences
        for d in self.magnets.get_device_list():
            self.magnets.get_device(d).set_source(DevSource.DEV)
        self.scrapers = DeviceGroup('scrapers', SCRAPER_LIST)

    def on(self, move_scrapers=True, start_kickers=True):
        """Start the injection"""
        if move_scrapers:
            self.scrapers_command('Topup')
        if self.bump.state() > DevState.STANDBY:
            print('  Set kickers STANDBY')
            self.bump.Standby()  # danger
        if start_kickers:
            print('  Set septa ON')
            self.magnets.command_inout('On', forward=False)  # danger
            wait_for(partial(all_notmoving, self.scrapers), ini=_SEPTUM_DELAY, timeout=10)
            print('  Set kickers ON')
            self.bump.On()  # danger
        else:
            wait_for(partial(all_notmoving, self.scrapers), timeout=10)
        wait_for(partial(is_notmoving, self.bump), timeout=8)

    def off(self, move_scrapers=True, off_magnets=True, cleaning_scrapers=-1):
        """Stop the injection"""
        print('  Set kickers STANDBY')
        self.bump.Standby()  # danger
        #self.scrapers_delivery(move_scrapers=move_scrapers, cleaning_scrapers=cleaning_scrapers)
        self.scrapers_command('Delivery',move_scrapers=move_scrapers, cleaning_scrapers=cleaning_scrapers)
        print('  Set septa OFF')
        self.magnets.command_inout('Off', forward=False)  # danger
        if off_magnets:
            time.sleep(_KICKER_DELAY)
            print('  Set kickers OFF')
            self.bump.Off()  # danger
        wait_for(partial(is_notmoving, self.bump), timeout=8)

    def query(self):
        """Check the injection state. Return value:
            DevState.ON :
            DevState.STANDBY
            DevState.OFF
            DevState.UNKNOWN : incoherent state
        """
        states = (self.bump.state(),) + self.magnets.state(forward=False)
        state = self.summarize.get(states, DevState.UNKNOWN)
        if state == DevState.UNKNOWN:
            state = max(states)
        return state

    def getstatus(self):
        states = (self.bump.state(),) + self.magnets.state(forward=False)
        names = (self.bump.devname,) + tuple(self.magnets.get_device_list(forward=False))
        state = self.summarize.get(states, DevState.UNKNOWN)
        if state == DevState.UNKNOWN:
            state = max(states)
            statuses = (self.bump.status(),) + self.magnets.status(forward=True)
            detail = ''.join(['{0:>16}: {1} : {2}\n'.format(n, s, st) for n, s, st in zip(names, states,statuses)])
        elif state == DevState.ALARM:
            statuses = (self.bump.status(),) + self.magnets.status(forward=True)
            detail = ''.join(['{0:>16}: {1} : {2}\n'.format(n, s, st) for n, s, st in zip(names, states,statuses)])
        else:
            detail = ''
        status = '{0}SR Injection is {1}'.format(detail, state)
        return state, status

    def scrapers_delivery(self, move_scrapers=True, cleaning_scrapers=-1):
        """Set scrapers to delivery position
        cleaning_scrapers = -1  : all scrapers are moved (default)
                            0   : C5 upp and low are left unchanged for cleaning
                            1   : C25 upp and low are left unchanged for cleaning
                            2   : C22 upp left unchanged for cleaning
        """
        if move_scrapers:
            cleanlist = self.scrclean.get(cleaning_scrapers, ())
            [self.scrapers.disable(n) for n in cleanlist]
            print('  Set scrapers for delivery')
            self.scrapers.command_inout('Delivery')  # danger
            [self.scrapers.enable(n) for n in cleanlist]


    def scrapers_command(self,command, move_scrapers=True, cleaning_scrapers=-1):
        """Set scrapers to delivery position
        cleaning_scrapers = -1  : all scrapers are moved (default)
                            0   : C5 upp and low are left unchanged for cleaning
                            1   : C25 upp and low are left unchanged for cleaning
                            2   : C22 upp left unchanged for cleaning
        """
        if move_scrapers:
            cleanlist = self.scrclean.get(cleaning_scrapers, ())
            [self.scrapers.disable(n) for n in cleanlist]
            print('  Set scrapers for delivery')
            self.scrapers.command_inout(command)  # danger
            [self.scrapers.enable(n) for n in cleanlist]

    def IDMode(self):
        mode = {1:'Delivery',2:'Topup',3:'Injection',4:'Open',0:'Unknown'}
        print(mode.keys())
        modes_index = [mode for mode in self.idservers.attr_value('Mode')]
        print(modes_index)
        state_index = np.amin(modes_index)
        if state_index > 0:
          return mode[state_index]
        else:
          return 'Delivery'
          


class FlushFile(object):
    def __init__(self, f):
        self.f = f

    def __getattr__(self, name):
        return self.f.getattr(name)

    def write(self, x):
        self.f.write(x)
        self.f.flush()


if __name__ == "__main__":
    # import argparse
    import optparse

    parser = optparse.OptionParser(usage="%prog [options] ON|OFF|QUERY",
                                   version="%prog 1.0", description="Sets the SR injection ON or OFF")
    parser.add_option('-f', '--ignore-scrapers', action="store_false", default=True, dest='move_scrapers',
                      help='Disable scraper motion')
    parser.add_option('-n', '--dont-start-kickers', action="store_false", default=True, dest='start_kickers',
                      help='Leave kickers and septa in STANDBY')
    parser.add_option('-s', '--dont-stop-kickers', action="store_false", default=True, dest='off_magnets',
                      help='Leave kickers in STANDBY')
    options, arguments = parser.parse_args()
    print(options)
    if len(arguments) != 1:
        parser.error("incorrect number of arguments")
    action = arguments[0].upper()
    if action == 'ON':
        SRInj().on(options.move_scrapers, options.start_kickers)
        print('Injection ON')
        code = 0
    elif action == 'OFF':
        SRInj().off(options.move_scrapers, options.off_magnets)
        print('Injection OFF')
        code = 0
    elif action == 'QUERY':
        code = SRInj.returncode.get(SRInj().query(), 4)
    else:
        code = 1
    sys.exit(code)
