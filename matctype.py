from __future__ import print_function
from builtins import chr
from builtins import str
from builtins import range
from builtins import object
import sys
import operator
from ctypes import *
import numpy
from functools import reduce

__author__ = 'L. Farvacque'
suffix = dict(darwin='dylib', linux2='so')[sys.platform]
libmat = CDLL('.'.join(('libmat', suffix)))
libmx = CDLL('.'.join(('libmx', suffix)))

c_mwSize = c_size_t
c_mxClassID = c_uint8
c_mxComplexity = c_uint8
c_mxArray_p = c_void_p

matOpen = libmat.matOpen
matOpen.argtypes = (c_char_p, c_char_p)
matOpen.restype = c_void_p
matClose = libmat.matClose
matClose.argtypes = (c_void_p,)
matClose.restype = c_int
matGetDir = libmat.matGetDir
matGetDir.argtypes = (c_void_p, POINTER(c_int))
matGetDir.restype = POINTER(c_char_p)
matGetVariable = libmat.matGetVariable
matGetVariable.argtypes = (c_void_p, c_char_p)
matGetVariable.restype = c_mxArray_p
matPutVariable = libmat.matPutVariable
matPutVariable.argtypes = (c_void_p, c_char_p, c_mxArray_p)
matPutVariable.restype = c_int

mxGetNumberOfDimensions = libmx.mxGetNumberOfDimensions_730
mxGetNumberOfDimensions.argtypes = (c_mxArray_p,)
mxGetNumberOfDimensions.restype = c_mwSize
mxGetDimensions = libmx.mxGetDimensions_730
mxGetDimensions.argtypes = (c_mxArray_p,)
mxGetDimensions.restype = POINTER(c_mwSize)
mxGetClassID = libmx.mxGetClassID
mxGetClassID.argtypes = (c_mxArray_p,)
mxGetClassID.restype = c_mxClassID
mxGetClassName = libmx.mxGetClassName
mxGetClassName.argtypes = (c_mxArray_p,)
mxGetClassName.restype = c_char_p

mxGetData = libmx.mxGetData
mxGetData.argtypes = (c_mxArray_p,)
mxGetData.restype = c_void_p
mxGetImagData = libmx.mxGetImagData
mxGetImagData.argtypes = (c_mxArray_p,)
mxGetImagData.restype = c_void_p
mxGetLogicals = libmx.mxGetLogicals
mxGetLogicals.argtypes = (c_mxArray_p,)
mxGetLogicals.restype = POINTER(c_uint8)
mxGetChars = libmx.mxGetChars
mxGetChars.argtypes = (c_mxArray_p,)
mxGetChars.restype = POINTER(c_uint16)
mxArrayToString = libmx.mxArrayToString
mxArrayToString.argtypes = (c_mxArray_p,)
mxArrayToString.restype = c_char_p
mxGetCell = libmx.mxGetCell_730
mxGetCell.argtypes = (c_mxArray_p, c_mwSize)
mxGetCell.restype = c_void_p
mxSetCell = libmx.mxSetCell_730
mxSetCell.argtypes = (c_mxArray_p, c_mwSize, c_mxArray_p)
mxSetCell.restype = None

mxCreateNumericArray = libmx.mxCreateNumericArray_730
mxCreateNumericArray.argtypes = [c_mwSize, POINTER(c_mwSize), c_mxClassID, c_mxComplexity]
mxCreateNumericArray.restype = c_mxArray_p
mxCreateLogicalArray = libmx.mxCreateLogicalArray_730
mxCreateLogicalArray.argtypes = [c_mwSize, POINTER(c_mwSize)]
mxCreateLogicalArray.restype = c_mxArray_p
mxCreateCellArray = libmx.mxCreateCellArray_730
mxCreateCellArray.argtypes = [c_mwSize, POINTER(c_mwSize)]
mxCreateCellArray.restype = c_mxArray_p
mxDestroyArray = libmx.mxDestroyArray
mxDestroyArray.argtypes = (c_void_p,)
mxDestroyArray.restype = None


class mxDescr(object):
    def __init__(self, mxID, ctype, tkind, itemsize, mxclass=None):
        self.mxID = mxID
        self.ctype = ctype
        self.tkind = tkind
        self.itemsize = itemsize
        self.mxclass = mxclass


class pyDescr(object):
    def __init__(self, createfunc, *args):
        self.args = args
        self.createfunc = createfunc


class mxArray(object):

    @classmethod
    def getarray(cls, mxarrptr):
        classname = mxGetClassName(mxarrptr)
        mxd = cls.mxdescr[classname]
        return mxd.mxclass(mxarrptr, mxd.ctype, mxd.tkind, mxd.itemsize)

    @classmethod
    def build(cls, obj):
        return cls.pydescr[type(obj)].createfunc(obj)

    def __init__(self, mxarr, ctype, tkind, itemsize):
        self.mxarr = mxarr
        self.ctype = ctype
        self.tkind = tkind
        self.itemsize = itemsize
        ndims = mxGetNumberOfDimensions(self.mxarr)
        mxdims = mxGetDimensions(self.mxarr)
        self.shape = tuple(mxdims[i] for i in range(ndims) if mxdims[i] > 1)
        if len(self.shape) == 0:
            self.shape = (1,)
        self.nitems = reduce(operator.mul, self.shape)
        print(self.shape, self.nitems)


class mxNumericArray(mxArray):
    def __init__(self, mxarr, ctype, tkind, itemsize):
        super(mxNumericArray, self).__init__(mxarr, ctype, tkind, itemsize)
        rdata = mxGetData(mxarr)
        s = []
        w = itemsize
        for i in range(len(self.shape)):
            s.append(w)
            w *= self.shape[i]
        self.__array_interface__ = dict(shape=self.shape, typestr=self.tkind+str(self.itemsize),
                                        data=(rdata, True), strides=tuple(s), version=3)

    @property
    def obj(self):
        if self.nitems > 1:
            return numpy.array(self)
        else:
            return numpy.array(self)[0]


class mxNumericArray2(mxArray):
    @property
    def obj(self):
        mxGetData = libmx.mxGetData
        mxGetData.argtypes = (c_void_p,)
        mxGetData.restype = self.ctype
        rdata = mxGetData(self.mxarr)
        obj = numpy.empty(self.shape, dtype=self.tkind+str(self.itemsize), order='F')
        obj1 = numpy.ravel(obj, order='A')
        for i in range(self.nitems):
            obj1[i] = rdata[i]
        if self.nitems <= 1:
            obj = obj[0]
        return obj


class mxLogicalArray(mxArray):
    @property
    def obj(self):
        data = mxGetLogicals(self.mxarr)
        obj = numpy.empty(self.shape, dtype=self.tkind+str(self.itemsize), order='F')
        obj1 = numpy.ravel(obj, order='A')
        for i in range(self.nitems):
            obj1[i] = data[i]
        if self.nitems <= 1:
            obj = obj[0]
        return obj


class mxCharArray(mxArray):
    @property
    def obj(self):
        data = mxGetChars(self.mxarr)
        obj = u''
        for i in range(self.nitems):
            obj += chr(data[i])
        return obj


class mxCellArray(mxArray):

    @property
    def obj(self):
        obj = numpy.empty(self.shape, dtype=self.tkind, order='F')
        obj1 = numpy.ravel(obj, order='A')
        for i in range(self.nitems):
            mxarrptr = mxGetCell(self.mxarr, i)
            if mxarrptr is not None:
                obj1[i] = mxArray.getarray(mxarrptr).obj
            else:
                obj1[i] = None
        return obj

mxArray.mxdescr = dict(
    cell=mxDescr(1, c_void_p, 'O', 0, mxCellArray),
    struct=mxDescr(2, c_void_p, 'O', 0),
    logical=mxDescr(3, POINTER(c_uint8), 'b', 1, mxLogicalArray),
    char=mxDescr(4, POINTER(c_uint16), 'S', 2, mxCharArray),
    double=mxDescr(6, POINTER(c_double), 'f', 8, mxNumericArray2),
    single=mxDescr(7, POINTER(c_float), 'f', 4, mxNumericArray2),
    int8=mxDescr(8, POINTER(c_int8), 'i', 1, mxNumericArray2),
    uint8=mxDescr(9, POINTER(c_uint8), 'u', 1, mxNumericArray2),
    int16=mxDescr(10, POINTER(c_int16), 'i', 2, mxNumericArray2),
    uint16=mxDescr(11, POINTER(c_uint16), 'u', 2, mxNumericArray2),
    int32=mxDescr(12, POINTER(c_int32), 'i', 4, mxNumericArray2),
    uint32=mxDescr(13, POINTER(c_uint32), 'u', 4, mxNumericArray2),
    int64=mxDescr(14, POINTER(c_int64), 'i', 8, mxNumericArray2),
    uint64=mxDescr(15, POINTER(c_uint64), 'u', 8, mxNumericArray2),
    )

numpydict = dict((
    (0, 'logical'),  # bool
    (1, 'int8'),     # int8
    (2, 'uint8'),    # uint8
    (3, 'int16'),    # int16
    (4, 'uint16'),   # uint16
    (5, 'int32'),    # int32
    (6, 'uint32'),   # uint32
    (7, 'int64'),    # int64
    (8, 'uint64'),   # uint64
    (11, 'single'),  # single
    (12, 'double'),  # double
))


def ndarray_to_mx(obj):
    mxd = mxArray.mxdescr[numpydict[obj.dtype.num]]
    mxID = mxd.mxID
    ndims = len(obj.shape)
    dims = (c_mwSize*ndims)()
    for i in range(ndims):
        dims[i] = obj.shape[i]
    nitems = reduce(operator.mul, dims)
    mxarr = mxCreateNumericArray(ndims, dims, mxID, 0)
    mxGetData = libmx.mxGetData
    mxGetData.argtypes = (c_void_p,)
    mxGetData.restype = mxd.ctype
    rdata = mxGetData(mxarr)
    objF = obj.flatten('F')
    for i in range(nitems):
        rdata[i] = objF[i]
    return mxarr


def float_to_mx(obj):
    mxd = mxArray.mxdescr['double']
    mxID = mxd.mxID
    ndims = 2
    dims = (c_mwSize*ndims)(1, 1)
    mxarr = mxCreateNumericArray(ndims, dims, mxID, 0)
    mxGetData = libmx.mxGetData
    mxGetData.argtypes = (c_void_p,)
    mxGetData.restype = POINTER(c_double)
    rdata = mxGetData(mxarr)
    rdata[0] = obj
    return mxarr


def logical_to_mx(obj):
    ndims = 2
    dims = (c_mwSize*ndims)(1, 1)
    mxarr = mxCreateLogicalArray(ndims, dims)
    rdata = mxGetLogicals(mxarr)
    rdata[0] = obj
    return mxarr


def list_to_mx(obj):
    nitems = len(obj)
    ndims = 2
    dims = (c_mwSize*ndims)(1, nitems)
    mxarr = mxCreateCellArray(ndims, dims)
    for i in range(nitems):
        print(mxArray.build(obj[i]))
        # mxSetCell(mxpointer, i, mxArray.fromobject(obj[i]))
    return mxarr

mxArray.pydescr = dict((
    (float, pyDescr(float_to_mx)),
    (bool, pyDescr(logical_to_mx)),
    (numpy.ndarray, pyDescr(ndarray_to_mx)),
    (list, pyDescr(list_to_mx))
))


class MatFile(object):
    _oned = dict(column=1, row=2)
    _matversion = {'4': 'w4', '5': 'w'}

    def __init__(self, fname, mode='r', oned_as='column', appendmat=True):
        """Open a .mat file.
        mode may be 'r', 'u', 'w', 'w4', 'wz', 'w7.3'
        """
        if appendmat and not fname.endswith('.mat'):
            fname += '.mat'
        self.fname = fname
        self.oned_as = oned_as
        self.variables = set()
        self.mfile = None
        self.open(mode)

    def __del__(self):
        self.close()

    def __iter__(self):
        """x.__iter__() <==> iter(x)"""
        return self.variables.__iter__()

    def __contains__(self, key):
        """x.__contains__(k) -> True if x has a key k, else False"""
        return self.variables.__contains__(key)

    def __getitem__(self, key):
        """x.__getitem__(y) <==> x[y]"""
        # return _matfile.matread(self.mfile, key)
        mxarr = self.getmx(key)
        return mxarr.obj

    def __setitem__(self, item, value):
        """x.__setitem__(i, y) <==> x[i]=y"""
        self.update(((item, value),))

    def __delitem__(self, item):
        """x.__delitem__(y) <==> del x[y]"""
        # _matfile.matdelete(self.mfile, item)
        self.variables.remove(item)

    def getmx(self, key):
        mxarrptr = matGetVariable(self.mfile, key)
        if mxarrptr is not None:
            return mxArray.getarray(mxarrptr)
        else:
            raise KeyError

    def get(self, *args):
        try:
            return self[args[0]]
        except KeyError:
            if len(args) >= 2:
                return args[1]
            else:
                raise

    def keys(self):
        """x.keys() -> a stp-like object providing a view on D's keys"""
        return set(self)

    def update(self, *args, **kwargs):
        """x.update([E, ]**F) -> None.  Update x from dict/iterable E and F.
        If E is present and has a .keys() method, then does:  for k in E: x[k] = E[k]
        If E is present and lacks a .keys() method, then does:  for k, v in E: x[k] = v
        In either case, this is followed by: for k in F:  x[k] = F[k]
        """
        oned_as = self._oned[kwargs.pop('oned_as', self.oned_as)]
        if len(args) > 1:
            raise TypeError("MatFile.update() takes at most 1 argument ({0} given)".format(len(args)))
        elif len(args) == 1:
            kwargs.update(args[0])
        for key in kwargs:
            matPutVariable(self.mfile, key, mxArray.build(kwargs[key]))
            # _matfile.matwritearray(self.mfile, key, keywords[key], oned_as)
            self.variables.add(key)

    def _iti(self):
        for i in self.variables:
            yield (i, self[i])

    def _ita(self):
        for i in self.variables:
            yield self[i]

    def iterkeys(self):
        """x.iterkeys() -> an iterator over the keys of x"""
        return self.variables.__iter__()

    def iteritems(self):
        """x.iteritems() -> an iterator over the (key, value) items of x"""
        return self._iti()

    def itervalues(self):
        """x.itervalues() -> an iterator over the values of x"""
        return self._ita()

    def open(self, mode='r'):
        self.mfile = matOpen(self.fname, mode)
        nvars = c_int()
        varnames = matGetDir(self.mfile, byref(nvars))
        for i in range(nvars.value):
            self.variables.add(str(varnames[i]))

    def close(self):
        if self.mfile is not None:
            ok = matClose(self.mfile)
            print('close', self.fname, ':', ok)
        self.mfile = None
        self.variables.clear()


def loadmat(file_name, m_dict=None, appendmat=True):
    """loadmat(file_name, mdict=None, appendmat=True, **keywords)
    Load Matlab(tm) file

    file_name : string
       Name of the mat file (do not need .mat extension if
       appendmat==True) If name not a full path name, search for the
       file on the sys.path list and use the first one found (the
       current directory is searched first).  Can also pass open
       file-like object
    m_dict : dict, optional
        dictionary in which to insert matfile variables
    appendmat : {True, False} optional
       True to append the .mat extension to the end of the given
       filename, if not already present
    """
    m = MatFile(file_name, appendmat=appendmat)
    if m_dict is None:
        return dict(iter(m.items()))
    else:
        m_dict.update(m)


def savemat(file_name, mdict, appendmat=True, format='5', do_compression=False, oned_as='column', **kwargs):
    """Save a dictionary of names and arrays into a MATLAB-style .mat file.

    This saves the array objects in the given dictionary to a MATLAB-
    style .mat file.

    Parameters
    ----------
    file_name : str or file-like object
        Name of the .mat file (.mat extension not needed if ``appendmat ==
        True``).
        Can also pass open file_like object.
    mdict : dict
        Dictionary from which to save matfile variables.
    appendmat : bool, optional
        True (the default) to append the .mat extension to the end of the
        given filename, if not already present.
    format : {'5', '4'}, string, optional
        '5' (the default) for MATLAB 5 and up (to 7.2),
        '4' for MATLAB 4 .mat files
    long_field_names : bool, optional
        False (the default) - maximum field name length in a structure is
        31 characters which is the documented maximum length.
        True - maximum field name length in a structure is 63 characters
        which works for MATLAB 7.6+
    do_compression : bool, optional
        Whether or not to compress matrices on write.  Default is False.
    oned_as : {'row', 'column'}, optional
        If 'column', write 1-D numpy arrays as column vectors.
        If 'row', write 1-D numpy arrays as row vectors.
    """
    mode = MatFile._matversion[format] + 'z'[not do_compression:]
    m = MatFile(file_name, mode=mode, oned_as=oned_as, appendmat=appendmat)
    m.update(mdict, **kwargs)
