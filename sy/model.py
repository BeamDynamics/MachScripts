from __future__ import print_function, division
from atmodel import ATModel


class Model(ATModel):
    """Access to optical values
    
    Available locations:
    
    bpm:   All BPMs
    hbpm:  Horizontal BPMs
    h_bpms:Logical mask (1x75) of horizontal BPMs
    vbpm:  Vertical BPMs
    v_bpms:Logical mask (1x75) of vertical BPMs
    qp:    All quadrupoles
    sx:    All sextpoles
    steerh:    Horizontal steerers
    steerv:    Vertical steerers
    
    Available parameters:
    
    0:     index in the AT structure
    1:     Integrated strength
    2:     theta/2pi
    3:     betax
    4:     alphax
    5:     phix/2pi/nux
    6:     etax
    7:     eta'x
    8:     betaz
    9:     alphaz
    10:    phiz/2pi/nux
    11:    etaz
    12:    eta'z
    
    Default parameters:
    
    [   2         3         5        6      8       10      ]
    [theta/2Pi, betax, Phix/2PiNux, etax, betaz, Phiz/2PiNux]
    """

    def __init__(self, *args):
        """
        m = sy.model()              take the lattice from
            $APPHOME/sy/optics/settings/theory/betamodel.mat

        m = sy.model('opticsname')  take the lattice from
            $APPHOME/sy/optics/settings/opticsname/betamodel.mat

        m = sy.model(path)          take the lattice from
            path or path/betamodel.mat or path/betamodel.str
       """
        super(Model, self).__init__(self, *args)
        self.model = self.eng.sy.model(*args)
