from __future__ import print_function, division, unicode_literals   # for python2
import numpy as np
from pymach.tango import Device
from pymach.timing import repeat, TimeoutError


class BpmDevice(Device):
    def __init__(self, bpmname='sy/d-bpmlibera-sp/all', *args, **kwargs):
        super(BpmDevice, self).__init__(bpmname, *args, **kwargs)
        self.mask = np.int32(0x1ff)
        self.averaging = 1
        self.keep = np.ones(75, dtype=bool)

    # noinspection PyPep8Naming
    def set_Disabled(self, value):
        self.keep = np.ones(75, dtype=bool)
        self.keep[value] = False

    # noinspection PyPep8Naming
    def get_Disabled(self):
        return np.flatnonzero(np.logical_not(self.keep))

    Disabled = property(get_Disabled, set_Disabled, None, "List of bpms disabled for DD processing")

    def read_xz(self, averaging=None, wait=False):
        """Gets horizontal and vertical positions, using defaults values for averaging"""
        if wait:
            cnt = self.Fillcounter
        else:
            cnt = 0

        if averaging is None:
            averaging = self.averaging

        x = np.zeros(75)
        z = np.zeros(75)
        nb = np.zeros(75, dtype=int)
        av = 0
        for _ in repeat(timeout=1.2*averaging, period=0.25):
            c = self.FillCounter
            if c > cnt:
                status, xx, zz = self.read_attributes(('All_Status', 'XPosition', 'ZPosition'))
                ok = (np.bitwise_and(status.value, self.mask) == 0)
                x[ok] += xx.value[ok]
                z[ok] += zz.value[ok]
                nb[ok] += 1
                av += 1
                if av >= averaging:
                    break
                cnt = c
        else:
            raise TimeoutError('Time-out waiting for counter')

        with np.errstate(invalid='ignore'):
            # return np.roll(x / nb, -52), np.roll(z / nb, -52)
            return x / nb, z / nb
