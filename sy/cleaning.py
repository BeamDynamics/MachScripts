from __future__ import print_function, division, unicode_literals   # for python2
import numpy as np
import PyTaco
from pymach.tango import Device
from pymach.timing import wait_for


class CleaningDevice(Device):
    trig_delay = 10000

    def __init__(self, sparkname='sy/d-clean/spark', *args, **kwargs):
        super(CleaningDevice, self).__init__(sparkname, *args, **kwargs)
        self.counter = 0
        self.initialize()

    # noinspection PyAttributeOutsideInit
    def initialize(self):

        # Le trigger actuel :
        dev_vpdu = PyTaco.Device('sr/d-tm/timer1')
        dev_vpdu.open(PyTaco.NO_DC)
        dev_vpdu.cmd('DevSetDelay', self.trig_delay)

        # BPM Libera Spark
        self.ADC_Enable = False
        self.AcquisitionDuration = 20.0
        self.TriggerDelay = 0
        self.QSUM_Enable = True
        self.AMPL_enable = False
        self.IQ_Enable = False
        self.Decimation_Enable = True
        self.TDP_AMPL_Enable = False
        self.TDP_XZ_Enable = False
        self.DP_QSUM_Enable = False
        self.SendSoftwareTrigger()

    def check(self, nb_cycles=50):
        def new_trigger():
            new_counter = self.FillCounter
            ok = (new_counter != self.counter)
            if ok:
                self.counter = new_counter
            return ok

        self.counter = self.FillCounter
        data_out = np.empty(nb_cycles)
        for c in range(nb_cycles):
            wait_for(new_trigger, timeout=3.0, period=0.01)
            data_out[c] = self.TBT_Sum.mean()
        return data_out
