#!/usr/bin/env python
"""A module to perform response matrix measurements.
"""
from __future__ import print_function           # for python2
import sys
sys.path.append('/operation/machine/lib/python2.7/site-packages/')   # builtins
from builtins import input, zip, range, object  # for python2
import itertools
from numpy import array, zeros, ones, nan, isfinite, r_, tile, nanmax, size
from scipy.io import savemat
from optparse import OptionParser
from pymach import nanutil
from ebs.bpm import BpmDevice
from time import sleep
from matplotlib.pyplot import figure, draw, ion
import shelve
import re
import os

# sys.path.append('/operation/dserver/python/bliss_modules/debian8/')  # pytango
import PyTango

threshold = 1.e-4
sleeptime = 7
outdir = "."
options = {"n_average": 5, "ignore_incoh": False, "delta_kick": {"H": 0.0001, "V": 0.0001}, "delta_f": 50, "algorithm": 1}
peakcheck = 5
try:
    bpm = BpmDevice('Tango://ebs-simu:10000/srdiag/beam-position/all')
except PyTango.DevFailed as _inst:
    print("\n Warning: Bpm initialization failed:\n")
    print(type(_inst), ":", _inst)


class ActionDev(PyTango.AttributeProxy):
    """Actuator definition for response matrix measurement.
    An actuator is defined by:
    - its name
    - its plane ('H' or 'V')
    - its index along the ring (1 to 288)
    - its current step
    """
    __slots__ = ['num', 'plane', 'delta']

    def __init__(self, machname, plane, num, delta, *args):
        PyTango.AttributeProxy.__init__(self, machname, *args)
        self.plane = plane
        self.num = num
        self.delta = delta

    def ccode(self, plane):
        return 'steer{0}{1:03d}'.format(self.plane if self.plane == plane else self.plane + '2' + plane, self.num)

    def save(self, x, z, iaxz, algo, dest="."):

        def stt(fpath, orbit):
            f = open(os.path.join(dest, fpath), 'w')
            # f.write("%.4f\t%s,%d" % (self.delta,self.xname,algo))
            f.write("{0:.4f}\t{1:s},{2:d}".format(self.delta, self.xname, algo))
            for val in orbit:
                # f.write(isfinite(val) and ("\t%.5f" % (1000.0*val,)) or "\t")
                f.write(isfinite(val) and "\t{0:.5f}".format(1000.0 * val) or "\t")
            f.write("\n")
            f.close()

        xfilename = self.ccode('H')
        zfilename = self.ccode('V')
        stt(xfilename, x)
        stt(zfilename, z)
        data = shelve.open(os.path.join(dest, "iax"))
        data[zfilename] = [self.delta, self.xname, iaxz]
        data.close()

    @property
    def xname(self):
        return '/'.join((self.get_device_proxy().name(), self.name())).upper()


class SteerDev(ActionDev):
    """Steerer definition for response matrix measurement.
    A steerer is defined by:
    - its plane ('H' or 'V')
    - its index along the ring (1 to 288)
    """
    _sxlist = list(zip(tile(array(["sh1","sd1","sf2","sd1","sh2","sd1","sf2","sd1","sh3"]), 32),
                   tile(array(["a", "a", "a", "b", "b", "d", "e", "e", "e"]), 32),
                           r_[4:33, 1:4].repeat(9)))

    def __init__(self, plane, num):
        ActionDev.__init__(self, "Tango://ebs-simu:10000/srmag/{0}st-{1}/C{3:02d}-{2}/Strength".format(
            plane, *(SteerDev._sxlist[num - 1])), plane, num, options["delta_kick"][plane])



class IaxPos(object):
    """Groups all the IAX monitors for global access.
    """
    celllist = ('d01', 'd17', 'd27', 'id07', 'id25')

    @staticmethod
    def testval(devi):
        v = nan
        try:
            attr = devi.read()
            if attr.quality == PyTango.AttrQuality.ATTR_VALID:
                v = attr.value
        except PyTango.DevFailed:
            pass
        return v

    def __init__(self, dest="."):

        # self.dbfile = os.path.join(dest, "iax")
        # data = shelve.open(self.dbfile)
        # if 'iax_names' in data:
        #    device_names = data['iax_names']
        # else:
        #    device_names = ['Tango://ebs-simu:10000/srdiag/emittance/%s/VPosition' % cellnum for cellnum in self.celllist]
        #    data['iax_names'] = device_names
        # data.close()

        device_names = ['Tango://ebs-simu:10000/srdiag/emittance/%s/VPosition' % cellnum for cellnum in self.celllist]

        self.devicelist = [PyTango.AttributeProxy(dname) for dname in device_names]

    @property
    def values(self):
        """Positions on the IAX monitors."""
        return array([self.testval(iax) for iax in self.devicelist])


class IaxEmittance(object):
    """Groups all the IAX monitors for global access.
    """
    # celllist = ('c5', 'c10', 'c11', 'c14', 'c18', 'c21', 'c25', 'c26', 'c29', 'c31', 'c3', 'd9', 'id25', 'd11-lens')
    celllist = ('d01', 'd17', 'd27', 'id07', 'id25')

    @staticmethod
    def testval(devi):
        v = nan
        try:
            attr = devi.read()
            if attr.quality == PyTango.AttrQuality.ATTR_VALID:
                v = attr.value
        except PyTango.DevFailed:
            pass
        return v

    def __init__(self):
        device_names = ['Tango://ebs-simu:10000/srdiag/emittance/%s/Emittance_v' % cellnum for cellnum in self.celllist]
        self.devicelist = [PyTango.AttributeProxy(dname) for dname in device_names]

    @property
    def values(self):
        """Vertical Emittance on the IAX monitors."""
        return array([self.testval(iax) for iax in self.devicelist])

    @property
    def names(self):
        return [iax.get_device_proxy().name() for iax in self.devicelist]

    @property
    def cells(self):
        return array(self.celllist)


class RespError(Exception):
    """Exception raised during a response matrix measurement.
    (nothing added to the base Exception)
    """
    pass


def loadiax(code, dest="."):
    data = shelve.open(os.path.join(dest, "iax"))
    keylist = sorted(s for s in data if s.startswith('steer' + code))
    steer_list = [int(re.match('steer' + code + '(\d+)', key).group(1)) for key in keylist]
    iax_list = [int(re.match('Tango://ebs-simu:10000/srdiag/emittance/(\d+)/VPosition', n.lower()).group(1)) for n in data['iax_names']]
    resp = nan * ones((len(keylist), len(iax_list)))
    curr = nan * ones(len(keylist))
    for ist, res in enumerate([data[key] for key in keylist]):
        resp[ist] = res[2]
        curr[ist] = res[0]
    data.close()
    return {'steer_list': steer_list, 'iax_list': iax_list, 'curr': curr, 'resp': resp}


def generlist(plane, stnum, *args):
    st, pl = zip(*itertools.product(stnum, plane))
    if len(args) > 0:
        st = ((a + b) for b in stnum for a in args[0])
    return zip(itertools.count(), itertools.repeat(size(pl)), pl, st)


def selectlist(mode="default"):
    f = mode.find('[')
    if f < 0:
        f = len(mode)
    code = mode[:f]
    if code == 'foc':
        ll = generlist(['H', 'V'], range(1, 289, 18))
        blk = 4
    elif code == 'skew':
        ll = generlist(['H'], range(1, 289, 9))
        blk = 4
    elif code == 'full':
        ll = generlist(['H', 'V'], range(1, 289))
        blk = 12
    elif code == 'both':
        ll = generlist(['H', 'V', 'H'], range(1, 289, 9), [0, 0, 0])
        blk = 6
    elif code == 'nop':
        ll = []
        blk = 4
    else:
        ll = generlist(['H', 'V'], range(1, 9*2+1, 9))
        blk = 4
    return eval("list(ll)" + mode[f:]), blk


def checkorbit(orbit, reference=None):
    """rms and max deviations from the reference orbit."""
    if reference is not None:
        orbit = orbit - reference
    return isfinite(orbit).sum(), nanutil.nanstd(orbit), nanmax(abs(orbit))


def driftcontrol(bpm, xref, zref):
    print("Checking orbit drift")
    sleep(2)
    x0, z0 = bpm.read_sa(wait=True)
    okh, sth, mxh = checkorbit(x0, xref)
    okv, stv, mxv = checkorbit(z0, zref)
    if (sth > threshold) or (stv > threshold):
        raise RespError("orbit drifted")
    return x0, z0


def measorbit(steerer, bpm, iax, label, algo, *args):
    vini = steerer.read().w_value
    try:
        steerer.write(vini + 0.5 * steerer.delta)
        print("up   %s: setting to %.9g" % (steerer.xname, vini + 0.5 * steerer.delta))
        sleep(sleeptime)
        x1, z1 = bpm.read_sa(wait=True)
        iaxz1 = iax.values
        steerer.write(vini - 0.5 * steerer.delta)
        print("down %s: setting to %.9g" % (steerer.xname, vini - 0.5 * steerer.delta))
        sleep(sleeptime)
        x0, z0 = bpm.read_sa(wait=True)
        iaxz0 = iax.values
    finally:
        steerer.write(vini)
        print("end  %s: setting to %.9g" % (steerer.xname, vini))

    okh, sth, mxh = checkorbit(x1, x0)
    okv, stv, mxv = checkorbit(z1, z0)
    if (okh < 200) or (okv < 200):
        raise RespError("Not enough valid BPMs")
        # if (mxh/sth >peakcheck) or (mxv/stv > peakcheck):
    # print "mxh/sth: %g/%g, mxv/stv: %g/%g" % (mxh,sth,mxv,stv)
    # respfile(steerer,x1-x0,z1-z0,"/machfs/tmp")
    # raise RespError("wrong measurement: peak/std > %g" %(peakcheck,))
    steerer.save(x1 - x0, z1 - z0, iaxz1 - iaxz0, algo, *args)
    print("\t\t\t\t\t\t%s finished (%.2f, %.2f) micro m" % (label, 1.e6 * sth, 1.e6 * stv))

    return x1 - x0, z1 - z0


# noinspection PyStringFormat
def takereference(bpm):
    xref, zref = bpm.read_sa()
    okx, xrms, xpeak = checkorbit(xref)
    okz, zrms, zpeak = checkorbit(zref)
    print("\n H reference orbit: %d active bpms, rms %f mm, peak %f mm" % (okx, 1.e6 * xrms, 1.e6 * xpeak))
    print("\n V reference orbit: %d active bpms, rms %f mm, peak %f mm\n" % (okz, 1.e6 * zrms, 1.e6 * zpeak))
    return xref, zref


steerlist, blocksize = selectlist()


# noinspection PyUnboundLocalVariable
def measloop(stlist=steerlist, bp=bpm, bks=blocksize, *args):
    iax = IaxPos()
    iaxem = IaxEmittance()
    iaxemvals = iaxem.values
    xmax = stlist[0][1]
    fig = figure()
    axh = fig.add_subplot(2, 1, 1)
    hb = axh.bar(range(xmax), zeros(xmax, dtype=float))
    axh.set_xlim((0, xmax))
    axh.set_ylim((0, 350))
    axv = fig.add_subplot(2, 1, 2)
    vb = axv.bar(range(xmax), zeros(xmax, dtype=float))
    axv.set_xlim((0, xmax))
    axv.set_ylim((0, 350))
    draw()
    try:
        algo = bp.CalculationAlgorithm
        if algo != options["algorithm"]:
            raise RespError("BPM algorithm incorrect")
        bp.IgnoreIncoh = options["ignore_incoh"]
        bp.Averaging = options["n_average"]
        xref, zref = takereference(bp)
        frf = ActionDev('Tango://ebs-simu:10000/srrf/master-oscillator/1/Frequency', 'F', 0, options["delta_f"])
        _, _ = measorbit(frf, bp, iax, 'RF frequency', algo, *args)

        for index, steerer in [(idx, SteerDev(plane, num)) for idx, idmax, plane, num in stlist]:
            xresp, zresp = measorbit(steerer, bp, iax, "steerer %i/%i" % (index + 1, xmax), algo, *args)
            okh, sth, mxh = checkorbit(xresp)
            okh, stv, mxv = checkorbit(zresp)
            hb[index].set_height(1.e6 * sth)
            vb[index].set_height(1.e6 * stv)
            draw()
            if not (index + 1) % bks:
                _, _ = driftcontrol(bp, xref, zref)
        try:
            savemat('iaxH2V', loadiax("H2V", *args), oned_as='column')
            savemat('iaxV', loadiax("V", *args), oned_as='column')
        except:
            print("cannot create IAX response .mat files")
        try:
            savemat('iaxemittance', {'iax_list': iaxem.cells, 'iax_values': iaxemvals}, oned_as='column')
        except:
            print("cannot create IAX emittance .mat files")
    except (PyTango.DevFailed, RespError) as inst:
        print('________________exception catched _____________________\n')
        print(type(inst).__name__, ":", inst)
        print('_______________________________________________________')


def main():
    bpm.CalculationAlgorithm = options["algorithm"]
    sleep(2)
    measloop(steerlist, bpm, blocksize, outdir)
    bpm.CalculationAlgorithm = 0


def parseargs():
    parser = OptionParser(usage="usage: %prog [options] foc|skew|both|full|nop\n       %prog foc[6:]")
    parser.add_option("-t", "--threshold", type="float", default=threshold,
                      help="stop threshold for rms orbit drift [%default]")
    parser.add_option("-n", "--n_average", type="int", default=options["n_average"],
                      help="averaging number for bpm readings [%default]")
    parser.add_option("-o", "--out", type="string", default=outdir,
                      help="output directory [%default]")
    parser.add_option("-i", "--ignore_incoh", action="store_true", default=False,
                      help="Ignore BPM incoherency")
    parser.add_option("--h_step", type="float", default=options["delta_kick"]["H"],
                      help="H steerer step [%default rad]")
    parser.add_option("--v_step", type="float", default=options["delta_kick"]["V"],
                      help="V steerer step [%default rad]")
    parser.add_option("--f_step", type="float", default=options["delta_f"],
                      help="frequency step [%default Hz]")
    parser.add_option("-g", "--algorithm", type="int", default=options["algorithm"],
                      help="BPM algorithm [%default]")
    (opts, args) = parser.parse_args()
    strlst, blksze = selectlist(*args[0:1])
    options["delta_i"] = {"H": opts.h_step, "V": opts.v_step}
    options["delta_f"] = opts.f_step
    options["n_average"] = opts.n_average
    options["ignore_incoh"] = opts.ignore_incoh
    options["algorithm"] = opts.algorithm


    return strlst, blksze, opts.threshold, opts.out


if __name__ == "__main__":
    ion()
    steerlist, blocksize, threshold, outdir = parseargs()
    try:
        main()
    except Exception as _inst:
        print('________________exception catched _____________________\n')
        print(type(_inst).__name__, ": ", _inst)
        print('_______________________________________________________')
    resp = input("quit ?")
