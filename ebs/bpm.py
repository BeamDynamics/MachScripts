from __future__ import print_function, division
import sys
# sys.path.append('/operation/dserver/python/bliss_modules/debian8/')
# sys.path.append('/operation/machine/lib/python2.7/site-packages/')
import time
import types
import collections
import numpy as np
import PyTango

from pymach.tango import Device
from pymach.timing import wait_for, TimeoutError


# noinspection PyPep8Naming
class BpmDevice(Device):
    ddattr = dict(h='HPositions', x='HPositions', v='VPositions', z='VPositions', s='Sum_DD')

    @staticmethod
    def oscillation(data, kickturn=None, orbit=None):
        if kickturn is None:
            kickturn = data.shape[data.ndim-1]
        if orbit is None:
            orbit = data[..., :kickturn].mean(axis=data.ndim-1)
        osc = data - orbit[..., np.newaxis]
        return orbit, osc

    def __init__(self, bpmname='Tango://ebs-simu:10000/srdiag/beam-position/all', *args, **kwargs):
        super(BpmDevice, self).__init__(bpmname, *args, **kwargs)
        self.mask = np.int32(0x1ff)
        self.Averaging = 1
        self.keep = np.ones(320, dtype=bool)
        self.trigger = np.zeros(320, dtype=np.int32)

    def _newcnt(self):
        trig = self.All_DD_TriggerCounter
        # noinspection PyTypeChecker
        ok = np.all(trig > self.trigger)
        if ok:
            self.trigger = trig
        return ok

    def set_ignoreIncoh(self, value):
        self.mask = np.int32(0x0ff) if value else np.int32(0x1ff)

    def get_ignoreIncoh(self):
        # noinspection PyTypeChecker
        return not np.bool(np.bitwise_and(self.mask, 0x100))

    ignoreIncoh = property(get_ignoreIncoh, set_ignoreIncoh, None, "Flag for ignoring incoherent bpms")

    def set_Disabled(self, value):
        self.keep = np.ones(320, dtype=bool)
        self.keep[value] = False

    def get_Disabled(self):
        return np.flatnonzero(np.logical_not(self.keep))

    Disabled = property(get_Disabled, set_Disabled, None, "List of bpms disabled for DD processing")

    def read_sa(self, Averaging=None, wait=False, ignoreIncoh=None):
        """Gets horizontal and vertical positions, using defaults values
        for averaging and incoherence processing"""
        if Averaging is None:
            Averaging = self.Averaging
        if ignoreIncoh is None:
            mask = self.mask
        elif ignoreIncoh:
            mask = np.int32(0x0ff)
        else:
            mask = np.int32(0x1ff)
        xz = np.zeros((2, 320))
        nb = np.zeros(320, dtype=int)
        ww = float(wait)

        for _ in range(Averaging):
            time.sleep(ww)
            status = (np.bitwise_and(self.All_Status, mask) == 0)

            xz[0,:]+=self.HPositions[status]
            xz[1,:]+=self.VPositions[status]

            nb[status] += 1
            ww = 1

        with np.errstate(invalid='ignore'):
            return xz[0, :] / nb, xz[1, :] / nb

    def read_dd(self, *args, **kwargs):
        """Read turn-by-turn data

        Arguments:
        *args:          List of desired planes: 'h', 'x', 'v', 'z', 's' ...

        Keywords:
        'Trigger':      function which will trigger the acquisition (ex,: Ke.On)
        'Averaging':    Number of acquisitions
        'SleepTime:     Delay defore readng the data (default: 0.3 s)
        'timeout':      max. time for data availability

        Example:
            x,z=bpm.read_dd('h','v',Trigger=ke.On,Averaging=5)
        """

        def pack_and_check(plane, attribute):
            data = np.concatenate(plane, axis=0)
            # noinspection PyUnresolvedReferences
            identical = (data[1:, ...] == data[:-1, ...]).all(axis=2)
            if identical.any():
                print('Duplicated {0}:'.format(attribute))
                print(np.transpose(np.nonzero(identical)), file=sys.stderr)
                raise ValueError("Duplicated data detected")
            return np.squeeze(data)

        avg = kwargs.pop('Averaging', 1)
        sleeptime = kwargs.pop('SleepTime', 0.3)
        tg = kwargs.pop('Trigger', [])
        attributes = [self.ddattr[arg.lower()] for arg in args if isinstance(arg, str)]
        if not isinstance(tg, collections.Iterable):
            tg = [tg]
        triggers = [arg.On for arg in tg if isinstance(arg, PyTango.DeviceProxy)] + \
                   [arg for arg in tg if isinstance(arg, (types.FunctionType, types.LambdaType))]

        result = map(lambda attr: [], attributes)
        if triggers or (avg > 1) or ('timeout' in kwargs):
            self.trigger = self.All_DD_TriggerCounter
            tmax = 2
        else:
            tmax = 0
        timeout = kwargs.pop('timeout', 2)
        for _ in range(avg):
            nmiss = 0
            for test in range(tmax):
                [trig() for trig in triggers]
                nmiss = wait_for(self._newcnt, period=0.25, timeout=timeout, fail=False)
                if nmiss == 0:
                    time.sleep(sleeptime)
                    break
            if nmiss > 0:
                raise TimeoutError("Timeout waiting for {0} condition{1}".format(nmiss, "s"[nmiss == 1:]))
            map(lambda res, attr: res.append(self.attr_value(attr)[np.newaxis]), result, attributes)

        values = map(pack_and_check, result, attributes)
        if len(values) == 1:
            return values[0]
        else:
            return tuple(values)
