from __future__ import print_function, division
from atmodel import ATModel


class Model(ATModel):
    """Access to optical values
    
    Available locations:
    
    bpm:       All BPMs
    hbpm:      Horizontal BPMs
    vbpm:      Vertical BPMs
    qp:        All quadrupoles
    sx:        All sextpoles
    steerh:    Horizontal steerers
    steerv:    Vertical steerers
    id:        All IDs
    idx:       ID n
    pinhole    All 5 pinhole source points
    pinhole07  Pinhole id 07
    pinhole17  Pinhole bm 17
    pinhole25  Pinhole id 25
    pinhole27  Pinhole bm 27
    pinhole01  Pinhole bm 01
    
    Available parameters:
    
    0:     index in the AT structure
    1:     Integrated strength
    2:     theta/2pi
    3:     betax
    4:     alphax
    5:     phix/2pi/nux
    6:     etax
    7:     eta'x
    8:     betaz
    9:     alphaz
    10:    phiz/2pi/nux
    11:    etaz
    12:    eta'z
    
    Default parameters:
    
    [   2         3         5        6      8       10      ]
    [theta/2Pi, betax, Phix/2PiNux, etax, betaz, Phiz/2PiNux]
    """

    def __init__(self, *args):
        """
        m = ebs.model()              take the lattice from
            $APPHOME/sy/optics/settings/theory/betamodel.mat

        m = ebs.model('opticsname')  take the lattice from
            $APPHOME/sy/optics/settings/opticsname/betamodel.mat

        m = ebs.model(path)          take the lattice from
            path or path/betamodel.mat or path/betamodel.str
       """
        super(Model, self).__init__(self, *args)
        self.model = self.eng.ebs.model(*args)
