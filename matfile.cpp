#include <Python.h>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <cstring>

#include <mat.h>
#include <matrix.h>

#if PY_MAJOR_VERSION >= 3 || ( PY_MAJOR_VERSION >= 2 && PY_MINOR_VERSION >= 7)
#include <pycapsule.h>
static void matdestroy(PyObject *pypmat)
{
   if (PyCapsule_IsValid(pypmat, "matfile.file")) {
      matClose((MATFile *)PyCapsule_GetPointer(pypmat, "matfile.file"));
      PySys_WriteStdout("MAT-file closed\n");
   }
}
#define CAPSULE_TYPE PyCapsule_Type
#define CAPSULE_NEW(ptr) PyCapsule_New(ptr, "matfile.file", &matdestroy)
#define CAPSULE_GETPTR(cap) (PyCapsule_IsValid(cap, "matfile.file") ? PyCapsule_GetPointer(cap, "matfile.file") : NULL)
#else
static void matdestroy(void *matfile)
{
   matClose((MATFile *)matfile);
   PySys_WriteStdout("MAT-file closed\n");
}
#define CAPSULE_TYPE PyCObject_Type
#define CAPSULE_NEW(ptr) PyCObject_FromVoidPtr(ptr, &matdestroy)
#define CAPSULE_GETPTR(cap) (PyCObject_Check(cap) ? PyCObject_AsVoidPtr(cap) : NULL)
#endif

#define PY_ARRAY_UNIQUE_SYMBOL QEM_ARRAY_API
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>

#if PY_MAJOR_VERSION >= 3
  #define MOD_ERROR_VAL NULL
  #define MOD_SUCCESS_VAL(val) val
  #define MOD_INIT(name) PyMODINIT_FUNC PyInit_##name(void)
#else
  #define MOD_ERROR_VAL
  #define MOD_SUCCESS_VAL(val)
  #define MOD_INIT(name) PyMODINIT_FUNC init##name(void)
#endif

typedef PyObject *(*convitem)(const mxArray *, mwIndex);
extern "C" bool mxIsScalar(const mxArray *array_ptr);

static PyObject *MatFileError;

static PyObject *PyNone_Get(void)
{
   Py_INCREF(Py_None);
   return Py_None;
}

static void printobj(PyObject *obj)
{
   if (obj != NULL) {
      PyObject_Print((PyObject *)obj->ob_type, stderr, 0);
      fprintf(stderr, ", ");
      PyObject_Print(obj, stderr, 0);
      fprintf(stderr, "\n");
   }
   else {
      fprintf(stdout, "NULL pointer");
   }
}

static PyObject *mx_to_py(const mxArray *mxvar);

static PyObject *char_to_py(const mxArray *mxvar)
{
   char *str = mxArrayToString(mxvar);
   PyObject *result = PyUnicode_FromString(str);
   mxFree(str);
   return result;
}

#ifdef ZUT
static PyObject *struct_elem(const mxArray *mxvar, mwIndex index)
{
   PyObject *result = PyDict_New();
   int nfields = mxGetNumberOfFields(mxvar);
   while (nfields-- > 0) {
      const char *fname = mxGetFieldNameByNumber(mxvar, nfields);
      PyObject *pyvar = mx_to_py(mxGetFieldByNumber(mxvar, index, nfields));
      PyDict_SetItemString(result, fname, pyvar);
      Py_CLEAR(pyvar);
   }
   return result;
}

static PyObject *cell_elem(const mxArray *mxvar, mwIndex index)
{
   return mx_to_py(mxGetCell(mxvar, index));
}

static PyObject *listout(mwSize ndim, const mwSize *dims, const mxArray *mxvar, mwIndex *index, convitem conv)
{
   PyObject *result;
   mwSize i, imax = dims[ndim-1];
   if (imax == 1) {
      result = (ndim == 1) ? conv(mxvar, (*index)++) : listout(ndim-1, dims, mxvar, index, conv);
   }
   else {
      result = PyList_New(imax);
      if (ndim == 1) {
	     for (i=0; i<imax; i++) {
	        PyList_SetItem(result, i, conv(mxvar, (*index)++));	/* steals the reference */
	     }
      }
      else {
	     for (i=0; i<imax; i++) {
	        PyList_SetItem(result, i, listout(ndim-1, dims, mxvar, index, conv));	/* steals the reference */
	     }
      }
   }
   return result;
}
#endif

static PyObject *struct_to_py(const mxArray *mxvar)
{
   PyObject *result = NULL;
   if (mxIsScalar(mxvar)) {
      int nfields = mxGetNumberOfFields(mxvar);
      result = PyDict_New();
      while (nfields-- > 0) {
         const char *fname = mxGetFieldNameByNumber(mxvar, nfields);
         PyObject *pyvar = mx_to_py(mxGetFieldByNumber(mxvar, 0, nfields));
         if (pyvar != NULL) {
            PyDict_SetItemString(result, fname, pyvar);
            Py_CLEAR(pyvar);
         }
         else {
            PySys_WriteStderr("Cannot convert field %s\n", fname);
         }
      }
   }
   else {
      PySys_WriteStderr("Can only convert scalar structure\n");
      result = PyNone_Get();
   }
   //mwSize ndim = mxGetNumberOfDimensions(mxvar);
   //const mwSize *dims = mxGetDimensions(mxvar);
   //mwIndex index = 0;
   //return listout(ndim, dims, mxvar, &index, struct_elem);
   return result;
}

static PyObject *cell_to_py(const mxArray *mxvar)
{
   size_t sz = mxGetNumberOfElements(mxvar);
   PyObject *result = PyList_New(sz);
   for (size_t i=0; i<sz; i++) {
      PyList_SetItem(result, i, mx_to_py(mxGetCell(mxvar, i)));
   }
   return result;
}

template<typename mxtype, typename numpytype, int typenum>
static PyObject *to_numarr(const mxArray *mxvar)
{
   PyObject *result;
   mwSize ndim = mxGetNumberOfDimensions(mxvar);
   const mwSize *mxdims = mxGetDimensions(mxvar);
   mwSize d;
   mwSize nel = mxGetNumberOfElements(mxvar);
   mxtype *mxptr = (mxtype *) mxGetData(mxvar);
   npy_intp *pydims = new npy_intp[ndim];
   npy_intp *pyd = pydims;
   for (mwSize i=0; i<ndim; i++) {
      if ((d=*mxdims++) > 1) *pyd++ = d;
   }
   result = PyArray_EMPTY(pyd-pydims, pydims, typenum, 1);
   delete[] pydims;
   numpytype *pyptr = (numpytype *)PyArray_DATA((PyArrayObject *)result);
   while (nel-- > 0) *pyptr++ = *mxptr++;
   return PyArray_Return((PyArrayObject *)result);       /* check for scalar arrays */
}

static PyObject *mx_to_py(const mxArray *mxvar)
{
   PyObject *result;
   if (mxvar == NULL) {
      result = PyNone_Get();
      PySys_WriteStderr("NULL mxArray\n");
   }
   else if (mxIsChar(mxvar)) result = char_to_py(mxvar);
   else if (mxIsDouble(mxvar)) result = to_numarr<double, npy_double, NPY_DOUBLE>(mxvar);
   else if (mxIsLogical(mxvar)) result = to_numarr<mxLogical, npy_bool, NPY_BOOL>(mxvar);
   else if (mxIsStruct(mxvar)) result = struct_to_py(mxvar);
   else if (mxIsCell(mxvar)) result = cell_to_py(mxvar);
   else if (mxIsInt8(mxvar)) result = to_numarr<int8_T, npy_int8, NPY_INT8>(mxvar);
   else if (mxIsInt16(mxvar)) result = to_numarr<int16_T, npy_int16, NPY_INT16>(mxvar);
   else if (mxIsInt32(mxvar)) result = to_numarr<int32_T, npy_int32, NPY_INT32>(mxvar);
   else if (mxIsInt64(mxvar)) result = to_numarr<int64_T, npy_int64, NPY_INT64>(mxvar);
   else if (mxIsUint8(mxvar)) result = to_numarr<uint8_T, npy_uint8, NPY_UINT8>(mxvar);
   else if (mxIsUint16(mxvar)) result = to_numarr<uint16_T, npy_uint16, NPY_UINT16>(mxvar);
   else if (mxIsUint32(mxvar)) result = to_numarr<uint32_T, npy_uint32, NPY_UINT32>(mxvar);
   else if (mxIsUint64(mxvar)) result = to_numarr<uint64_T, npy_uint64, NPY_UINT64>(mxvar);
   else {
      result = PyNone_Get();
      PySys_WriteStderr("Unknown mxArray type\n");
   }
   return result;
}

static mwSize *mxSetDims(PyArrayObject *pyarr, mwSize *nd, int oned_as)
{
   int ndim = PyArray_NDIM(pyarr);
   npy_intp *pydims = PyArray_DIMS(pyarr);
   int mdim = std::max<int>(ndim, 2);
   mwSize *mxdims = new mwSize[mdim];
   mxdims[0] = 1;
   mxdims[1] = 1;
   mwSize *mxptr = mxdims;
   if ((ndim == 1) && (oned_as == 2)) *mxptr++ = 1;
   for (int i=0; i<ndim; i++) *mxptr++ = *pydims++;
   *nd = mdim;
   return mxdims;
}

static const char *tochar(PyObject *pystr)
{
   const char *p = NULL;
   if (PyUnicode_Check(pystr)) {
      PyObject *encoded = PyUnicode_AsEncodedString(pystr, "utf-8", "backslashreplace");
      if (encoded != NULL) {
         p = tochar(encoded);
         Py_CLEAR(encoded);
      }
   }
   else if (PyBytes_Check(pystr)) {
      p = PyBytes_AsString(pystr);
   }
   return p;
}

static mxArray *py_to_mx(PyObject *pystr, int oned_as);

static mxArray *bool_to_mx(mxLogical value)
{
   return mxCreateLogicalScalar(value);
}

template<typename mxtype, mxClassID mxID, typename numtype>
static mxArray *num_to_mx(numtype value)
{
   mxArray *mxarr = mxCreateNumericMatrix(1, 1, mxID, mxREAL);
   *(mxtype *)mxGetData(mxarr) = value;
   return mxarr;
}

static mxArray *dict_to_mx(PyObject *pydict, int oned_as)
{
   Py_ssize_t sz = PyDict_Size(pydict);
   mwSize mxsz = 0;
   const char **keys = new const char *[sz], **k = keys;
   PyObject **values = new PyObject *[sz], **v = values;
   Py_ssize_t pos = 0;
   int ok;
   PyObject *key;
   PyObject *value;
   while ((ok=PyDict_Next(pydict, &pos, &key, &value))) {
      const char *fieldname = tochar(key);
      if (fieldname != 0) {
         *k++ = fieldname;
         *v++ = value;
         mxsz++;
      }
      else {
         PySys_WriteStdout("Skipped field\n");
      }
   }
   mxArray *mxarr = mxCreateStructMatrix(1, 1, mxsz, keys);
   for (mwSize i = 0; i<mxsz; i++) {
      PySys_WriteStderr("Set field %s\n", keys[i]);
      //mxSetField(mxarr, 1, keys[i], py_to_mx(values[i], oned_as));
      mxSetFieldByNumber(mxarr, 1, i, py_to_mx(values[i], oned_as));
   }
   delete[] keys;
   delete[] values;
   return mxarr;
}

static mxArray *list_to_mx(PyObject *pystr, int oned_as)
{
   mwSize sz = PyList_Size(pystr);
   mwSize mxdims[2] = {1, 1};
   mxdims[oned_as-1] = sz;
   mxArray *mxarr = mxCreateCellArray(2, mxdims);
   PySys_WriteStderr("Created cell array [%ld,%ld]\n", mxdims[0], mxdims[1]);
   for (mwSize i=0; i<sz; i++) {
      mxSetCell(mxarr, i, py_to_mx(PyList_GetItem(pystr, i), oned_as));
   }
   return mxarr;
}

static mxArray *objarr_to_mx(PyArrayObject *pyarr, int oned_as)
{
   mxArray *mxarr = NULL;
   if (PyArray_NDIM(pyarr) == 0) {
      char *itemptr = PyArray_BYTES(pyarr);
      mxarr = py_to_mx(PyArray_GETITEM(pyarr, itemptr), oned_as);
   }
   else {
      mwSize ndim;
      mwSize *mxdims = mxSetDims(pyarr, &ndim, oned_as);
      mxarr = mxCreateCellArray(ndim, mxdims);
      delete[] mxdims;
      npy_intp size = PyArray_SIZE(pyarr);
      PyArrayObject *pyfort = (PyArrayObject *)PyArray_Ravel(pyarr, NPY_FORTRANORDER);
      for (npy_intp i=0; i<size; i++) {
         char *itemptr = (char *)PyArray_GETPTR1(pyfort, i);
         mxSetCell(mxarr, i, py_to_mx(PyArray_GETITEM(pyfort, itemptr), oned_as));
      }
      Py_CLEAR(pyfort);
   }
   return mxarr;
}

static mxArray *boolarr_to_mx(PyArrayObject *pyarr, int oned_as)
{
   mxArray *mxarr;
   if ( PyArray_NDIM(pyarr) == 0) {
      mxarr = mxCreateLogicalScalar(*(npy_bool *)PyArray_DATA(pyarr));
   }
   else {
      mwSize ndim;
      mwSize *mxdims = mxSetDims(pyarr, &ndim, oned_as);
      mxarr = mxCreateLogicalArray(ndim, mxdims);
      delete[] mxdims;
      npy_intp size = PyArray_SIZE(pyarr);
      PyArrayObject *pyfort = (PyArrayObject *)PyArray_Ravel(pyarr, NPY_FORTRANORDER);
      npy_bool *nptr = (npy_bool *) PyArray_DATA(pyfort);
      mxLogical *mptr = mxGetLogicals(mxarr);
      for (npy_intp i=0; i<size; i++) *mptr++ = *nptr++;
      Py_CLEAR(pyfort);
   }
   return mxarr;
}

template<typename mxtype, mxClassID mxID, typename numtype>
static mxArray *numarr_to_mx(PyArrayObject *pyarr, int oned_as)
{
   mwSize ndim;
   mwSize *mxdims = mxSetDims(pyarr, &ndim, oned_as);
   mxArray *mxarr = mxCreateNumericArray(ndim, mxdims, mxID, mxREAL);
   delete[] mxdims;
   npy_intp size = PyArray_SIZE(pyarr);
   PyArrayObject *pyfort = (PyArrayObject *)PyArray_Ravel(pyarr, NPY_FORTRANORDER);
   numtype *nptr = (numtype *) PyArray_DATA(pyfort);
   mxtype *mptr = (mxtype *) mxGetData(mxarr);
   for (npy_intp i=0; i<size; i++) *mptr++ = *nptr++;
   Py_CLEAR(pyfort);
   return mxarr;
}

static mxArray *numpy_to_mx(PyArrayObject *pyarr, int oned_as)
{
   PyArray_Descr *descr = PyArray_DESCR(pyarr);
   mxArray *mxarr;
   
   int nd = PyArray_NDIM(pyarr);
   npy_intp *pydims = PyArray_DIMS(pyarr);
   PySys_WriteStderr("Converting <%p>, type = %c, type_num: %d, dims:", pyarr, descr->type, descr->type_num);
   for (int i=0; i<nd; i++) PySys_WriteStderr( " %ld", pydims[i]);
   PySys_WriteStderr(", oned_as: %d\n", oned_as);

   if (descr->type_num == NPY_DOUBLE) mxarr = numarr_to_mx<double, mxDOUBLE_CLASS, npy_double>(pyarr, oned_as);
   else if (descr->type_num == NPY_STRING) mxarr = objarr_to_mx(pyarr, oned_as);
   else if (descr->type_num == NPY_UNICODE) mxarr = objarr_to_mx(pyarr, oned_as);
   else if (descr->type_num == NPY_BOOL) mxarr = boolarr_to_mx(pyarr, oned_as);
   else if (descr->type_num == NPY_INT32) mxarr = numarr_to_mx<int32_T, mxINT32_CLASS, npy_int32>(pyarr, oned_as);
   else if (descr->type_num == NPY_UINT32) mxarr = numarr_to_mx<uint32_T, mxUINT32_CLASS, npy_uint32>(pyarr, oned_as);
   else if (descr->type_num == NPY_INT16) mxarr = numarr_to_mx<int16_T, mxINT16_CLASS, npy_int32>(pyarr, oned_as);
   else if (descr->type_num == NPY_UINT16) mxarr = numarr_to_mx<uint16_T, mxUINT16_CLASS, npy_uint32>(pyarr, oned_as);
   else if (descr->type_num == NPY_INT8) mxarr = numarr_to_mx<int8_T, mxINT8_CLASS, npy_int8>(pyarr, oned_as);
   else if (descr->type_num == NPY_UINT8) mxarr = numarr_to_mx<uint8_T, mxUINT8_CLASS, npy_uint8>(pyarr, oned_as);
   else if (descr->type_num == NPY_INT64) mxarr = numarr_to_mx<int64_T, mxINT64_CLASS, npy_int64>(pyarr, oned_as);
   else if (descr->type_num == NPY_FLOAT) mxarr = numarr_to_mx<float, mxSINGLE_CLASS, npy_float>(pyarr, oned_as);
   else if (descr->type_num == NPY_OBJECT) mxarr = objarr_to_mx(pyarr, oned_as);
   else mxarr = NULL;
   return mxarr;
}

static mxArray *py_to_mx(PyObject *pystr, int oned_as)
{
   mxArray *mxarr = NULL;
   printobj(pystr);
   if (PyUnicode_Check(pystr) || PyBytes_Check(pystr)) {
      mxarr =  mxCreateString(tochar(pystr));
   }
   else if (PyArray_Check(pystr)) {
      mxarr = numpy_to_mx((PyArrayObject *)pystr, oned_as);
   }
   else if (PyBool_Check(pystr)) {      // Test before integers because it's a integer's subclass
      mxarr = bool_to_mx(PyLong_AsLong(pystr));
   }
#if PY_MAJOR_VERSION < 3
   else if (PyInt_Check(pystr)) {
      mxarr = num_to_mx<int32_T, mxINT32_CLASS, long>(PyInt_AsLong(pystr));
   }
#endif
   else if (PyLong_Check(pystr)) {
      mxarr = num_to_mx<int32_T, mxINT32_CLASS, long>(PyLong_AsLong(pystr));
   }
   else if (PyFloat_Check(pystr)) {
      mxarr = num_to_mx<double, mxDOUBLE_CLASS, double>(PyFloat_AsDouble(pystr));
   }
   else if (PyList_Check(pystr)) {
      mxarr = list_to_mx(pystr, oned_as);
   }
   else if (PyDict_Check(pystr)) {
      mxarr = dict_to_mx(pystr, oned_as);
   }
   else {
      PySys_WriteStdout("Unknown type\n");
   }
   return mxarr;
}

static PyObject *
matopen(PyObject *self, PyObject *args)
{
   PyObject *result = NULL;
   const char *fname;
   const char *fmode = "r";
   if (PyArg_ParseTuple(args, "s|s:matopen", &fname, &fmode)) {
      MATFile *pmat = matOpen(fname, fmode);
      result = (pmat != NULL) ?
         CAPSULE_NEW(pmat) :
	 PyErr_Format(PyExc_IOError, "Error opening file %s.", fname);
   }
   return result;
}

static PyObject *
matread(PyObject *self, PyObject *args)
{
   PyObject *result = NULL;
   PyObject *pypmat;
   const char *mxname;
   if (PyArg_ParseTuple(args, "O!s:matread", &CAPSULE_TYPE, &pypmat, &mxname))  {
      MATFile *mtf = (MATFile *) CAPSULE_GETPTR(pypmat);
      if (mtf != NULL) {
         const mxArray *mxvar = matGetVariable(mtf, mxname);
         result = (mxvar != NULL) ?
         mx_to_py(mxvar) : PyErr_Format(PyExc_KeyError, "Cannot extract %s from mat-file.", mxname);
      }
      else {
         result = PyErr_Format(MatFileError, "Invalid matfile object");
      }
   }
   return result;
}

static PyObject *
matwritearray(PyObject *self, PyObject *args)
{
   PyObject *pyarr;
   PyObject *result = NULL;
   PyObject *pypmat;
   const char *mxname;
   int oned_as = 1;
// if (PyArg_ParseTuple(args, "O!sO&|i:matwrite",  &CAPSULE_TYPE, &pypmat, &mxname, PyArray_Converter, &pyarr, &oned_as)) {
   if (PyArg_ParseTuple(args, "O!sO|i:matwrite",  &CAPSULE_TYPE, &pypmat, &mxname, &pyarr, &oned_as)) {
      MATFile *mtf = (MATFile *) CAPSULE_GETPTR(pypmat);
      if (mtf != NULL) {         
//       mxArray *mxarr = numpy_to_mx((PyArrayObject *)pyarr, oned_as);
         mxArray *mxarr = py_to_mx(pyarr, oned_as);
         if (mxarr != NULL) {
            int ok = matPutVariable(mtf, mxname, mxarr);
            mxDestroyArray(mxarr);
            result = (ok == 0) ? PyNone_Get() : PyErr_Format(PyExc_KeyError, "Cannot create variable %s.", mxname);
         }
         else {
            result = PyErr_Format(PyExc_TypeError, "Cannot convert to matlab array.");
         }
      }
      else {
         result = PyErr_Format(MatFileError, "Invalid matfile object");
      }
//    Py_CLEAR(pyarr);
   }
   return result;
}

static PyObject *
matdelete(PyObject *self, PyObject *args)
{
   PyObject *result = NULL;
   PyObject *pypmat;
   const char *mxname;
   if (PyArg_ParseTuple(args, "O!s:matdelete", &CAPSULE_TYPE, &pypmat, &mxname))  {
      MATFile *mtf = (MATFile *) CAPSULE_GETPTR(pypmat);
      if (mtf != NULL) {
         int ok = matDeleteVariable(mtf, mxname);
         result = (ok == 0) ? PyNone_Get() : PyErr_Format(PyExc_KeyError, "Cannot delete %s from mat-file.", mxname);
      }
      else {
         result = PyErr_Format(MatFileError, "Invalid matfile object");
      }
   }
   return result;
}

static PyObject *directory(PyObject *self, PyObject *args)
{
   PyObject *result = NULL;
   PyObject *pypmat;
   if (PyArg_ParseTuple(args, "O!", &CAPSULE_TYPE, &pypmat)) {
      MATFile *mtf = (MATFile *) CAPSULE_GETPTR(pypmat);
      if (mtf != NULL) {
         int num;
         char **mdir = matGetDir(mtf, &num);
         if (num >= 0) {
            result = PySet_New(NULL);
            for (int i=0; i<num; i++) PySet_Add(result, PyUnicode_FromString(mdir[i]));
            mxFree(mdir);
         }
         else {
            result = PyErr_Format(PyExc_KeyError, "Error analysing the file directory.");
         }
      }
      else {
         result = PyErr_Format(MatFileError, "Invalid matfile object");
      }
   }
   return result;
}

static PyMethodDef module_functions[] = {
    {"open",(PyCFunction) matopen,		METH_VARARGS,
    PyDoc_STR("open(filename,mode) -> matfile[Capsule]	open a matlab file\n"
        "\n"
        " filename : matlab file name\n"
        " mode     : file opening mode ('r', 'u', 'w', 'w4', 'wz', 'w7.3', default 'r')\n"
        "\n"
    )},

    {"matread",(PyCFunction) matread,   METH_VARARGS,
    PyDoc_STR("matread(matfile,varname) -> array	read a variable from of a MATLAB .mat file\n"
        "\n"
        " matfile : matlab file\n"
        " varname : variable name\n"
        "\n"
    )},

    {"matwritearray",(PyCFunction) matwritearray,   METH_VARARGS,
    PyDoc_STR("matwritearray(matfile,varname,value) -> None write a numpy array to of a MATLAB .mat file\n"
        "\n"
        " matfile : matlab file\n"
        " varname : variable name\n"
        " value : value to be cenverted to an array\n"
        "\n"
    )},

    {"matdelete",(PyCFunction) matdelete,   METH_VARARGS,
    PyDoc_STR("matdelete(matfile,varname) -> None delete a variable in a MATLAB .mat file\n"
        "\n"
        " matfile : matlab file\n"
        " varname : variable name\n"
        "\n"
    )},

    {"directory",(PyCFunction) directory,   METH_VARARGS,
    PyDoc_STR("directory(matfile) -> list	gets the variable names of a MATLAB .mat file\n"
        "\n"
        " matfile : matlab file\n"
        "\n"
    )},

    {NULL}  /* Sentinel */
};

MOD_INIT(_matfile)
{
#if PY_MAJOR_VERSION >= 3
    static struct PyModuleDef moduledef = {
	PyModuleDef_HEAD_INIT,
	"_matfile",			/* m_name */
	"access to MATLAB .mat files.",      /* m_doc */
	-1,                 /* m_size */
	module_functions,   /* m_methods */
	NULL,               /* m_reload */
	NULL,               /* m_traverse */
	NULL,               /* m_clear */
	NULL,               /* m_free */
    };
#endif
    PyObject* m;

#if PY_MAJOR_VERSION >= 3
    m = PyModule_Create(&moduledef);
#else
    m = Py_InitModule3("_matfile", module_functions,
                       "_matfile: access to MATLAB .mat files\n");
#endif
    if (m == NULL)
       return MOD_ERROR_VAL;

    import_array();

    MatFileError = PyErr_NewException((char *)"matfile.MatFileError", NULL, NULL);
    Py_XINCREF(MatFileError);
    PyModule_AddObject(m, "MatFileError", MatFileError);

    return MOD_SUCCESS_VAL(m);
}
