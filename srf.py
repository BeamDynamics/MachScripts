""" Storage Ring filling modes"""
from __future__ import print_function, division     # for python2
from builtins import str, zip, map, range, object   # for python2
import time
import sys
import random
from threading import Timer, Event

from pymach.tango import Device, DevState, DevSource, EventType
import numpy as np

from pymach.timing import WaitEvent
import PyTaco  # for pulse_rotation, pulse_select

__author__ = 'L. Farvacque'
__all__ = 'SRFill'
__version__ = '2.0'


def _plural(nb, word):
    return str(nb) + " " + word + "s"[nb == 1:]


class SRFill(object):
    class Devnull(object):
        def write(self, *_):
            pass

    def __init__(self, i_short=0.09 / 2.0 / 3.0, i_long=4.0 / 2.0 / 3.0, marker_current=1.0, **kwargs):
        """
        :keyword info: destination of the info stream
        """
        self.stp = Event()
        self.fill = {'7/8+1': self.build78, 'top7/8': self.topup78,
                     'other': self.fillstd, 'hybrid': self.fill_hybrid}
        self.conv = np.array([1, 3, 7, 15, 31], dtype=np.int16)
        self.i_step = {True: i_short, False: 0.1 * i_long}
        self.marker_current = marker_current
        self.info = kwargs.pop('info', SRFill.Devnull())
        self.ct = Device('sr/d-ct/1', source=DevSource.DEV)
        self.ict = Device('sr/d-ct/ict1', source=DevSource.DEV)
        self.gun = Device('elin/beam/run', source=DevSource.DEV)
        self.pulselength = Device('elin/r-gun/short')
        self.devbclock = Device('sy/tim/bclock')
        self.devpulserot = PyTaco.Device('sy/t-inj/pulse_rotation')
        # self.divider = PyTaco.Device('sy/t-inj/t0_freq')
        self.devpulses = [PyTaco.Device(dev) for dev in
                          ['sy/t-inj/pulse_select', 'sy/t-inj/pulse_1', 'sy/t-inj/pulse_2', 'sy/t-inj/pulse_3',
                           'sy/t-inj/pulse_4', 'sy/t-inj/pulse_5']]
        [dev.open(8) for dev in [self.devpulserot] + self.devpulses]  # No DC
        # [dev.open(8) for dev in [self.devpulserot, self.divider] + self.devpulses]

    def set(self):
        self.stp.set()

    def clear(self):
        self.stp.clear()

    def is_cancelled(self):
        if self.stp.is_set():
            raise KeyboardInterrupt('Cancelled by operator')
        return False

    def _fillstep(self, reached, ctdev, limit, npulses, timeout):
        ctdev.EventThreshold = limit
        reached.clear()
        print('    Filling up to {0:.1f} mA with {1} (timeout: {2:.0f} s)'.format(limit, _plural(npulses, 'pulse'),
                                                                                  timeout))
        self.gun.On()
        try:
            t0 = time.time()
            reached.wait(timeout=timeout, stop=lambda: ctdev.Current >= limit or self.is_cancelled())
            print(file=sys.__stdout__)
            print('    Filled in {0:.1f} s (timeout {1:.1f} s)'.format(time.time() - t0, timeout))
        finally:
            self.gun.Off()

    def _teststep(self, reached, ctdev, limit, npulses, timeout):
        ctdev.EventThreshold = limit
        reached.clear()
        print('    Filling up to {0:.1f} mA with {1} (timeout: {2:.0f} s)'.format(limit, _plural(npulses, 'pulse'),
                                                                                  timeout))
        t = Timer(timeout / 3.0, reached)  # 6 s/mA
        t.start()
        # self.gun.On()
        try:
            t0 = time.time()
            reached.wait(timeout=timeout, stop=lambda: ctdev.Current >= limit or self.is_cancelled())
            print(file=sys.__stdout__)
            print('    Filled in {0:.1f} s (timeout {1:.1f} s)'.format(time.time() - t0, timeout))
        finally:
            t.cancel()
            # self.gun.Off()

    def _setclock(self, pulse_list, bunch_list, rotcommand, shuffle=False, sb_position=0):
        nb_pulses = len(pulse_list)
        if len(bunch_list) > 992:
            print('Bunch list too long ({0}), truncated'.format(len(bunch_list)), file=sys.stderr)
            bunch_list = bunch_list[:992]
        if shuffle:
            random.shuffle(bunch_list)
        self.devbclock.Bunch_list = (bunch_list + sb_position) % 992
        self.devpulserot.cmd(rotcommand)
        data = zip(self.devpulses, np.insert(pulse_list, 0, self.conv[nb_pulses - 1]))
        [devc.cmd('DevSetValue', v) for devc, v in data]
        time.sleep(1.0)

    def get_nb_pulses(self):
        v = self.devpulses[0].cmd('DevRead')[0]
        return np.flatnonzero(self.conv == v)[0] + 1

    def get_pulse_length(self):
        return self.pulselength.state() == DevState.CLOSE

    def fill_hybrid(self, total_current, sb_current=0, sb_position=0, shuffle=True, timeout=0.0, **kwargs):
        """Fill the SR in 24*8 +1

        :param total_current: desired total current [mA]
        :param sb_current: desired single bunch current [mA]
        :keyword sb_position: desired bucket_number of the single bunch
        :keyword shuffle: randomize the bucket list
        :keyword timeout: Time-out for injection
        :keyword info: destination of the info stream
        """

        info = kwargs.get('info', self.info)
        pct = Device(self.ct.CurrentDeviceName, source=DevSource.DEV)

        total_initial = pct.read_attribute('current').value
        sb_initial = self.ict.read_attribute('current').value

        multibunch_current = total_current - sb_current
        multibunch_initial = total_initial - sb_initial

        delta_i_multi = (multibunch_current - multibunch_initial)
        delta_i_sb = sb_current - sb_initial
        t1 = delta_i_multi / 4.0  # time for step1
        t2 = delta_i_sb  # time for step3
        if timeout <= 2.6:
            timeout = 2.6 + (t1 + t2) * 3 / 10 / self.i_step[True]  # 3: safety, 10: Hz
        tmnorm = (timeout - 2.6) / (t1 + t2)

        self.clear()
        currentreached = WaitEvent(period=0.5)
        evtid = pct.subscribe_event('Current', EventType.DATA_READY_EVENT, currentreached, [], True)
        t0 = time.time()
        print('Filling up to {0:.1f} mA (timeout: {1:.0f} s)'.format(total_current, timeout))
        if (multibunch_current > 0) and (delta_i_multi > 0):
            imax = total_initial + delta_i_multi
            number_pulse_booster = 4  # bunches 148->851
            booster_spacing = 62
            lb4 = np.arange(8) + 148
            lb4 = np.concatenate((lb4, lb4 + 31))
            lb4 = np.concatenate((lb4, lb4 + 248, lb4 + 496))
            pulse_list = np.arange(number_pulse_booster, dtype=np.int16) * booster_spacing
            print('delta_i: {0:.3} mA with {1} bunches'.format(delta_i_multi, lb4.size), file=info)
            print(lb4, file=info)
            self._setclock(pulse_list, lb4, 'DevClose', shuffle=shuffle, sb_position=sb_position)
            self._fillstep(currentreached, pct, imax, len(pulse_list), max(1.3 + t1 * tmnorm, 5))

        if (sb_current > 0) and (delta_i_sb > 0):
            lb1 = np.array([0], dtype=np.int16)
            pulse_list = np.arange(1, dtype=np.int16)
            print('delta_i: {0:.3} mA with {1} bunches'.format(delta_i_multi, lb1.size), file=info)
            print(lb1, file=info)
            self._setclock(pulse_list, lb1, 'DevClose', shuffle=False, sb_position=sb_position)
            self._fillstep(currentreached, self.ict, sb_current, len(pulse_list), max(1.3 + t2 * tmnorm, 5))
        pct.unsubscribe_event(evtid)
        print(file=sys.__stdout__)
        print("Refill done in {0:.1f} s (timeout {1:.0f} s)".format(time.time() - t0, timeout))

    def fillstd(self, total_current, timeout=0, **kwargs):
        """Fill the SR

        :param total_current:  desired total current [mA]
        :keyword timeout: Time-out for injection
        """

        pct = Device(self.ct.CurrentDeviceName, source=DevSource.DEV)
        total_initial = kwargs.pop('pctest', pct.Current)
        nb_pulses = self.get_nb_pulses()
        delta_i = (total_current - total_initial)
        if timeout <= 1.3:
            i_step = nb_pulses * self.i_step[self.get_pulse_length()]
            timeout = 1.3 + delta_i * 3 / 10 / i_step  # 3: safety, 10: Hz
        self.clear()
        currentreached = WaitEvent(period=0.5, name='current reached')
        evtid = pct.subscribe_event('Current', EventType.DATA_READY_EVENT, currentreached, [], True)
        t0 = time.time()
        print('Filling up to {0:.1f} mA (timeout: {1:.0f} s)'.format(total_current, timeout))
        if (total_current > 0) and (delta_i > 0):
            self._fillstep(currentreached, pct, total_current, nb_pulses, timeout)
        pct.unsubscribe_event(evtid)
        print("Refill done in {0:.1f} s (timeout {1:.0f} s)".format(time.time() - t0, timeout))

    def bucketlist(self, fill0, fill, blist, n_pulses):

        def plist(idx, value):
            val = np.empty(value, dtype=np.int16)
            val.fill(idx)
            return val

        delta = fill - fill0
        dsumi = np.dot(delta, n_pulses)
        if dsumi > 0:
            order = np.argsort(-delta)
            f0 = -delta[order]
            n0 = n_pulses[order]
            ib = (dsumi + np.cumsum(f0 * n0)) / np.cumsum(n0)
            limit = np.flatnonzero(f0 > ib)
            nm = len(f0) if limit.size == 0 else limit[0]
            sigi = np.ceil((ib[nm - 1] * np.arange(1, nm + 1) - np.cumsum(f0[:nm])) / self.i_step[True])
            ns = sigi - np.concatenate(([0], sigi[:-1]))
            lb = np.concatenate(map(plist, blist[order][:nm], ns[:nm]))
            lc = np.concatenate(map(plist, n0[:nm], ns[:nm]))
            return lb, lc
        else:
            return np.array([], dtype=np.int16), np.array([], dtype=np.int16)

    def build78(self, total_current, sb_current=0, sb_position=0, shuffle=True, timeout=0.0, **kwargs):
        """Fill the SR in 7/8+1, using 2, then 4, then 1 bunch from the linac

        :param total_current: desired total current [mA]
        :param sb_current: desired single bunch current [mA]
        :keyword sb_position: desired bucket_number of the single bunchfinal_s
        :keyword shuffle: randomize the bucket list
        :keyword timeout: Time-out for injection
        :keyword info: destination of the info stream
       """

        def list4(shft):
            f1 = np.roll(np.arange(88, dtype=np.int16), shft) + (sb_position + 145)
            return np.concatenate((f1, f1 + 4 * 88)) % 992

        info = kwargs.get('info', self.info)
        pct = Device(self.ct.CurrentDeviceName, source=DevSource.DEV)
        total_initial = kwargs.pop('pctest', pct.Current)
        sb_initial = kwargs.pop('ictest', self.ict.Current)
        delta_sb = max(sb_current - sb_initial, 0.0)
        marker_current = self.marker_current if sb_current > 0 else 0.0

        try:
            profile = kwargs.pop('profile')
        except KeyError:
            mode = 'without'
            if sb_current < 2.5:  # No feedback
                if sb_initial > 0.5:  # Top-up
                    sb_num = 3  # Markers are 3 times lower than the single bunch
                else:  # Refill
                    sb_num = 2  # Markers are 2 times lower than the single bunch
            elif sb_current < 4.5:  # With feedback
                if total_initial > 10.0:  # Top-up
                    sb_num = 8  # Markers are 8 times lower than the single bunch
                else:  # Refill
                    sb_num = 4  # Markers are 4 times lower than the single bunch
            else:  # high chromaticity
                if total_initial > 10.0:  # Top-up
                    sb_num = 16  # Markers are 16 times lower than the single bunch
                else:  # Refill
                    sb_num = 8  # Markers are 8 times lower than the single bunch

            deltas = delta_sb * (sb_num + 2) / sb_num
            sm_current = sb_current + 2 * marker_current
            sm_initial = sm_current - deltas  # single + markers (estimate)
            lb2 = (np.arange(82, dtype=np.int16) + (sb_position + 849)) % 992
            lb4 = np.concatenate((list4(0), list4(1), list4(2), list4(3)))
            lb1 = (np.array([62] + [0] * sb_num + [930], dtype=np.int16) + sb_position) % 992
            delta_i_multi = total_current - sm_current - total_initial + sm_initial
            delta2 = 164 / 868 * delta_i_multi
            delta4 = 704 / 868 * delta_i_multi
        else:
            mode = 'with'
            filling = np.roll(profile, -sb_position)
            filling = filling / np.sum(filling[62:931]) * (total_initial - sb_initial)
            final_s = np.array(
                [max(marker_current, filling[62]), max(sb_current, sb_initial), max(marker_current, filling[930])])
            bunch_current = (total_current - np.sum(final_s)) / 867

            fill_2 = np.sum(np.vstack((
                filling[63:145], filling[list(range(849, 930)) + [144]]
            )), axis=0) / 2
            fill_4 = np.sum(np.concatenate((
                np.reshape(filling[145:497], (4, 88)), np.reshape(filling[497:849], (4, 88))
            ), axis=1), axis=0) / 4
            delta24 = 868 * bunch_current - 2 * np.sum(fill_2) - 4 * np.sum(fill_4)
            lb24, flag = self.bucketlist(np.concatenate((fill_2, fill_4)), bunch_current * np.ones(258),
                                         np.concatenate((np.arange(849, 931, dtype=np.int16),
                                                         np.arange(145, 233, dtype=np.int16),
                                                         np.arange(497, 585, dtype=np.int16))),
                                         np.concatenate((2 * np.ones(82, dtype=np.int16),
                                                         4 * np.ones(176, dtype=np.int16))))
            lb2 = lb24[flag == 2]
            lb4 = lb24[flag == 4]
            delta2 = lb2.size / (lb2.size + 2 * lb4.size) * delta24
            delta4 = delta24 - delta2
            # noinspection PyTypeChecker
            fill_s = np.array(
                [filling[62], sb_initial, filling[930] + self.i_step[True] * np.count_nonzero(lb2 == 930)])
            deltas = max(np.sum(final_s) - np.sum(fill_s), 0)
            lb1, _ = self.bucketlist(fill_s, final_s, np.array([62, 0, 930]), np.ones(3))

        t2 = delta2 / 2
        t4 = delta4 / 4
        ts = deltas
        if timeout <= 3.9:
            timeout = 3.9 + (t2 + t4 + ts) * 3 / 10 / self.i_step[True]  # 3: safety, 10: Hz
        tmnorm = (timeout - 3.9) / (t2 + t4 + ts) if timeout > 0 else 1.0
        imax = total_initial

        self.clear()
        currentreached = WaitEvent(period=0.5)
        evtid = pct.subscribe_event('Current', EventType.DATA_READY_EVENT, currentreached, [], True)
        print(
            'Filling up to {0:.1f} mA with multiple pulses {1} reference (timeout: {2:.0f} s)'.format(total_current,
                                                                                                      mode,
                                                                                                      timeout))

        t0 = time.time()
        if delta2 > 0:
            imax += delta2
            number_pulse_booster = 2  # bunches 849->930 and 63->144
            booster_spacing = 206
            pulse_list = np.arange(number_pulse_booster, dtype=np.int16) * booster_spacing
            print('delta_i: {0:.3} mA with {1} bunches'.format(delta2, lb2.size), file=info)
            print(lb2, file=info)
            self._setclock(pulse_list, lb2, 'DevClose', shuffle=shuffle, sb_position=sb_position)
            self._fillstep(currentreached, pct, imax, len(pulse_list), max(1.3 + t2 * tmnorm, 5))

        if delta4 > 0:
            imax += delta4
            number_pulse_booster = 4  # bunches 145->848
            booster_spacing = 88
            pulse_list = np.arange(number_pulse_booster, dtype=np.int16) * booster_spacing
            print('delta_i: {0:.3} mA with {1} bunches'.format(delta4, lb4.size), file=info)
            print(lb4, file=info)
            self._setclock(pulse_list, lb4, 'DevOpen', shuffle=shuffle, sb_position=sb_position)
            self._fillstep(currentreached, pct, imax, len(pulse_list), max(1.3 + t4 * tmnorm, 5))

        if deltas > 0:
            pulse_list = np.arange(1, dtype=np.int16)
            print('delta_i: {0:.3} mA with {1} bunches'.format(deltas, lb1.size), file=info)
            print(lb1, file=info)
            self._setclock(pulse_list, lb1, 'DevClose', shuffle=shuffle, sb_position=sb_position)
            self._fillstep(currentreached, self.ict, sb_current, len(pulse_list), max(1.3 + ts * tmnorm, 5))

        pct.unsubscribe_event(evtid)
        print(file=sys.__stdout__)
        print("Refill done in {0:.1f} s (timeout {1:.0f} s)".format(time.time() - t0, timeout))

    def topup78(self, total_current, sb_current=0, sb_position=0, shuffle=True, timeout=0.0, **kwargs):
        """Fill the SR in 7/8+1 using only 1 bunch from the linac

        :param total_current: desired total current [mA]
        :param sb_current: desired single bunch current [mA]
        :param sb_position: desired bucket_number of the single bunch
        :param shuffle: randomize the bucket list
        :param timeout: Time-out for injection
        :keyword info: destination of the info stream
        """

        try:
            profile = kwargs.pop('profile')
        except KeyError:
            self.build78(total_current, sb_current=sb_current, sb_position=sb_position,
                         shuffle=shuffle, timeout=timeout, **kwargs)
            return
        info = kwargs.get('info', self.info)
        pct = Device(self.ct.CurrentDeviceName, source=DevSource.DEV)
        total_initial = kwargs.pop('pctest', pct.Current)
        sb_initial = kwargs.pop('ictest', self.ict.Current)
        marker_current = self.marker_current if sb_current > 0 else 0.0

        print('total : {0:.3f}/{1:.3f}'.format(total_initial, total_current), file=info)
        print('single: {0:.3f}/{1:.3f}'.format(sb_initial, sb_current), file=info)
        filling = np.roll(profile, -sb_position)
        mm_initial = total_initial - sb_initial
        filling = filling / np.sum(filling[62:931]) * mm_initial  # noinspection PyTypeChecker
        mm_current = total_current - max(sb_current, sb_initial)
        print('multi : {0:.3f}/{1:.3f}'.format(mm_initial, mm_current), file=info)
        bunch_current = (mm_current - 2 * marker_current) / 867  # Markers excluded
        deltamm = max(mm_current - mm_initial, 0.0)
        fill0 = bunch_current * np.ones(869)
        fill0[0] = marker_current
        fill0[-1] = marker_current
        fillm = filling[62:931]
        lb, _ = self.bucketlist(fillm, fill0, np.arange(62, 931, dtype=np.int16), np.ones(869, dtype=np.int16))

        delta1 = max(sb_current - sb_initial, 0.0)
        lb1 = np.array([0])
        tm = deltamm
        t1 = delta1
        if timeout <= 0:
            timeout = 2.6 + (tm + t1) * 3 / 10 / self.i_step[True]  # 3: safety, 10: Hz
        tmnorm = (timeout - 2.6) / (tm + t1) if timeout > 0 else 1.0

        self.clear()
        currentreached = WaitEvent(period=0.5)
        evtid = pct.subscribe_event('Current', EventType.DATA_READY_EVENT, currentreached, [], True)
        print('Filling up to {0:.1f} mA with a single pulse (timeout: {1:.0f} s)'.format(total_current, timeout))
        t0 = time.time()
        imax = total_initial
        if deltamm > 0:
            imax += deltamm
            pulse_list = np.arange(1, dtype=np.int16)
            print('delta_i: {0:.3} mA with {1} bunches'.format(deltamm, lb.size), file=info)
            print(lb, file=info)
            # self._setclock(pulse_list, lb, 'DevClose', shuffle=shuffle, sb_position=sb_position)
            self._teststep(currentreached, pct, imax, len(pulse_list), max(1.3 + tm * tmnorm, 5))

        if delta1 > 0:
            pulse_list = np.arange(1, dtype=np.int16)
            print('delta_i: {0:.3} mA with {1} bunches'.format(delta1, lb1.size), file=info)
            print(lb1, file=info)
            # self._setclock(pulse_list, lb1, 'DevClose', shuffle=False, sb_position=sb_position)
            self._teststep(currentreached, self.ict, sb_current, len(pulse_list), max(1.3 + t1 * tmnorm, 5))

        pct.unsubscribe_event(evtid)
        print(file=sys.__stdout__)
        print("Refill done in {0:.1f} s (timeout {1:.0f} s)".format(time.time() - t0, timeout))
