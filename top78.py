#!/usr/bin/env python
""" Top-up the Storage Ring in 7/8+1
Usage: fill78.py [options] total_current singlebunch_current

Options:
--version       show program's version number and exit
-h, --help      show this help message and exit
--timeout=t     time-out for refill [s]
"""

from __future__ import print_function, division  # for python2

import os

import numpy as np
from builtins import range  # for python2

import srf as srfill
from sr.profile import load_filling_pattern

__author__ = 'author'
__version__ = '1.0'

_FILLING_PROFILE_NAME = os.path.join(os.environ['APPHOME'], 'topup', 'refill.mat')


def getprofile(filename, max_age, min_current):
    # Try to get the filling profile measured by the purity device
    pattern = {}
    try:
        pattern = load_filling_pattern(filename, max_age, min_current)
    except KeyError:
        print('Wrong filling pattern: unexpected file contents')
    except RuntimeError as err:
        print('Wrong filling pattern:', err)
    except (IOError, OSError):
        print('No filling pattern available ')
    return pattern


if __name__ == "__main__":
    # import argparse
    import optparse

    # parser = argparse.ArgumentParser(prog='tgread',description='Read a Tango attribute')
    # parser.add_argument('attributename',nargs='+',help='Attribute_name')
    # parser.add_argument('-f',nargs='?',help='Format output')
    # parser.add_argument('-t',nargs='?',help='Polling period [s]')

    parser = optparse.OptionParser(
        usage="%prog [options] total_current sb_current",
        version=__version__, description="Fills the SR in 7/8+1 mode")
    parser.add_option('-t', '--timeout', type=float, default=0.0,
                      help='time-out for refill [s] (default: %default)')
    parser.add_option('-n', '--bucket-number', type=int, default=0,
                      help='Position of the single bunch (default: %default)')
    parser.add_option('-f', '--filename', type=str, default=_FILLING_PROFILE_NAME,
                      help='filling pattern file name (default: %default)')
    parser.add_option('-x', '--max-age', type=float, default=600.0,
                      help='maximum age of the filling pattern (default: %default)')
    parser.add_option('-i', '--min-current', type=float, default=150.0,
                      help='minimum current of the filling pattern (default: %default)')
    parser.add_option('-v', '--verbose', action='store_true', default=False,
                      help='Verbose mode (default: %default)')
    options, args = parser.parse_args()
    total_current = float(args[0])
    sb_current = float(args[1])
    print('Total current: {0}, single bunch current: {1}'.format(total_current, sb_current))

    patt = getprofile(options.filename, options.max_age, options.min_current)
    patt.update(sb_position=options.bucket_number, timeout=options.timeout)
    total_initial = patt.get('pct', 198.5)  # For test only
    sb_initial = patt.get('ict', 7.490)  # For test only
    filling = patt['profile'] / np.sum(patt['profile'][62:931]) * (total_initial - sb_initial)
    marker_current = 1.0
    final_s = np.array(
        [max(marker_current, filling[62]), max(sb_current, sb_initial), max(marker_current, filling[930])])
    bunch_current = (total_current - np.sum(final_s)) / 867

    fill_2 = np.sum(np.vstack((
        filling[63:145], filling[list(range(849, 930)) + [144]]
    )), axis=0) / 2
    fill_4 = np.sum(np.concatenate((
        np.reshape(filling[145:497], (4, 88)), np.reshape(filling[497:849], (4, 88))
    ), axis=1), axis=0) / 4

    srf = srfill.SRFill()
    lb24, flag = srf.bucketlist(np.concatenate((fill_2, fill_4)), bunch_current * np.ones(258),
                                np.concatenate((np.arange(849, 931, dtype=np.int16),
                                                np.arange(145, 233, dtype=np.int16),
                                                np.arange(497, 585, dtype=np.int16))),
                                np.concatenate((2 * np.ones(82, dtype=np.int16),
                                                4 * np.ones(176, dtype=np.int16))))
    lb2 = lb24[flag == 2]
    lb4 = lb24[flag == 4]
    print(lb2, len(lb2))
    print(lb4, len(lb4))
    # noinspection PyTypeChecker
    fill_s = np.array([filling[62], filling[0], filling[930] + srf.i_step[True]*np.count_nonzero(lb2 == 930)])
    print(fill_s)
    print(final_s)
    lb1, _ = srf.bucketlist(fill_s, final_s, np.array([62, 0, 930]), np.ones(3))
    print(lb1, len(lb1))
