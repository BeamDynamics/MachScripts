"""Improved PyTango classes:

tango.DevState():
    - has a 'severity' property to allow ordering
    - fully ordered enumeration allowing easy combination of states with min, max...

tango.Device(PyTango.DeviceProxy):
    - possibility to select the SOURCE at initialization
    - state() returns a tango.DevState instead of PyTango.DevState
    - methods attr_get, attr_value, attr_setpoint
    - method confirmedState returns MOVING if the new state differs from the previous one
    - method verbose_command subscribing to events to print messages from the server
    - 'devname' property
    - safe event handling: events are unsubscribed in the destructor

tango.DeviceGroup(PyTango.Group):
    - possiblity of giving group members at initialization
    - 3 methods for attribute access: attr_get, attr_setpoint, attr_value
    - "state" method to have easy access to all states

tango.Attribute(tango.Device):
    - possibility of specifying a default value at initialization
    - setread() returns a named tuple (setpoint, value, time, quality)
    - format(value) returns a formatted string for the value
    - 'read' and 'write' methods
    - 'value' and 'setpoint' properties
    - 'devname' property
    - safe event handling: events are unsubscribed in the destructor

tango.AttributeGroup(tango.DeviceGroup):
    - get the same attribute on a device group
    - setread() returns a named tuple (setpoint, value, time, quality), each attribute being a tuple
        of values
    - 'value' and 'setpoint' properties (tuples)
    - get_attribute_list() returns a list of attribute names
"""
from __future__ import print_function, division, unicode_literals, absolute_import   # for python2
from builtins import range, zip      # for python2
import sys
import collections
from threading import Event
try:
    # noinspection PyUnresolvedReferences
    from enum import Enum
except ImportError:
    pass
import tango
# noinspection PyUnresolvedReferences
from tango import DevFailed, DevSource, AttrQuality, EventType, ExtractAs
from .timing import wait_for

__all__ = ['Device', 'Attribute', 'DeviceGroup', 'AttributeGroup', 'DevState', 'DevFailed', 'DevSource', 'AttrQuality',
           'EventType']

_av = collections.namedtuple('AttributeValue', ['setpoint', 'value', 'time', 'quality'])


class DevState(Enum):
    __sv__ = [1, 4, 6, 1, 6, 1, 5, 3, 9, 5, 2, 7, 8, 10]

    ON = 0
    OFF = 1
    CLOSE = 2
    OPEN = 3
    INSERT = 4
    EXTRACT = 4
    MOVING = 6
    STANDBY = 7
    FAULT = 8
    INIT = 9
    RUNNING = 10
    ALARM = 11
    DISABLE = 12
    UNKNOWN = 13

    def __eq__(self, other):
        if self.__class__ is other.__class__:
            res = (self is other)
        else:
            res = (self.value == other)
        return res

    def __ne__(self, other):
        return not (self == other)

    def __hash__(self):
        return hash(self.value)

    def __lt__(self, other):
        return self.severity < other.severity

    def __le__(self, other):
        return self.severity <= other.severity

    def __gt__(self, other):
        return self.severity > other.severity

    def __ge__(self, other):
        return self.severity <= other.severity

    def __str__(self):
        return self.name

    def __int__(self):
        return self.value

    @property
    def severity(self):
        return self.__sv__[self.value]


class _DevAccess(object):
    @staticmethod
    def validate(attr, defval=None):
        if attr.quality == AttrQuality.ATTR_INVALID:
            return _av(defval, defval, attr.time.todatetime(), attr.quality)
        else:
            vs = defval if attr.w_value is None else attr.w_value
            vr = defval if attr.value is None else attr.value
            return _av(vs, vr, attr.time.todatetime(), attr.quality)

    @staticmethod
    def invalid(defval):
        return _av(defval, defval, tango.TimeVal.now().todatetime(), AttrQuality.ATTR_INVALID)


class Device(tango.DeviceProxy, _DevAccess):
    """Add the following to the PyTange.DeviceProxy:

    - possibility to select the SOURCE at initialization
    - state() returns a tango.DevState instead of PyTango.DevState
    - methods attr_get, attr_value, attr_setpoint
    - method confirmedState returns MOVING if the new state differs from the previous one
    - property devname
    - safe event handling: events are unsubscribed in the destructor
    """

    def __init__(self, *args, **kwargs):
        """
        Keyword arguments:
            green_mode:
            wait:
            timeout:
        """
        source = kwargs.pop('source', DevSource.CACHE_DEV)
        super(Device, self).__init__(*args, **kwargs)
        self.set_source(source)
        self.prevstate = tango.DevState.UNKNOWN
        self.prevstatus = ''
        self.evtlist = []           # list of event subscriptions
        self.timer = Event()        # for interruptible long actions

    def __del__(self):
        for event_id in self.evtlist:
            super(Device, self).unsubscribe_event(event_id)
            # super(Device, self).__del__()  # No destructor in DeviceProxy

    def set(self):
        self.timer.set()

    def clear(self):
        self.timer.clear()

    def sleep(self, delay):
        self.timer.wait(timeout=delay)
        if self.timer.is_set():
            raise KeyboardInterrupt('Cancelled by operator')

    def subscribe_event(self, *args, **kwargs):
        evtid = super(Device, self).subscribe_event(*args, **kwargs)
        self.evtlist.append(evtid)
        return evtid

    def unsubscribe_event(self, event_id):
        for i in range(len(self.evtlist)):
            if event_id is self.evtlist[i]:
                del self.evtlist[i]
                break
        super(Device, self).unsubscribe_event(event_id)

    def attr_get(self, attrname, **kwargs):
        """read an attribute and return a named tuple with fields 'setpoint', 'value', 'time' and quality
        Keyword arguments:
            default:    default attribute value (default: throw DevFailed exception)
            extract_as:
            green_mode:
            wait:
            timeout:
        """
        catch = ('default' in kwargs)
        defval = kwargs.pop('default', None)
        try:
            attr = self.read_attribute(attrname, **kwargs)
            return self.validate(attr, defval)
        except DevFailed:
            if not catch:
                raise
            return self.invalid(defval)

    def attr_value(self, attrname, **kwargs):
        """get an attribute value as a tuple of values for each member of the group
        Keyword arguments:
            default:    default attribute value (default: throw DevFailed exception)
            extract_as:
            green_mode:
            wait:
            timeout:
        """
        return self.attr_get(attrname, **kwargs).value

    def attr_setpoint(self, attrname, **kwargs):
        """get an attribute setpoint as a tuple of values for each member of the group
        Keyword arguments:
            default:    default attribute value (default: throw DevFailed exception)
            extract_as:
            green_mode:
            wait:
            timeout:
        """
        return self.attr_get(attrname, **kwargs).setpoint

    def state(self):
        # self.prevstate = super(Device, self).state()
        self.prevstate = self.attr_get('State', default=tango.DevState.UNKNOWN).value
        return DevState(self.prevstate)

    def status(self):
        # self.prevstate = super(Device, self).state()
        self.prevstatus = self.attr_get('Status', default='').value
        return self.prevstatus.replace('\n',' ')

    # noinspection PyPep8Naming
    def confirmedState(self):
        # newstate = super(Device, self).state()
        newstate = self.attr_get('State', default=tango.DevState.UNKNOWN).value
        val = newstate if newstate == self.prevstate else tango.DevState.MOVING
        self.prevstate = newstate
        return DevState(val)

    @property
    def devname(self):
        return self.name()


class DeviceGroup(tango.Group, object):
    @staticmethod
    def validate(grpattr, defval=None):
        attr = grpattr.get_data()
        return Device.invalid(defval) if grpattr.has_failed() else Device.validate(attr, defval)

    def __init__(self, groupname, *devnames):
        super(DeviceGroup, self).__init__(groupname)
        for arg in devnames:
            self.add(arg if isinstance(arg, (collections.Sequence, DeviceGroup)) else tuple(arg))

    def attr_get(self, attrname, **kwargs):
        """read an attribute and return a named tuple with fields 'setpoint', 'value', 'time' and 'quality'
        Keyword arguments:
            default:    default attribute value  None)
            forward:    If it is set to true (the default) request is forwarded to subgroups.
                        Otherwise, it is only applied to the local set of devices
        """
        defval = kwargs.pop('default', None)
        attrlist = self.read_attribute(attrname, **kwargs)
        return _av(*zip(*(self.validate(grpattr, defval) for grpattr in attrlist)))

    def attr_value(self, attrname, **kwargs):
        """get an attribute value as a tuple of values for each member of the group
        Keyword arguments:
            default:    default attribute value (default: None)
            forward:    If it is set to true (the default) request is forwarded to subgroups.
                        Otherwise, it is only applied to the local set of devices
        """
        return self.attr_get(attrname, **kwargs).value

    def attr_setpoint(self, attrname, **kwargs):
        """get an attribute setpoint as a tuple of values for each member of the group
        Keyword arguments:
            default:    default attribute value (default: None)
            forward:    If it is set to true (the default) request is forwarded to subgroups.
                        Otherwise, it is only applied to the local set of devices
        """
        return self.attr_get(attrname, **kwargs).setpoint

    def state(self, **kwargs):
        """get states as a tuple of values for each memeber of the group
        Keyword arguments:
            forward:    If it is set to true (the default) request is forwarded to subgroups.
                        Otherwise, it is only applied to the local set of devices
        """
        return tuple(DevState(st) for st in self.attr_get('State', default=tango.DevState.UNKNOWN, **kwargs).value)

    def status(self, **kwargs):
        """get states as a tuple of values for each memeber of the group
        Keyword arguments:
            forward:    If it is set to true (the default) request is forwarded to subgroups.
                        Otherwise, it is only applied to the local set of devices
        """
        return tuple(st.replace('\n',' ') for st in self.attr_get('Status', default='', **kwargs).value)

    def __repr__(self):
        # noinspection PyUnresolvedReferences
        return '{0}({1})'.format(self.__class__.__name__, self.get_name())


class Attribute(tango.AttributeProxy, _DevAccess):

    def __init__(self, *args, **kwargs):
        """
        Keyword arguments:
            source:
        """
        source = kwargs.pop('source', DevSource.CACHE_DEV)
        self.dft = {}
        if 'default' in kwargs:
            self.dft['default'] = kwargs.pop('default')
        self.evtlist = []           # list of event subscriptions
        super(Attribute, self).__init__(*args, **kwargs)
        self.config = self.get_config()
        self.set_source(source)

    def __del__(self):
        for event_id in self.evtlist:
            super(Attribute, self).unsubscribe_event(event_id)
            # super(Device, self).__del__()  # No destructor in AttributeProxy

    def subscribe_event(self, *args, **kwargs):
        evtid = super(Attribute, self).subscribe_event(*args, **kwargs)
        self.evtlist.append(evtid)
        return evtid

    def unsubscribe_event(self, event_id):
        for i in range(len(self.evtlist)):
            if event_id is self.evtlist[i]:
                del self.evtlist[i]
                break
        super(Attribute, self).unsubscribe_event(event_id)

    def format(self, value):
        try:
            return ' '.join((self.config.format % (float(self.config.display_unit) * value), self.config.unit))
        except (TypeError, ValueError):
            return '?'

    def get_source(self):
        return self.get_device_proxy().get_source()

    def set_source(self, *args):
        return self.get_device_proxy().set_source(*args)

    def setread(self, **kwargs):
        """return a named tuple with fields 'setpoint', 'value', 'time' and quality
        Keyword arguments:
            default:    default attribute value (default: throw DevFailed exception)
            extract_as:
            green_mode:
            wait:
            timeout:
        """
        kwargs.update(self.dft)
        catch = ('default' in kwargs)
        defval = kwargs.pop('default', None)
        try:
            attr = self.read(**kwargs)
            return self.validate(attr, defval)
        except DevFailed:
            if not catch:
                raise
            return self.invalid(defval)

    @property
    def devname(self):
        return self.get_device_proxy().name()

    @property
    def fullname(self):
        return '/'.join((self.devname, self.name()))

    @property
    def setpoint(self):
        return self.setread(**self.dft).setpoint

    @setpoint.setter
    def setpoint(self, value):
        self.write(value)

    @property
    def value(self):
        return self.setread(**self.dft).value

    def __repr__(self):
        return 'Attribute({0})'.format(self.fullname)

    def __str__(self):
        return 'Attribute({0})'.format(self.fullname)


class AttributeGroup(DeviceGroup):
    """Create a group for reading an attribute on several devices.
    """

    def __init__(self, attrname, *devnames, **kwargs):
        """Create a group of attributes for fast access
            attrname:   Attribute name
            devnames:   Iterable for device names
        Keyword arguments:
            group:      group name (default: attrname)
            default:    default attribute value (default: None)
        """
        groupname = kwargs.pop('group', attrname)
        super(AttributeGroup, self).__init__(groupname, *devnames)
        self.attrname = attrname
        self.default = kwargs.pop('default', None)

    def setread(self, **kwargs):
        """return the attribute value as a named tuple with fields 'setpoint', 'value', 'time' and 'quality'
        Keyword arguments:
            default:    default attribute value (defaults to the value specified at instanciation)
            forward:    If it is set to true (the default) request is forwarded to subgroups.
                        Otherwise, it is only applied to the local set of devices
        """
        defval = kwargs.pop('default', self.default)
        return self.attr_get(self.attrname, default=defval, **kwargs)

    @property
    def setpoint(self):
        return self.attr_get(self.attrname, default=self.default).setpoint

    @setpoint.setter
    def setpoint(self, value):
        # noinspection PyUnresolvedReferences
        self.write_attribute(self.attrname, value, multi=(len(value) == self.get_size()))

    @property
    def value(self):
        return self.attr_get(self.attrname, default=self.default).value

    def get_attribute_list(self, **kwargs):
        """get the list of attribute names
        Keyword arguments:
            forward:    If it is set to true (the default) request is forwarded to subgroups.
                        Otherwise, it is only applied to the local set of devices
        """
        # noinspection PyUnresolvedReferences
        return ['/'.join((devname, self.attrname)) for devname in self.get_device_list(**kwargs)]

    def __repr__(self):
        # noinspection PyUnresolvedReferences
        return '{0}({1},{2})'.format(self.__class__.__name__, self.get_name(), self.attrname)


class VerboseDevice(Device):
    """Add a new method "verbose_command" for long commands which subscribes to
    2 events giving an information string while the device is moving"""

    class PrintCallback(object):
        def __init__(self, stream):
            self.stream = stream
            self.count = 0

        def push_event(self, evt):
            if (not evt.err) and (self.count > 0):
                message = evt.attr_value.value
                print(message, file=self.stream)
            self.count += 1

    def verbose_command(self, *args, **kwargs):
        """Supersedes the "command_inout" method by subscribing to the StdErr
        and StdOut events, sending the desired command, waiting for the
        termination of the MOVING state and unsubscribing to the event.

        verbose_command(self, cmd_name, cmd_param=None, **kwargs) -> any

        Keywords:
        stdout: file-like object used for standard output (default: sys.stdout)
        stderr: file-like object used for standard error (default: sys.stderr)
        ini: time interval between sending the command and checking the MOVING
             state (default 0)
        period: polling period (default 1)
        timeout: timeout in seconds (default 10)
        """
        def is_notmoving():
            return self.state() != DevState.MOVING

        stderr = kwargs.pop('stderr', sys.stderr)
        stdout = kwargs.pop('stdout', sys.stdout)
        period = kwargs.pop('period', 1)
        ini = kwargs.pop('ini', 0)
        timeout = kwargs.pop('timeout', 10)
        evterr = self.subscribe_event('StdErr', EventType.USER_EVENT, VerboseDevice.PrintCallback(stderr), [], True)
        evtout = self.subscribe_event('StdOut', EventType.USER_EVENT, VerboseDevice.PrintCallback(stdout), [], True)
        try:
            self.command_inout(*args)
            wait_for(is_notmoving, period=period, ini=ini, timeout=timeout)
        finally:
            self.unsubscribe_event(evtout)
            self.unsubscribe_event(evterr)
