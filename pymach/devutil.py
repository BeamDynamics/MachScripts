"""Utility functions for the Tango control system

wait4dev: wait until a condition is fulfilled
selectdev: return the 1st device satisfying a condition
"""
from __future__ import print_function, division, absolute_import   # for python2
import os
import csv
from .tango import DevState, Attribute


def is_state(device, state):
    return device.state() == state


def is_notstate(device, state):
    return device.state() != state


def is_on(device):
    return device.state() == DevState.ON


def is_notmoving(device):
    return device.state() != DevState.MOVING


def all_state(devgroup, state, **kwargs):
    return all(st == state for st in devgroup.state(**kwargs))


def all_notstate(devgroup, state, **kwargs):
    return all(st != state for st in devgroup.state(**kwargs))


def all_notmoving(devgroup, **kwargs):
    return all(st != DevState.MOVING for st in devgroup.state(**kwargs))


def all_on(devgroup, **kwargs):
    return all(st == DevState.ON for st in devgroup.state(**kwargs))


def appdata_file(fn):
    if fn[0] == '~':
        fn = os.getenv('APPHOME') + fn[1:]
    return fn


def read_permanent_values(filename):
    """Get attribute values from the file"""
    with open(appdata_file(filename), 'rb') as tabfile:
        prmreader = csv.reader(tabfile, delimiter='\t')
        return [(Attribute(row[0]), float(row[1])) for row in prmreader]


def store_permanent_values(filename, names):
    """Store attribute values in a file"""
    with open(appdata_file(filename), 'wt') as tabfile:
        [tabfile.write('{0}\t{1:.4f}\n'.format(name, Attribute(name).setpoint)) for name in names]
