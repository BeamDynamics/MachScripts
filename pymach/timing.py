from __future__ import print_function, division     # for python2
from builtins import range, object                  # for python2
from six import reraise
import sys
import time
import math
import numpy
from threading import Thread, Event

__author__ = 'L. Farvacque'
__all__ = ['timer', 'repeat', 'wait_for', 'WaitEvent', 'XThread', 'TimeoutError']


class TimeoutError(Exception):
    pass


def timer(timeout=0.0):
    def tm():
        return time.time() > tlim

    tlim = time.time() + timeout
    return tm


def timiter(timeout=0.0):
    tlim = time.time() + timeout
    remain = timeout
    while remain > 0:
        yield remain
        remain = tlim - time.time()


def repeat(timeout=10.0, period=1.0, ini=0.0, sleep=time.sleep, remain=False):
    """
    Iterator repeating at regular intervals and returning the elapsed time:
    :param timeout: timeout in seconds (default 10)
    :param period:  polling period in seconds (default 1)
    :param ini:  time before the first event (default 0)
    :param sleep: sleep function (default time.sleep)
    :return: iterator
    """
    t0 = time.time()
    for i in range(int(math.floor((timeout - ini) / period)) + 1):
        elapsed = ini + i * period
        slp = t0 + elapsed - time.time()
        if slp > 0:
            sleep(slp)
            yield (timeout - elapsed) if remain else elapsed


def wait_for(*args, **kwargs):
    """
    Wait until all conditions return True, throw a TimeoutError exception when
    the timeout is reached. Once a condition is reached, it will not be tested again.
    :param args: conditions, functions without input arguments returning True when the condition is reached
    :keyword ini: time before the loop starts
    :keyword period: polling period (default 1)
    :keyword timeout: timeout in seconds (default 10)
    :keyword fail: if 'fail' is True (default value), wait_for raises an exception on timeout. Otherwise, it returns
                   the number of still pending events
    :return: Number of pending events
    """
    conditions = numpy.array(args)
    fail = kwargs.pop('fail', True)
    pending = numpy.ones(conditions.shape, dtype=bool)
    for _ in repeat(**kwargs):
        pending[pending] = [not cond() for cond in conditions[pending]]
        if not any(pending):
            return 0
    nmiss = numpy.count_nonzero(pending)
    if fail:
        raise TimeoutError("Timeout waiting for {0} condition{1}".format(nmiss, "s"[nmiss == 1:]))
    return nmiss


class WaitEvent(object):
    """Event object suitable to be used as a Tango event callback

    In addition to the Python event, it can be called by Tango, and then
    it turns to the "set" state and records the Tngo event.

    the "wait" method returns the received Tango event. If the specified
    timeout is exceed, the behaviour depends on a "fail" keyword argument:
        fail is True (Default) : a TimeoutError is raised
        fail is False          : it returns None

    An additional callable with no input argument can be specified by the
    keyword "stop", either to the WaitEvent constructor or to the wait method.
    It is periodically called during the wait and if returns True, wait will
    exit with a None return value.
    """
    def __init__(self, stop=None, period=1.0, name='event'):
        """Create an Event object to be used as a Tango event callback

        :keyword: name='event'      event name
        :keyword: stop=stop_test    callable to check the end condition
        :keyword: period=30         periodicity of the "stop" check
        """
        self.evt = Event()
        self.period = period
        self.stop = stop
        self.tgevent = None
        self.__name__ = name

    def set(self):
        self.evt.set()

    def clear(self):
        self.tgevent = None
        self.evt.clear()

    def is_set(self):
        return self.evt.is_set()

    def __call__(self, *args):
        if len(args) > 0:
            self.push_event(args[0])
        else:
            self.evt.set()

    def push_event(self, tangoevent):
        if not tangoevent.err:
            self.tgevent = tangoevent
            self.evt.set()

    def wait(self, timeout=30.0, **kwargs):
        """Wait for a Tango event to be sent

        :keyword: stop=stop_test    callable to check the end condition
        :keyword: period=30         periodicity of the "stop" check
        :keyword fail=True          if 'fail' is True (default value), wait raises
                                    an exception on timeout. Otherwise, it returns None
        """
        stop = kwargs.pop('stop', self.stop)
        fail = kwargs.pop('fail', True)
        if stop is None:
            self.evt.wait(timeout)
            if self.evt.is_set():
                return self.tgevent
        else:
            period = kwargs.pop('period', self.period)
            for _ in repeat(timeout=timeout, period=period, sleep=self.evt.wait, **kwargs):
                if stop():
                    self.evt.set()
                if self.evt.is_set():
                    return self.tgevent
        if fail:
            raise TimeoutError('Timeout waiting for {0}'.format(self.__name__))
        return None


class XThread(Thread):
    """
    A thread for use in interactive sessions that can display its progress
    and report Exceptions

    - The "join_poll" method is similar to join with additional flags:

       "period": if not None, it periodically calls the "check" method of the target
                 object to display the progress (default None). If no "check" method
                 exists, a default animation is displayed

       "fail":   if True, any exception causing the thread to abort will be raised
                 in the calling thread after the thread finishes (default False)

    - The "join_poll" method catches the KeyboardInterrupt and calls the "set" method
      of the target if it exists

    """
    class _Symbol(object):
        """Function for displaying the progress of the thread"""
        _str = '\\|/- '

        def __init__(self):
            self.k = 0

        def __call__(self):
            print('\r{0}'.format(self._str[self.k]), end='')
            sys.stdout.flush()
            self.k += 1
            if self.k >= len(self._str):
                self.k = 0

    def __init__(self, *args, **kwargs):
        def nop():
            pass

        super(XThread, self).__init__(*args, **kwargs)
        self.error = None
        try:
            self.set = kwargs['target'].set
        except AttributeError:
            self.set = nop
        try:
            self.check = kwargs['target'].check
        except AttributeError:
            self.check = self._Symbol()

    def run(self):
        try:
            super(XThread, self).run()
        except (KeyboardInterrupt, Exception):
            self.error = sys.exc_info()

    def join_poll(self, timeout=None, period=None, fail=False):
        tlim = float('Inf') if timeout is None else time.time() + timeout
        try:
            if period is None:
                super(XThread, self).join(timeout)
            else:
                while self.is_alive() and time.time() < tlim:
                    self.check()
                    super(XThread, self).join(period)
        except KeyboardInterrupt:
            self.set()
            super(XThread, self).join(tlim - time.time())
        if not (self.is_alive() or self.error is None):
            try:
                if fail:
                    reraise(*self.error)
            finally:
                self.error = self.error[1]      # remove traceback to avoid circular references
