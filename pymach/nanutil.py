from builtins import range
from numpy import nan, asarray, isfinite, ones, ndenumerate

__all__ = ['nanstd', 'nanmean']


def nancompute(arr, defval, meth, axis, *args):
    if axis is None:
        res = defval
        ok = isfinite(arr)
        if ok.sum() > 0:
            res = getattr(arr[ok], meth)(None, *args)
    else:
        dims = list(range(axis)) + list(range(axis + 1, arr.ndim))
        res = defval * ones(asarray(arr.shape)[dims])
        brr = arr.transpose(dims + [axis])
        for indices, v in ndenumerate(res):
            vals = brr[indices]
            ok = isfinite(vals)
            if ok.sum() > 0:
                res[indices] = getattr(vals[ok], meth)(None, *args)
    return res


def nanstd(arr, axis=None):
    """Compute the standard deviation along the specified axis, after removing NaN values
    """
    return nancompute(arr, nan, "std", axis)


def nanmean(arr, axis=None):
    """Compute the mean value along the specified axis, after removing NaN values
    """
    return nancompute(arr, nan, "mean", axis)
