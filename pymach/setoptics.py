#!/usr/bin/env python
from __future__ import with_statement
from PyTango import Database,DeviceProxy
import numpy as np

def set_attributes(d,v):
    d.X_Beta_0,d.X_Alpha_0,d.X_Eta_0,d.X_Etap_0,d.Z_Beta_0,d.Z_Alpha_0,d.Z_Eta_0,d.Z_Etap_0=v

def get_properties(propname):
    keylist=['betax','alphax','etax','etapx','betaz','alphaz','etaz','etapz']
    db=Database()
    propdir=db.get_property(propname,keylist)
    return [float(propdir[key][0]) for key in keylist]

def fill1(devlist,propname):
    map(set_attributes,devlist,np.tile(get_properties(propname),(len(devlist),1)))

def fill2(devlist,fname):
#    with open(fname,'rU') as ff:
#	vals=[[float(s) for s in line.strip('\t\n').split('\t')]+[0,0] for line in ff if line[0]!='#']
    vals=np.loadtxt(fname)
    map(set_attributes,devlist,np.concatenate((vals,np.zeros((len(vals),2))),axis=1))

if __name__ == "__main__":
    celllist=range(4,33)+range(1,4)
    fill2([DeviceProxy('sr/opt-hd/c%02d'%i) for i in celllist],'d1.txt')
    fill2([DeviceProxy('sr/opt-sd/c%02d'%i) for i in celllist],'d2.txt')
    fill2([DeviceProxy('sr/opt-ss/c%02d'%i) for i in celllist],'id.txt')
#    fill1([DeviceProxy('sr/opt-hd/c%02d'%i) for i in celllist[1::2]],'optic-id25_1-emit')
#    fill1([DeviceProxy('sr/opt-sd/c%02d'%i) for i in celllist[1::2]],'optic-d9-emit')
#    fill1([DeviceProxy('sr/opt-ss/c%02d'%i) for i in celllist[0::2]],'optic-id30-emit')
