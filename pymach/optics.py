from __future__ import print_function
from numpy import r_, arange
import matfile
import os


def getmachenv(*args):
    path = ''
    mach = ''
    nin = 0
    if nin < len(args) and (not (args[nin] in ('sr', 'sy'))):
        path = args[nin]
        if path.find('/sr/') >= 0:
            mach = 'sr'
        elif path.find('/sy/') >= 0:
            mach = 'sy'
        nin += 1
    if nin < len(args) and args[nin] in ('sr', 'sy'):
        mach = args[nin]
        nin += 1
    if len(mach) == 0: mach = 'sr'
    if len(path) == 0:
        path = os.path.join(os.environ['APPHOME'], mach, 'optics', 'settings', 'theory')
    elif len(os.path.split(path)[0]) == 0:
        path = os.path.join(os.environ['APPHOME'], mach, 'optics', 'settings', path)
    return path, mach, args[nin:]


def load(*args):
    """(PARAMS,V1,V2,...)=LOAD(OPTICS,CODE1,CODE2,...) Load optics parameters
    
    	OPTICS	machine description: looks for:
    
     'sr' or 'sy':	machine name
    			path defaults to $(APPHOME)/mach/optics/settings/theory
     '/machfs/appdata/sr/optics/settings/theory'[,'sr']:
    			full path of optics directory, machine name
     'nombz2.5'[,'sr']:	optics name, machine name
     atstruct[,'sr']:	an Accelerator Toolbox structure, machine name
    
    	CODE1,CODE2... name of desired parameter table.
    		'bpm'		all bpms
    		'hbpm'		horizontal bpms
    		'vbpm'		vertical bpms
    		'steerh'	horizontal steerers
    		'steerv'	vertical steerers
    		'qp'		quadrupoles
    		'h_bpms'	flags the horizontal bpms
    		'v_bpms'	flags the vertical bpms
               numeric vector      code of selected parameters
    
    	For SR
    		'sx'		sextupoles
    		'cornq'		quadrupole correctors
    		'corsq'		skew quadrupole correctors
    		'pin25'		ID25 pinhole camera
    		'pin9'		D9 pinhole camera
    		'id'		middle of straight sections
    		'high'		middle of high beta straight sections
    		'low'		middle of low beta straight sections
    
    	PARAMS	structure containing the following fields
    		periods,nuh,nuv,ll,alpha,emitx,emitz,strname
    
          parameter code:
    
     [ 0    1       2       3       4       5       6       7       8       9   ]
     [Kl theta/2pi betax alphax  phix/2pi etax    eta'x   betaz   alphaz phiz/2pi]
    
          default parameters:
    
          [   1         2         4        5      7       9      ]
          [theta/2Pi, betax, Phix/2PiNux, etax, betaz, Phiz/2PiNux]
    """

    def hlrange(cx):
        if len(cx) == 0:
            return arange(32)
        else:
            cx2 = int(cx) - 4
            return cx2 + 32 * (cx2 < 0)

    path, mach, args = getmachenv(*args)
    oldfile = os.path.join(path, "matoptics.mat")
    newfile = os.path.join(path, "optics.mat")
    fileorder = r_[1:10, 0]
    locs = fileorder.copy()
    locs[fileorder] = arange(fileorder.size)
    if os.path.isfile(newfile):
        print("Using", newfile)
        pool = matfile.load(newfile)
    elif os.path.isfile(oldfile):
        print("Using", oldfile)
        pool = matfile.load(oldfile)
    else:
        if mach == 'sr':
            print("cannot find load_fsroptics")
        elif mach == 'sy':
            print("cannot find load_fsyoptics")
        else:
            print("cannot find machine")
        raise ValueError

    smach = pool['smach']
    params = locs[r_[1, 2, 4, 5, 7, 9]]
    out = [pool['pm']]
    for arg in args:
        if isinstance(arg, str):
            incode = arg.lower()
            vals = None
            if incode[1:] == 'bpm':
                if smach.has_key(incode[0] + "_bpms"):
                    vals = smach['bpm'][:, smach[incode[0] + "_bpms"]]
                else:
                    vals = smach['bpm']
            elif smach.has_key(incode):
                vals = smach[incode]
            elif incode == 'high':
                vals = smach['id'][:, arange(0, 32, 2)]
            elif incode == 'low':
                vals = smach['id'][:, arange(1, 32, 2)]
            elif incode[:2] == 'id':
                vals = smach['id'][:, hlrange(incode[2:])]
            elif incode[:4] == 'hilo':
                vals = smach['id'][:, hlrange(incode[4:])]
            if vals is None:
                out.append(vals)
            else:
                out.append(vals[params])
        else:
            pass
            params = locs[arg]
    return out
