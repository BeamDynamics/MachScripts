#!/usr/bin/env python

# see ~/.pydistutils.cgf for the defaulted final file destinations.
from distutils.core import setup, Extension
import os

import numpy


# noinspection PyShadowingNames
def matlab_extension(macharch):
    mathome = '/opt/matlab'
    extra_link_args = []
    if macharch == 'macosx':
        os.environ['MACOSX_DEPLOYMENT_TARGET'] = '10.10'  # Force 10.4 options (otherwise 10.3)
        matlab_libraries = [os.path.join(mathome, 'bin/maci64'), os.path.join(mathome, 'sys/os/maci64'), ]
    elif macharch == 'centos5_64':
        matlab_libraries = [os.path.join(mathome, 'bin/glnxa64'), os.path.join(mathome, 'sys/os/glnxa64'), ]
        extra_link_args.append(','.join(('-Wl', '-rpath', os.path.join(mathome, 'bin/glnxa64'))))
    elif macharch == 'debian6_64':
        matlab_libraries = [os.path.join(mathome, 'bin/glnxa64'), os.path.join(mathome, 'sys/os/glnxa64'), ]
        extra_link_args.append(','.join(('-Wl', '-rpath', os.path.join(mathome, 'bin/glnxa64'))))
    elif macharch == 'debian7_64':
        mathome = '/sware/com/matlab_2014b'
        matlab_libraries = [os.path.join(mathome, 'bin/glnxa64'), os.path.join(mathome, 'sys/os/glnxa64'), ]
        extra_link_args.append(','.join(('-Wl', '-rpath', os.path.join(mathome, 'bin/glnxa64'))))
    elif macharch == 'debian8':
        mathome = '/sware/com/matlab_2015b'
        matlab_libraries = [os.path.join(mathome, 'bin/glnxa64'), os.path.join(mathome, 'sys/os/glnxa64'), ]
        extra_link_args.append(','.join(('-Wl', '-rpath', os.path.join(mathome, 'bin/glnxa64'))))
    else:
        matlab_libraries = [os.path.join(mathome, 'bin/glnx86'), os.path.join(mathome, 'sys/os/glnx86'), ]
        extra_link_args.append(','.join(('-Wl', '-rpath', os.path.join(mathome, 'bin/glnx86'))))
    return Extension('_matfile', ['matfile.cpp', ],
                     define_macros=[('DEBUG', '0'), ],
                     include_dirs=[numpy.get_include(), os.path.join(mathome, 'extern/include')],
                     libraries=['mat', ],
                     extra_link_args=extra_link_args,
                     library_dirs=matlab_libraries)


macharch = os.environ['MACHARCH']
# setup.py will install files in the directories indicated in the file:
# "~/.pydistutils.cfg"
#
# [install]
# #prefix=/operation/machine
# install-base=/operation/machine
# install-purelib=/operation/machine/lib/python$py_version_short/site-packages
# install-platlib=/operation/machine/lib/$MACHARCH/python$py_version_short/site-packages
# install-scripts=/operation/machine/bin/$MACHARCH
# install-data=/operation/machine/data
# install-headers=/operation/machine/include

# how to run this script from CTRM:
# commit changes in the MachScripts repository,
# push to repository
# ssh beamdyn@raki
# cd ~/dev/MachScripts
# git pull
# python setup.py install

if __name__ == "__main__":
    setup(name='pymach',
          version='1.0',
          description='Procedures for machine control',
          author='Beam dynamics group',
          author_email='laurent.farvacque@esrf.fr',
          packages=['notifier', 'pymach', 'ebs', 'sr', 'sy'],
          #                                         Pure python modules
          py_modules=['atmodel', 'topuploop', 'fill78', 'fillstd', 'fillhybrid', 'matengine',
                      'mxarray', 'mfile', 'subcommand', 'setbump',
                      'srf', 'srfill', 'srinj', 'syinj', 'syext','beam_test'],
          #                                         Extension (compiled) modules
          # ext_modules=[matlab_extension(macharch),],
          #                                         Scripts (made executable and installed in /bin
          scripts=['scripts/sr.respmat', 'scripts/sr.setoptics', 'scripts/sr.profile',
                   'scripts/ebs.respmat', 'scripts/ebs.setoptics','scripts/sr_test',
                   'fill78.py', 'fillhybrid.py', 'fillstd.py', 'setbump.py', 'resetBPI',
                   'srinj.py', 'syinj.py', 'syext.py', 'topuploop.py'],

          requires=['PyTaco', 'tango', 'numpy', 'scipy', 'matplotlib', 'hdf5storage']
          )
