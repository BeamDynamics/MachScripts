from __future__ import print_function   # for python2
from builtins import range, object      # for python2
import sys
import os
from ctypes import *
from mxarray import mxArray

__author__ = 'L. Farvacque'
__all__ = ['MatFile', 'loadmat', 'savemat']

suffix = dict(darwin='.dylib', linux2='.so')[sys.platform]
prefix = dict(darwin='/opt/matlab/bin/maci64/', linux2='/opt/matlab/bin/glnxa64/')[sys.platform]
try:
    libmat = CDLL(''.join(('libmat', suffix)))
except OSError:
    libmat = CDLL(''.join((prefix, 'libmat', suffix)))

class Utf8ifier(object):
    @classmethod
    def from_param(cls, value):
        return value.encode('utf-8', 'surrogateescape')

def utf8decoder(value, func, args):
    return value.decode('utf-8', 'surrogateescape')

c_mxArray_p = c_void_p

matOpen = libmat.matOpen
matOpen.argtypes = (Utf8ifier, Utf8ifier)
matOpen.restype = c_void_p
matClose = libmat.matClose
matClose.argtypes = (c_void_p,)
matClose.restype = c_int
matGetDir = libmat.matGetDir
matGetDir.argtypes = (c_void_p, POINTER(c_int))
matGetDir.restype = POINTER(c_char_p)
matGetVariable = libmat.matGetVariable
matGetVariable.argtypes = (c_void_p, Utf8ifier)
matGetVariable.restype = c_mxArray_p
matPutVariable = libmat.matPutVariable
matPutVariable.argtypes = (c_void_p, Utf8ifier, c_mxArray_p)
matPutVariable.restype = c_int


class MatFile(object):
    _oned = dict(column=1, row=2)
    _matversion = {'4': 'w4', '5': 'w', '7.3': 'w7.3'}

    def __init__(self, fname, mode='r', oned_as='row', appendmat=True):
        """Open a .mat file.
        mode may be 'r', 'u', 'w', 'w4', 'wz', 'w7.3'
        """
        if appendmat and not fname.endswith('.mat'):
            fname += '.mat'
        self.fname = fname
        self.oned_as = oned_as
        self.variables = set()
        self.mfile = None
        self.open(mode)

    def __del__(self):
        self.close()

    def __iter__(self):
        """x.__iter__() <==> iter(x)"""
        return self.variables.__iter__()

    def __contains__(self, key):
        """x.__contains__(k) -> True if x has a key k, else False"""
        return self.variables.__contains__(key)

    def __getitem__(self, key):
        """x.__getitem__(y) <==> x[y]"""
        # return _matfile.matread(self.mfile, key)
        mxarr = self.getmx(key)
        return mxarr.topy()

    def __setitem__(self, item, value):
        """x.__setitem__(i, y) <==> x[i]=y"""
        self.update(((item, value),))

    def __delitem__(self, item):
        """x.__delitem__(y) <==> del x[y]"""
        # _matfile.matdelete(self.mfile, item)
        self.variables.remove(item)

    def getmx(self, key):
        mxarray = matGetVariable(self.mfile, key)
        if mxarray is not None:
            return mxArray.fromfile(mxarray)
        else:
            raise KeyError('Variable {0} is unknown'.format(key))

    def get(self, *args):
        try:
            return self[args[0]]
        except KeyError:
            if len(args) >= 2:
                return args[1]
            else:
                raise

    def keys(self):
        """x.keys() -> a set-like object providing a view on D's keys"""
        return set(self)

    def update(self, *args, **kwargs):
        """x.update([E, ]**F) -> None.  Update x from dict/iterable E and F.
        If E is present and has a .keys() method, then does:  for k in E: x[k] = E[k]
        If E is present and lacks a .keys() method, then does:  for k, v in E: x[k] = v
        In either case, this is followed by: for k in F:  x[k] = F[k]
        """
        oned_as = kwargs.pop('oned_as', self.oned_as)
        if len(args) > 1:
            raise TypeError("MatFile.update() takes at most 1 argument ({0} given)".format(len(args)))
        elif len(args) == 1:
            kwargs.update(args[0])
        for key in kwargs:
            print('Creating variable', key)
            mxarr = mxArray.fromobject(kwargs[key], oned_as=oned_as)  # mxarray must be kept
            print('mxarr:', mxarr)
            res = matPutVariable(self.mfile, key, mxarr.mxarray)             # while its pointer is used
            # _matfile.matwritearray(self.mfile, key, keywords[key], oned_as)
            print('ok?', res)
            if res == 0:
                self.variables.add(key)
            else:
                raise OSError(os.strerror(res))

    def __repr__(self):
        if self.mfile is not None:
            return "{0}('{1}') with variables: {2}".\
                format(self.__class__.__name__, self.fname, ' '.join(self.variables))
        else:
            return "{0}('{1}') closed".format(self.__class__.__name__, self.fname)

    def _iti(self):
        for i in self.variables:
            yield (i, self[i])

    def _ita(self):
        for i in self.variables:
            yield self[i]

    def iterkeys(self):
        """x.iterkeys() -> an iterator over the keys of x"""
        return self.variables.__iter__()

    def iteritems(self):
        """x.iteritems() -> an iterator over the (key, value) items of x"""
        return self._iti()

    def itervalues(self):
        """x.itervalues() -> an iterator over the values of x"""
        return self._ita()

    def open(self, mode='r'):
        self.mfile = matOpen(self.fname, mode)
        if self.mfile is None:
            raise IOError("Cannot open file {0}.".format(self.fname))
        nvars = c_int()
        varnames = matGetDir(self.mfile, byref(nvars))
        # self.variables=set(unicode(varnames[i]) for i in range(nvars.value))
        self.variables=set(varnames[i].decode('utf-8', 'surrogateescape') for i in range(nvars.value))

    def close(self):
        if self.mfile is not None:
            ok = matClose(self.mfile)
            print('close', self.fname, ':', ok)
        self.mfile = None
        self.variables.clear()


def loadmat(file_name, mdict=dict(), appendmat=True):
    """loadmat(file_name, mdict=None, appendmat=True, **keywords)
    Load Matlab(tm) file

    file_name : string
       Name of the mat file (.mat extension not needed if appendmat==True)
    mdict : dict, optional
        dictionary in which to insert matfile variables
    appendmat : {True, False} optional
       True to append the .mat extension to the end of the given
       filename, if not already present
    """
    m = MatFile(file_name, appendmat=appendmat)
    mdict.update(m)
    return mdict


def savemat(file_name, mdict, appendmat=True, format='5', do_compression=False, oned_as='row', **kwargs):
    """Save a dictionary of names and arrays into a MATLAB-style .mat file.

    This saves the array objects in the given dictionary to a MATLAB-
    style .mat file.

    Parameters
    ----------
    file_name : str or file-like object
        Name of the .mat file (.mat extension not needed if ``appendmat == True``).
    mdict : dict
        Dictionary from which to save matfile variables.
    appendmat : bool, optional
        True (the default) to append the .mat extension to the end of the
        given filename, if not already present.
    format : {'5', '4', '7.3'}, string, optional
        '4'   for MATLAB 4 .mat files,
        '5'   (the default) for MATLAB 5 and up (to 7.2),
        '7.3' HDF-5 based format availble from MATLAB >= 7.3.
    do_compression : bool, optional
        Whether or not to compress matrices on write.  Default is False.
    oned_as : {'row', 'column'}, optional
        If 'column', write 1-D numpy arrays as column vectors.
        If 'row', write 1-D numpy arrays as row vectors.
    """
    mode = MatFile._matversion[format] + 'z'[not do_compression:]
    m = MatFile(file_name, mode=mode, oned_as=oned_as, appendmat=appendmat)
    m.update(mdict, **kwargs)
