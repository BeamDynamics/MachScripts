from __future__ import print_function, division
from matlab.engine import start_matlab

_futeng = None

def start_engine():
    global _futeng
    if _futeng is None:
        _futeng = start_matlab(async=True)

def get_engine():
    global _futeng
    start_engine()
    return _futeng.result()