#!/usr/bin/env python
"""Control the top-up operation

Usage: topuploop [options] ON|OFF|QUERY

Options:
--clean
--noclean
-i current, --current=current
-t period, --period=period
--mode=mode
"""

from __future__ import print_function
from builtins import str
import sys
from functools import partial
from io import StringIO

from pymach.tango import Device, DevState
from pymach.devutil import is_notstate
from pymach.timing import wait_for

__version__ = '1.1'


# noinspection PyShadowingNames
def run_macro(door, *args):
    door.command_inout('RunMacro', args)
    wait_for(partial(is_notstate, door, DevState.RUNNING), timeout=1, period=0.5, ini=0.1)


# noinspection PyShadowingNames
def status(door=None, stdout=sys.stdout):
    door = door or Device('sys/door-topup/com')
    buf = StringIO()
    run_macro(door, 'GetTopupEnv')
    cron = Device('sys/countdowncron/01')
    print(u'       State : {0}'.format(cron.state()), file=buf)
    print(u'     Counter : {0[0]}'.format(cron.get_property('CountDownName')['CountDownName']), file=buf)
    v = eval(door.read_attribute('Info').value[0])
    [print(u'{0:>20s} : {1}'.format(key, v[key]), file=buf) for key in v if key[0] != '_']
    print(buf.getvalue(), end='', file=stdout)
    buf.close()


# noinspection PyShadowingNames
def on(door=None):
    run_macro(door, 'StartTopUp')


# noinspection PyShadowingNames
def off(door=None):
    run_macro(door, 'StopTopUp')


# noinspection PyShadowingNames
def reset(door=None):
    run_macro(door, 'ResetStatistics')


if __name__ == "__main__":
    import argparse

    do = dict(on=on, off=off, status=status, reset=reset)
    parser = argparse.ArgumentParser(description='Control the topup sequence')
    parser.add_argument('-v', '--version', action='version', version=__version__)
    parser.add_argument('-i', '--current', type=float, help='topup current [mA]')
    parser.add_argument('-t', '--period', type=float, help='topup periodicity [mn]')
    parser.add_argument('--sb-current', type=float, dest='sb_current', help='topup current [mA]')
    parser.add_argument('--clean', action="store_true", dest='clean', default=None, help='Bunch cleaning')
    parser.add_argument('--noclean', action="store_false", dest='clean', help='No bunch cleaning')
    parser.add_argument('--enable-bpss', action="store_true", dest='enable_bpss', default=None, help='Act on BPSS')
    parser.add_argument('--disable-bpss', action="store_false", dest='enable_bpss', help='Ignore BPSS')
    parser.add_argument('--enable-rips', action="store_true", dest='enable_rips', default=None, help='Act on RIPS')
    parser.add_argument('--disable-rips', action="store_false", dest='enable_rips', help='Ignore BPSS')
    parser.add_argument('--enable-syrf', action="store_true", dest='enable_syrf', default=None, help='Act on SYRF')
    parser.add_argument('--disable-syrf', action="store_false", dest='enable_syrf', help='Ignore SYRF')
    parser.add_argument('--enable-syinj', action="store_true", dest='enable_syinj', default=None,
                        help='Act on SY Injection')
    parser.add_argument('--disable-syinj', action="store_false", dest='enable_syinj', help='Ignore SY Injection')
    parser.add_argument('--enable-sycleaning', action="store_true", dest='enable_sycleaning', default=None,
                        help='Booster cleaning')
    parser.add_argument('--disable-sycleaning', action="store_false", dest='enable_sycleaning',
                        help='No booster cleaning')
    # parser.add_argument('--enable_ids', action="store_true", dest='enable_ids', default=None, help='Act on IDs')
    # parser.add_argument('--disable_ids', action="store_false", dest='enable_ids', help='Ignore IDs')
    parser.add_argument('--enable-scrapers', action="store_true", dest='enable_scrapers', default=None,
                        help='Act on SR scrapers')
    parser.add_argument('--disable-scrapers', action="store_false", dest='enable_scrapers', help='Ignore SR scrapers')
    parser.add_argument('--mode', dest='mode', choices=['7/8+1', 'hybrid', 'other'], help="Filling mode")
    parser.add_argument('--count-success', type=int, help="Set success counter")
    parser.add_argument('--count-failure', type=int, help="Set failure counter")
    parser.add_argument('command', nargs='?', default=None, help='Command {on, off, status, reset}')
    args = parser.parse_args()

    # doors = [DeviceProxy(name) for name in ['sys/door-topup/01', 'sys/door-topup/02']]
    # door = selectdev(doors, isnotrunning, timeout=100)
    door = Device('sys/door-topup/com')
    if args.current is not None:
        run_macro(door, 'SetFloatEnv', 'current_ref', str(args.current))
    if args.sb_current is not None:
        run_macro(door, 'SetFloatEnv', 'SbCurrent', str(args.sb_current))
    if args.period is not None:
        run_macro(door, 'SetIntegerEnv', 'period', str(int(args.period)))
    if args.clean is not None:
        run_macro(door, 'SetBooleanEnv', 'clean', str(args.clean))
    if args.enable_bpss is not None:
        run_macro(door, 'SetBooleanEnv', 'EnableBpss', str(args.enable_bpss))
    if args.enable_rips is not None:
        run_macro(door, 'SetBooleanEnv', 'EnableRips', str(args.enable_rips))
    if args.enable_syrf is not None:
        run_macro(door, 'SetBooleanEnv', 'EnableSyrf', str(args.enable_syrf))
    if args.enable_syinj is not None:
        run_macro(door, 'SetBooleanEnv', 'EnableSYInj', str(args.enable_syinj))
    if args.enable_sycleaning is not None:
        run_macro(door, 'SetBooleanEnv', 'EnableSYCleaning', str(args.enable_sycleaning))
    # if args.enable_ids is not None:
    #     run_macro(door, 'SetBooleanEnv', 'EnableIDs', str(args.enable_ids))
    if args.enable_scrapers is not None:
        run_macro(door, 'SetBooleanEnv', 'EnableScrapers', str(args.enable_scrapers))
    if args.mode is not None:
        run_macro(door, 'SetMode', args.mode)
    if args.count_success is not None:
        run_macro(door, 'SetIntegerEnv', 'Success', str(args.count_success))
    if args.count_failure is not None:
        run_macro(door, 'SetIntegerEnv', 'Failure', str(args.count_failure))

    if args.command is not None:
        command = args.command.lower()
        try:
            do[command](door)
        except KeyError:
            print('Unknown command', command, file=sys.stderr)
