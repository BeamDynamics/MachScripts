#!/usr/bin/python
"""Measure the filling pattern in 3 slices

Usage: profile.py [options]

Options:
  --version             show program's version number and exit
  -h, --help            show this help message and exit
  -t INTEGRATION_TIME, --integration-time=INTEGRATION_TIME
                        Counter integration time/slice [s] (default: 60.0)
  -n BUCKET_NUMBER, --bucket-number=BUCKET_NUMBER
                        Position of 1st slice (default: 0)
  -f FILENAME, --filename=FILENAME
                        file name (default: /operation/appdata/topup/refill.mat)
"""

from __future__ import print_function, division
# noinspection PyUnresolvedReferences
from builtins import *      # "future" package for Python3 compatibility
import datetime
import math
import itertools
import os
import sys
import time

import numpy as np
from scipy.io import loadmat, savemat

from pymach.tango import Device, DevSource
from pymach.timing import XThread
# noinspection PyUnresolvedReferences
from sr.purity import PurityDevice, __author__, __version__

__all__ = ['test_filling_pattern', 'load_filling_pattern', 'store_filling_pattern', 'bunch_list']

_FILLING_PROFILE_NAME = os.path.join(os.environ['APPHOME'], 'topup', 'refill.mat')


class _BunchList(dict):
    @staticmethod
    def setmask(length, index):
        a = np.zeros(length, dtype=bool)
        a[index] = True
        return a

    def __init__(self):
        super(_BunchList, self).__init__(self)
        self['uniform'] = np.ones(992, dtype=bool)
        self['16b'] = self.setmask(992, slice(0, None, 62))
        self['7/8+1'] = self.setmask(992, slice(63, 930))   # no single nor markers
        self['hybrid'] = self.setmask(992, list(itertools.chain(*(range(i, i+8) for i in range(148, 862, 31)))))

    def __call__(self, mode, sb_position=0):
        return np.roll(self[mode], sb_position)


def test_filling_pattern(filpat, mode, sb_position=0):
    """Compute the homogeneity of a filling pattern

    :param filpat:      "Filling pattern" dictionary as returned by "load_filling_pattern":
    :param mode:        Filling mode ('uniform'|'16b'|'7/8+1'|'hybrid')
    :param sb_position: single bunch position
    :return: Standard deviation of the normalized bunch population
    """
    vals = filpat['profile'][bunch_list(mode, sb_position)]
    sx2 = np.sum(vals*vals)
    sx = np.sum(vals)
    return math.sqrt(vals.size*sx2/sx/sx - 1)

def bunch_current_from_scope(filename):
    bct =  Device('sr/d-cpb/1', source=DevSource.DEV)
    pct = Device('sr/d-ct/1', source=DevSource.DEV)
    ict = Device('sr/d-ct/ict1', source=DevSource.DEV)
    pct1 = pct.Current
    ict1 = ict.Current
    profile = dict(profile=bct.Currents)
    profile.update(pct=0.5 * (pct1 + pct.Current), ict=0.5 * (ict1 + ict.Current))
    if filename is not None:
        if not filename:
            filename = os.path.join(os.environ['APPHOME'], 'sr', 'filling_pattern',
                                    'profile_{0:%Y%m%d_%H%M%S}.mat'.format(datetime.datetime.today()))
        print(filename)
        savemat(filename, profile, oned_as='column', appendmat=False)
        profile.update(filename=filename)
    return profile




def load_filling_pattern(filename, max_age=float('Inf'), min_current=-float('Inf')):
    """Read a filling pattern record, optionally checks its date and current

    :param filename:    name of a Matlab file recorded by the
                        "store_filling_pattern" function
    :param max_age:     raise an exception if the file is older than max_age [s]
    :param min_current: raise an exception if the total current is less than min_current [mA]
    :return:            "Filling_pattern" dictionary, with fields:
     'profile':     normalized filling pattern
     'pct':         total current
     'ict':         single bunch current
     'total_count': total count
     'date':        measurement date
    """
    date = os.stat(filename).st_mtime
    delay = time.time() - date
    if delay > max_age:
        raise RuntimeError('Filling pattern too old: {0:.0f}/{1:.0f} s'.format(delay, max_age))
    mfile = loadmat(filename, squeeze_me=True, struct_as_record=True)
    pct = float(mfile['pct'])  # for debian7 (type =numpy.ndarray)
    if pct < min_current:
        raise RuntimeError('Measurement current too low: {0:.3f}/{1:.3f} mA'.format(pct, min_current))
    ict = float(mfile['ict'])  # for debian7 (type =numpy.ndarray)
    return dict(profile=mfile['profile'], pct=pct, ict=ict, total_count=mfile['total_count'], date=date)


def store_filling_pattern(filename=None,use_scope=False, **kwargs):
    """Measure and optionally store the filling pattern

    :param filename:    path of a matlab file to be created with variables 'profile', 'pct', 'ict' and 'total_count'
                        if None, no file is vreated
                        if empty , the filename is automatically generated
    :param integration_time: integration time for each of the 3 slices (default: 60)
    :param bucket_number: center of the 1st slice (default: 0)
    :return:            "Filling_pattern" dictionary, with fields:
     'profile':         normalized filling pattern
     'pct':             total current
     'ict':             single bunch current
     'total_count':     total count
    """
    if use_scope:
      return  bunch_current_from_scope(filename)
    else:
      return _PurityMeasure('sr/d-purity/d19')(filename=filename, **kwargs)


class _PurityMeasure(PurityDevice):
    def __init__(self, *args, **kwargs):
        PurityDevice.__init__(self, *args, **kwargs)
        self.t0 = time.time()
        self.tlim = 0

    def __call__(self, **kwargs):
        filename = kwargs.pop('filename', None)
        kwargs.pop('use_scope',None)
        self.t0 = time.time()
        self.tlim = 3 * (kwargs.get('integration_time', 60) + 6) + 14
        pct = Device('sr/d-ct/1', source=DevSource.DEV)
        ict = Device('sr/d-ct/ict1', source=DevSource.DEV)
        pct1 = pct.Current
        ict1 = ict.Current
        profile = self.filling_pattern(**kwargs)
        profile.update(pct=0.5 * (pct1 + pct.Current), ict=0.5 * (ict1 + ict.Current))
        if filename is not None:
            if not filename:
                filename = os.path.join(os.environ['APPHOME'], 'sr', 'filling_pattern',
                                        'profile_{0:%Y%m%d_%H%M%S}.mat'.format(datetime.datetime.today()))
            print(filename)
            savemat(filename, profile, oned_as='column', appendmat=False)
            profile.update(filename=filename)
        print(filename)
        return profile

    def check(self):
        print('\r{0:3.0f}/{1:<3.0f}'.format(time.time() - self.t0, self.tlim), end='')
        sys.stdout.flush()


bunch_list = _BunchList()

if __name__ == "__main__":
    import argparse

    # parser = argparse.ArgumentParser(prog='tgread',description='Read a Tango attribute')
    # parser.add_argument('attributename',nargs='+',help='Attribute_name')
    # parser.add_argument('-f',nargs='?',help='Format output')
    # parser.add_argument('-t',nargs='?',help='Polling period [s]')

    parser = argparse.ArgumentParser(description="Measure the filling pattern in 3 slices")
    parser.add_argument('-v', '--version', action='version', version=__version__)
    parser.add_argument('-t', '--integration-time', type=float, default=60.0,
                        help='Counter integration time/slice [s] (default: %(default)s)')
    parser.add_argument('-n', '--bucket-number', type=int, default=0,
                        help='Position of 1st slice (default: %(default)s)')
    parser.add_argument('-f', '--filename', type=str, default='',
                        help='file name (default: automatically generated)')
    parser.add_argument('-s', '--use_scope', type=bool, default=False,
                        help='Use scope instead of APD')
    arguments = parser.parse_args()

    if not arguments.use_scope:
      tlim = 3 * (arguments.integration_time + 6) + 12
      print('Measuring filling pattern for {0} s'.format(tlim))
      th = XThread(name="Pattern", target=_PurityMeasure('sr/d-purity/d19'), kwargs=vars(arguments))
      print()
      try:
          th.start()
          th.join_poll(period=1, fail=True)
      except KeyboardInterrupt as err:
          print('Pattern measurement failed: {0}'.format(err), file=sys.stderr)
      else:
          print('Pattern measurement succeeded')
    else:
      store_filling_pattern(filename=arguments.filename,use_scope=True)
