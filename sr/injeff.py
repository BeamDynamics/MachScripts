from __future__ import print_function, division
from pymach.tango import Device, EventType
from pymach.timing import WaitEvent
from time import sleep


class InjEffDevice(Device):
    #
    # def __del__(self):
    #     print("deleting device")
    #     super(InjEffDevice, self).__del__()

    def get_new(self, Averaging=1, Trigger=None):
        """
        Return the injection efficiency
        :param Averaging: number of measurements
        :param Trigger: Function for triggering an injection (optional)
        :return: injection efficiency
        """
        evt = WaitEvent(name='injeff')
        evtid = self.subscribe_event('InjectionEfficiency', EventType.CHANGE_EVENT, evt, [], False)
        vsum = 0.0
        try:
            for _ in range(Averaging):
                evt.clear()
                if Trigger:
                    Trigger()
                tgevent = evt.wait(timeout=10)
                vsum += tgevent.attr_value.value
                sleep(1.0)  # wait before the next trigger (Kicker bug)
        finally:
            self.unsubscribe_event(evtid)
        return vsum/float(Averaging)


class TrigTest(object):
    def On(self):
        print("Trigger sent")
