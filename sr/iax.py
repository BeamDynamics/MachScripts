from __future__ import print_function, division, unicode_literals, absolute_import   # for python2
import numpy as np
from pymach.tango import AttributeGroup


class IaxPosition(AttributeGroup):
    """Groups all the IAX monitors for position access.
    """
    celllist = ('c5', 'c10', 'c11', 'c14', 'c18', 'c21', 'c25', 'c26', 'c29', 'c31', 'c3')

    def __init__(self, device_names=(), **kwargs):
        """Groups all iax vertical position measurements for fast access
            device_names:   Iterable for device names, default to all iaxs
        Keyword arguments:
            group:      tango group name (default: iaxPosition)
            default:    default attribute value (default: NaN)
        """
        group = kwargs.pop('group', 'iaxPosition')
        default = kwargs.pop('default', np.nan)
        dn = device_names or ('sr/d-emit/%s' % cellnum for cellnum in self.celllist)
        super(IaxPosition, self).__init__('ZPeakPosition', dn, group=group, default=default)


class IaxEmittance(AttributeGroup):
    """Groups all the IAX monitors for emittance access.
    """
    celllist = ('c5', 'c10', 'c11', 'c14', 'c18', 'c21', 'c25', 'c26', 'c29', 'c31', 'c3', 'd9', 'id25', 'd11')

    def __init__(self, device_names=(), **kwargs):
        """Groups all emittance measurements for fast access
            device_names:   Iterable for device names, default to all iax and pinhole cameras
        Keyword arguments:
            group:      tango group name (default: iaxEmittance)
            default:    default attribute value (default: NaN)
        """
        group = kwargs.pop('group', 'iaxEmittance')
        default = kwargs.pop('default', np.nan)
        dn = device_names or ('sr/d-emit/%s' % cellnum for cellnum in self.celllist)  # expand iterator
        super(IaxEmittance, self).__init__('ZEmittance', dn, group=group, default=default)
