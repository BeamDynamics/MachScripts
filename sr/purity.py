from __future__ import print_function, division
import time
from pymach.tango import Device
import numpy as np

__author__ = 'L. Farvacque'
__version__ = '1.0'


# noinspection PyPep8Naming
class PurityDevice(Device):
    """Bunch purity device
    add a "Profile" property
    add filling_pattern(integration_time=60, bucket_ini=0) method
    """

    def getnormcount(self):
        """beam profile [counts/s]"""
        cnt = self.BunchCount
        base = self.BucketNumber + self.BunchCountOriginOffset
        #bindex = np.mod((base - np.arange(5, cnt.size - 6)), 992)
        bindex = np.mod((base - np.arange(0, cnt.size)), 992)
        #bcount = cnt[5:-6]
        bcount = cnt
        return bcount, bindex, self.RealTime

    @property
    def Profile(self):
        """Get the bunch population normalized to 1"""
        bcount, bindex, duration = self.getnormcount()
        cnt = np.empty(992)
        cnt.fill(np.NaN)
        cnt[bindex] = bcount / bcount.sum()
        return cnt

    def filling_pattern(self, integration_time=60, bucket_number=0):
        """Measures the filling pattern in 3 slices and return a dictionary:
        'profile':      normalized profile
        'total_count':  array of total counts for each slice"""
        self.clear()
        blist = np.mod(np.array([0, 330, 660], dtype=np.int32) + bucket_number, 992)
        # Cut at 175, 505, 837
        self.StartMeasure()
        time.sleep(7)  # for diode motion
        self.StopCounting()
        time.sleep(7)  # for diode motion
        profile = np.empty(992)
        profile.fill(np.NaN)
        totcount = []
        tott = []
        try:
            for bucket in blist:
                # noinspection PyAttributeOutsideInit
                self.BucketNumber = bucket
                self.ClearMCAMemory()
                time.sleep(2.0)  # for clear to be effective (?)
                self.StartCounting()
                self.sleep(integration_time)
                self.StopCounting()
                time.sleep(3.0)  # for polling
                bc, bi, dt = self.getnormcount()
                profile[bi] = bc / dt
                totcount.append(np.sum(bc))
                tott.append(dt)
        except KeyboardInterrupt:
            self.StopCounting()
            raise
        finally:
            self.StopMeasure()
        print(profile,np.sum(profile))
        return dict(profile=profile / np.sum(profile), total_count=np.array(totcount), duration=np.array(tott))
