from __future__ import print_function, division
from atmodel import ATModel


class Model(ATModel):
    """Access to optical values
    
    Available locations:
    
    bpm:       All BPMs
    qp:        All quadrupoles
    sx:        All sextpoles
    steerh:    Horizontal steerers
    steerv:    Vertical steerers
    cornq:     Normal quadrupole correctors
    corsq:     Skew quadrupole correctors
    corsx:     Sextupole correctors
    iax:       In-air X-Ray monitors
    iax_c<x>   A single iax
    pin_d9:	Pinhole D9
    pin_d11:	Pinhole D11
    pin_id25:	Pinhole ID25
    id:        All IDs
    high:      High beta straights
    low:       Low beta straights
    bmsoft:    Soft bending magnet sources
    bmhard:    Hard bending magnet sources
    id<x>:     ID cell x
    bmsoft<x>: Soft bending magnet cell x
    bmhard<x>: Hard bending magnet cell x
    
    Available parameters:
    
    0:     index in the AT structure
    1:     Integrated strength
    2:     theta/2pi
    3:     betax
    4:     alphax
    5:     phix/2pi/nux
    6:     etax
    7:     eta'x
    8:     betaz
    9:     alphaz
    10:    phiz/2pi/nux
    11:    etaz
    12:    eta'z
    
    Default parameters:
    
    [   2         3         5        6      8       10      ]
    [theta/2Pi, betax, Phix/2PiNux, etax, betaz, Phiz/2PiNux]
    """

    def __init__(self, *args):
        """
        m = sr.model()              take the lattice from
            $APPHOME/sr/optics/settings/theory/betamodel.mat

        m = sr.model('opticsname')  take the lattice from
            $APPHOME/sr/optics/settings/opticsname/betamodel.mat

        m = sr.model(path)          take the lattice from
            path or path/betamodel.mat or path/betamodel.str
       """
        super(Model, self).__init__(self, *args)
        self.model = self.eng.sr.model(*args)
