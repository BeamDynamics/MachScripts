#!/usr/bin/env python
"""Read a Tango attribute"""
# import argparse
from __future__ import print_function   # for python2
from builtins import str                # for python2
import optparse
from time import sleep
import PyTango


def configure_device(fname, fmtflag):
    devname, attrname = fname.rsplit('/', 1)
    dev = PyTango.DeviceProxy(devname)
    attrinfo = dev.attribute_query(attrname)
    if fmtflag:
        try:
            mult = float(attrinfo.display_unit)
        except:
            mult = 1
        ff = lambda v: (attrinfo.format + ' ' + attrinfo.unit) % (mult * v,)
    else:
        ff = lambda v: str(v)
    # ff=lambda v:'%g' % v
    if attrinfo.writable == PyTango.AttrWriteType.READ:
        gg = lambda attr: ('', ff(attr.value))
    elif attrinfo.writable == PyTango.AttrWriteType.WRITE:
        gg = lambda attr: (ff(attr.w_value), '')
    else:
        gg = lambda attr: (ff(attr.w_value), ff(attr.value))
    return dev, attrinfo, gg


def read_device(dev, attrinfo, gg):
    attr = dev.read_attributes((attrinfo.name, 'State'))
    print("{0}/{1}\t{2}\t{3}\t{4}".format(dev.name(), attrinfo.name, str(attr[1].value), *gg(attr[0])))


if __name__ == "__main__":
    # parser = argparse.ArgumentParser(prog='tgread',description='Read a Tango attribute')
    #    parser.add_argument('attributename',nargs='+',help='Attribute_name')
    #    parser.add_argument('-f',nargs='?',help='Format output')
    #    parser.add_argument('-t',nargs='?',help='Polling period [s]')
    parser = optparse.OptionParser(usage="%prog [options] attribute_name")
    parser.add_option('-f', action="store_true", default=False, help='Format output')
    parser.add_option('-t', type="float", help='Polling period [s]')
    options, args = parser.parse_args()

    devs = [configure_device(fname, options.f) for fname in args]
    if options.t is not None:
        while True:
            try:
                [read_device(*args) for args in devs]
                sleep(options.t)
            except KeyboardInterrupt:
                break
    else:
        [read_device(*args) for args in devs]
