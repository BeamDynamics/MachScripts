#!/usr/bin/env python
"""test SR status
sr_test [-i min[:max]] [-t min[:max]] [-z min[:max]] [-o] [-u]
        default: sr_test -t0:720000 -i0.5:260 -o
        -i: beam current [mA]
        -t: lifetime [s]
        -z: vertical emittance [nm]
        -o: orbit OK
        -u: USM
"""

# import

import beam_test
import argparse

__author__ = 'L Farvacque', 'S Liuzzo'
__version__ = '2.0'

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="test the SR status")

    parser.add_argument('-v', '--version', action='version', version=__version__)

    parser.add_argument('-i', '--intensity', type=str,
                        help='total current [mA] in given range default: -i0.5:260 ')

    parser.add_argument('-t', '--lifetime', type=str,
                        help='e- beam lifetime [h] in given range default: -t0.:720000')

    parser.add_argument('-z', '--verticalemittance', type=str,
                        help='e- beam vertical emittance [nm] in given range default: -z0:1000000 ')

    parser.add_argument('-o', '--orbit', action='store_true', default=False,
                        help='e- beam orbit is ok', dest='orbit')

    parser.add_argument('-u', '--user_service_mode', action='store_true', default=False,
                        help='UserServiceMode', dest='usm')

    parser.add_argument('--verbose', action='store_true', default=False,
                        help='Verbose mode')

    arguments = parser.parse_args()

    # default case
    no_arguments = arguments.intensity is None and \
        arguments.lifetime is None and \
        arguments.verticalemittance is None and \
        arguments.orbit is False and \
        arguments.usm is False

    if no_arguments:
        arguments.intensity = "0.5:260"
        arguments.lifetime = "0:720000"
        arguments.orbit = True

    # parse input strings (as in original C++ code) to floats
    if arguments.intensity == '':
        arguments.intensity = "0.5:260"

    if arguments.lifetime == '':
        arguments.lifetime = "0:720000"

    if arguments.verticalemittance == '':
        arguments.verticalemittance = "0.0:1000000"

    if arguments.intensity is not None:
        # accepted range of current, if only one value, maximum is infinity
        I_r = [float(v) for v in arguments.intensity.split(':')]
    else:
        I_r = None

    if arguments.lifetime is not None:
        # accepted range of lifetime, if only one value, maximum is infinity
        T_r = [float(v) for v in arguments.lifetime.split(':')]
    else:
        T_r = None

    if arguments.verticalemittance is not None:
        # accepted range of vertical emittance, if only one value, maximum is infinity
        emit_v_r = [float(v) for v in arguments.verticalemittance.split(':')]
    else:
        emit_v_r = None

    srok = beam_test.sr_test(I_r, T_r, emit_v_r, arguments.orbit, arguments.usm, arguments.verbose)

    print('sr status is : {}'.format(srok))

    exit(srok)
