"""Notification system
"""
from __future__ import print_function, unicode_literals
import PyTango
from notifier.core import BaseNotifier


def growl_message(mess, *args, **kwargs):
    try:
        devname = kwargs.pop('devname', 'sys/lfalarm/1')
        queue = kwargs.pop('queue', 'Info')
        GrowlNotifier(devname=devname)(mess % args, queue=queue, **kwargs)
    except PyTango.DevFailed:
        pass


class GrowlNotifier(BaseNotifier):
    def __init__(self, devname='sys/lfalarm/1', **kwargs):
        self.growler = PyTango.DeviceProxy(devname)
        super(GrowlNotifier, self).__init__(**kwargs)

    def __call__(self, message, **kwargs):
        queue = kwargs.pop('queue', 'Output')
        self.growler.command_inout('Notify', [queue, self.header(**kwargs) + message])

    def arglist(self):
        arg1 = repr(self.growler.name())
        return ', '.join((arg1, super(GrowlNotifier, self).arglist()))
