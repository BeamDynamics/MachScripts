"""Notification system
"""
from __future__ import print_function, unicode_literals
import PyTango
from notifier.core import BaseNotifier


class SMSNotifier(BaseNotifier):
    def __init__(self, number="0689876824", devname='sys/sender/sms', **kwargs):
        self.number = number
        self.dev = PyTango.DeviceProxy(devname)
        super(SMSNotifier, self).__init__(**kwargs)

    def __call__(self, message, **kwargs):
        self.dev.SendSMS([self.number, self.header(**kwargs) + message])

    def arglist(self):
        arg1 = '='.join(('number', repr(self.number)))
        arg2 = '='.join(('devname', repr(self.dev.name())))
        return ', '.join((arg1, arg2, super(SMSNotifier, self).arglist()))
