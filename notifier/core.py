"""Notification system

Notifier creates notification queues. A Notifier object has notification channels identified
by a name ('Output','Error'...) which can be dynamically redirected to outputs like sys/talker, growl

Example:
ntf=notifier.Notifier(Output=print,Error='maclf')
ntf.notify('Output','output message printed on standard output')
ntf.notify('Error','error message displayed on growl')
"""

from __future__ import print_function, unicode_literals   # for python2
from builtins import str, object        # for python2
import sys
import os
import time
from contextlib import contextmanager

try:
    _process = sys.argv[0].partition('.')[0] or 'python'
except AttributeError:
    _process = 'python'


class BaseNotifier(object):
    # noinspection PyShadowingBuiltins
    def __init__(self, file=sys.stdout, format=''):
        """Keywords
        file:   output file (default: sys.stdout)
        format: header format, like '{date} {time}: {process:12} {queue:8}: '
        """
        self.file = file
        self.format = format

    def header(self, **kwargs):
        fmt = kwargs.pop('format', self.format)
        process = kwargs.pop('process', _process)
        queue = kwargs.pop('queue', '')
        tim = kwargs.pop('time', time.strftime('%H:%M:%S'))
        dat = kwargs.pop('date', time.strftime('%d/%m/%Y'))
        return fmt.format(process=process, queue=queue, time=tim, date=dat, **kwargs)

    def __call__(self, message, **kwargs):
        self.file.write(self.header(**kwargs))
        self.file.write(message)
        self.file.write('\n')
        self.file.flush()

    def arglist(self):
        alst = []
        if self.format != '':
            alst.append('='.join(('format', repr(self.format))))
        alst = ','.join(alst)
        return alst

    def __repr__(self):
        return '{0}({1})'.format(self.__class__.__name__, self.arglist())


class _BaseFileNotifier(BaseNotifier):
    def __init__(self, filename='notif.out', **kwargs):
        self.filename = filename
        super(_BaseFileNotifier, self).__init__(**kwargs)

    def arglist(self):
        alst = repr(self.filename)
        return ', '.join((alst, super(_BaseFileNotifier, self).arglist()))


class OpenFileNotifier(_BaseFileNotifier):
    def __init__(self, filename='notif.out', mode='a', buffering=1, **kwargs):
        try:
            fd = open(filename, mode, buffering)
        except IOError as err:
            print('Cannot open {0}: {1}'.format(filename, err.args[1]), file=sys.stderr)
            fd = open(os.devnull, mode, buffering)
        super(OpenFileNotifier, self).__init__(filename, file=fd, **kwargs)

    def __del__(self):
        self.file.close()


class FileNotifier(_BaseFileNotifier):
    def __init__(self, filename='notif.out', mode='a', buffering=1, **kwargs):
        self.mode = mode
        self.buffering = buffering
        super(FileNotifier, self).__init__(filename, **kwargs)

    def __call__(self, message, **kwargs):
        with open(self.filename, self.mode, self.buffering) as fd:
            self.file = fd
            super(FileNotifier, self).__call__(message, **kwargs)


class NotifIO(list):
    """File-like object for output to a list of destinations (output channels)
    """
    def __init__(self, *args, **kwargs):
        """Keywords:
        process: process name
        queue: channel name
        level: mask for enabling the output (default: 1)
        default_level: default level for notifications (default: 255)
        """
        self._process = kwargs.pop('process', _process)
        self._queue = kwargs.pop('queue', 'Output')
        self._buffer = ''
        self.level = kwargs.pop('level', 1)
        self.default_level = kwargs.pop('default_level', 255)
        super(NotifIO, self).__init__(args)

    def get_level(self):
        return self.level

    def set_level(self, level):
        self.level = level

    def write(self, message, **kwargs):
        """Buffered write command.
        Keyword arguments:
        process:    identifier of the calling process
        format:     header format (default: Notifier.format)
        date:
        time:
        """
        level = kwargs.pop('level', self.default_level)
        if level & self.level:
            if message.endswith('\n'):
                self._buffer = "".join((self._buffer, message[:-1]))
                self.flush(**kwargs)
            else:
                self._buffer = "".join((self._buffer, message))

    def flush(self, **kwargs):
        """Flush the internal output buffer
        Keyword arguments:
        process:    identifier of the calling process
        format:     header format (default: Notifier.format)
        date:
        time:
        """
        mdata = dict(process=self._process, queue=self._queue, date=time.strftime('%d/%m/%Y'),
                     time=time.strftime('%H:%M:%S'))
        mdata.update(kwargs)
        for handler in self:
            try:
                handler(self._buffer, **mdata)
            except Exception:
                pass
        self._buffer = ''

    def print(self, *args, **kwargs):
        fd = kwargs.pop('file', self)
        print(*args, file=fd, **kwargs)

    def notify(self, fmt, *args, **kwargs):
        """Send a notification
        Keyword arguments:
        process:    identifier of the calling process
        format:     header format (default: Notifier.format)
        date:
        time:
       """
        end = kwargs.pop('end', '\n')
        self.write(fmt % args + end, **kwargs)


class Notifier(dict):

    default_h = dict(Debug=lambda: [BaseNotifier(format='{queue:8}:')],
                     Info=lambda: [],
                     Output=lambda: [BaseNotifier()],
                     Warning=lambda: [BaseNotifier(format='{queue:8}:', file=sys.stderr)],
                     Error=lambda: [BaseNotifier(format='{queue:8}:', file=sys.stderr)],
                     Critical=lambda: [BaseNotifier(format='{queue:8}:', file=sys.stderr)])

    def __init__(self, **kwargs):
        """Keywords:
        process: process name
        level: mask for enabling the output (default: 1)
        default_level: default level for notifications (default: 255)
        DebugLevel: mask for enabling the Debug channel (default: $DBGLEVEL)
        """
        dbglevel = kwargs.pop('DebugLevel', int(os.getenv('DBGLEVEL', '0')))
        super(Notifier, self).__init__()
        for queue in self.default_h:
            try:
                hlist = kwargs[queue]
            except KeyError:
                hlist = self.default_h[queue]()
            self[queue] = NotifIO(*hlist, queue=queue, **kwargs)
        self['Debug'].set_level(dbglevel)

    def debug(self, *args, **kwargs):
        self['Debug'].notify(*args, **kwargs)

    def info(self, *args, **kwargs):
        self['Info'].notify(*args, **kwargs)

    def output(self, *args, **kwargs):
        self['Output'].notify(*args, **kwargs)

    def warning(self, *args, **kwargs):
        self['Warning'].notify(*args, **kwargs)

    def error(self, *args, **kwargs):
        self['Error'].notify(*args, **kwargs)

    def critical(self, *args, **kwargs):
        self['Critical'].notify(*args, **kwargs)

    def print(self, *args, **kwargs):
        self[kwargs.pop('queue', 'Output')].print(*args, **kwargs)

    @contextmanager
    def context(self, stdout='Output', stderr='Error', level=255):
        savout = sys.stdout
        saverr = sys.stderr
        savlevel = self[stdout].default_level
        try:
            sys.stdout = self[stdout]
            sys.stderr = self[stderr]
            self[stdout].default_level = level
            yield
        finally:
            sys.stdout = savout
            sys.stderr = saverr
            self[stdout].default_level = savlevel

    @contextmanager
    def dbglevel(self, level=255):
        savout = sys.stdout
        savlevel = self['Debug'].default_level
        try:
            sys.stdout = self['Debug']
            self['Debug'].default_level = level
            yield
        finally:
            sys.stdout = savout
            self['Debug'].default_level = savlevel


def dbgprintf(level, *args, **kwargs):
    message = kwargs.pop('sep', ' ').join(str(arg) for arg in args)
    default_notifier['Debug'].notify(message, level=level, **kwargs)


def dbgsetmask(mask):
    default_notifier['Debug'].set_level(mask)

default_notifier = Notifier()
