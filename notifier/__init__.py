"""Notification system

Notifier creates notification queues. A Notifier object has 6 default notification channels identified
by a name ('Debug','Info','Output','Warning','Error','Critical') which can be dynamically redirected to outputs
like sys/talker, growl. Additional channels may be added

Example:
ntf=notifier.Notifier()
ntf.notify('Output','output message printed on standard output')
ntf.notify('Error','error message displayed on growl')
"""
from __future__ import print_function
from notifier.core import *
# noinspection PyProtectedMember
from notifier.core import default_notifier

__author__ = 'laurent'
__all__ = ['Notifier', 'BaseNotifier', 'OpenFileNotifier', 'FileNotifier', 'dbgprintf', 'dbgsetmask']
__version__ = "1.0.0"


# noinspection PyShadowingBuiltins
def print(*args, **kwargs):
    """Print using the default notifier"""
    default_notifier.print(*args, **kwargs)
