"""Notification system
"""
from __future__ import print_function, unicode_literals
import PyTango
from notifier.core import BaseNotifier


class TalkNotifier(BaseNotifier):
    def __init__(self, talker_name='sys/talker/2', **kwargs):
        self.talker = PyTango.DeviceProxy(talker_name)
        super(TalkNotifier, self).__init__(**kwargs)

    # noinspection PyUnusedLocal
    def __call__(self, message, **kwargs):
        self.talker.command_inout('DevTalk', message)

    def arglist(self):
        arg1 = repr(self.talker.name())
        return ', '.join((arg1, super(TalkNotifier, self).arglist()))
