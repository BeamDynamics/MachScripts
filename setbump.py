#!/usr/bin/env python
from __future__ import print_function
import sys
from optparse import OptionParser

import PyTango
import numpy


def setbump(bumpname, filename):
    try:
        table = numpy.loadtxt(filename)
    except IOError as err:
        print("Cannot read \"{0}\": {1}\n".format(filename, err.strerror), file=sys.stderr)
        return
    try:
        bump = PyTango.DeviceProxy(bumpname)
    except PyTango.DevFailed:
        print("Unknown device {0}\n".format(bumpname), file=sys.stderr)
        return
    bump.InterpolationTable = table


def parseargs():
    parser = OptionParser(usage="usage: %prog [options] bump_name file_name")
    (opts, args) = parser.parse_args()
    return args[0], args[1]


if __name__ == "__main__":
    bumpname, filename = parseargs()
    setbump(bumpname, filename)
