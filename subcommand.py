import sys
from subprocess import Popen, PIPE
from threading import Thread


def subcommand(cmd):
    def outthread(sin, sout):
        # for line in sin:
        # sout.write(line)
        while True:
            line = sin.readline()
            if line:
                sout.write(line)
            else:
                break

    proc = Popen(cmd, stdout=PIPE, stderr=PIPE)
    thout = Thread(target=outthread, args=(proc.stdout, sys.stdout))
    thout.start()
    therr = Thread(target=outthread, args=(proc.stderr, sys.stderr))
    therr.start()
    thout.join()
    therr.join()
    # noinspection PyUnusedLocal
    remout, remerr = proc.communicate()
    return proc.returncode
