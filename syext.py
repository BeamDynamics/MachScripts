#!/usr/bin/env python
"""Control of the Booster extraction
Usage: syext.py [options] ON|OFF|QUERY

Options:
--version             show program's version number and exit
-h, --help            show this help message and exit
-w, --store-settings  Store the present settings when switching OFF

Value returned by QUERY:

2: ON, RUNNING
3: OFF, STANDBY
4: UNKNOWN
"""

from __future__ import print_function  # for python2
from builtins import object  # for python2

import sys
import time
from functools import partial

from pymach.devutil import all_on, is_state, is_notmoving, all_notmoving
from pymach.tango import Device, DeviceGroup, DevState, DevSource
from pymach.timing import wait_for

__author__ = 'L Farvacque'
SEPTUM_LIST = ('sy/ps-se/1', 'sy/ps-se/2')
_SEPTUM_DELAY = 2.5
BUMPER_LIST = ('sy/ps-b/1', 'sy/ps-b/2', 'sy/ps-b/3')
KICKER_NAME = 'sy/ps-ke/1'
RESET_DELAY = 10


class SyExtError(Exception):
    def __str__(self):
        return "Extraction error: {0}".format(self.args[0])


class SYExt(object):
    kON = (DevState.ON, DevState.ON, DevState.ON, DevState.ON, DevState.ON, DevState.ON)
    kRUNNING = (DevState.STANDBY, DevState.ON, DevState.ON, DevState.ON, DevState.ON, DevState.ON)
    kSTANDBY = (
        DevState.STANDBY, DevState.STANDBY, DevState.STANDBY, DevState.STANDBY, DevState.STANDBY, DevState.STANDBY)
    kOFF = (DevState.OFF, DevState.OFF, DevState.OFF, DevState.OFF, DevState.OFF, DevState.OFF)
    summarize = dict(
        ((kON, DevState.ON), (kRUNNING, DevState.RUNNING), (kSTANDBY, DevState.STANDBY), (kOFF, DevState.OFF)))
    returncode = dict(
        ((DevState.RUNNING, 2), (DevState.ON, 2), (DevState.STANDBY, 3), (DevState.OFF, 3), (DevState.DISABLE, 3)))

    def __init__(self):
        self.magnets = DeviceGroup('stdby', SEPTUM_LIST + BUMPER_LIST)
        # self.stdby = DeviceGroup('stdby', SEPTUM_LIST)
        # self.magnets = DeviceGroup('magnets', self.stdby, BUMPER_LIST)
        # noinspection PyUnresolvedReferences
        for d in self.magnets.get_device_list():
            # noinspection PyUnresolvedReferences
            self.magnets.get_device(d).set_source(DevSource.DEV)
        # self.se2 = self.magnets.get_device(SEPTUM_LIST[1])    # cannot use this device !
        self.se2 = Device(SEPTUM_LIST[1], source=DevSource.DEV)
        self.kicker = Device(KICKER_NAME, source=DevSource.DEV)

    def on(self):
        """Start the extraction
        """
        interlocked = [d for d in (self.kicker, self.se2) if d.state() in (DevState.FAULT, DevState.DISABLE)]
        if len(interlocked) > 0:
            print('  Reseting interlocks')
            [d.Reset() for d in interlocked]
            time.sleep(4)
        if any(is_state(d, DevState.DISABLE) for d in (self.kicker, self.se2)):
            raise SyExtError('Extraction interlocked')
        if self.kicker.state() > DevState.STANDBY:
            print('  Set kicker STANDBY')
            self.kicker.Standby()  # danger
        if any((st == DevState.OFF) for st in self.magnets.state()):
            print('  Set septa and bumpers STANDBY')
            self.magnets.command_inout('Standby')  # danger
            time.sleep(_SEPTUM_DELAY)
        print('  Set septa and bumpers ON')
        self.magnets.command_inout('On', forward=True)  # danger
        wait_for(partial(is_notmoving, self.kicker), partial(all_notmoving, self.magnets, forward=True), timeout=8)

    def teston(self):
        interlocked = [d for d in (self.kicker, self.se2) if d.state() in (DevState.FAULT, DevState.DISABLE)]
        if len(interlocked) > 0:
            print('  Test Reseting interlocks')
            time.sleep(4)
        print('  Test magnet settings')
        time.sleep(2)
        print('  Test kicker STANDBY')
        print('  Test septa and bumpers STANDBY')
        time.sleep(5)
        print('  Test septa and bumpers ON')
        time.sleep(2)
        print('  Done')

    def off(self, off_magnets=True):
        """Stop the extraction
        """
        print('  Set kicker STANDBY')
        self.kicker.Standby()  # danger
        print('  Set septa and bumpers STANDBY')
        self.magnets.command_inout('Standby')
        time.sleep(_SEPTUM_DELAY)
        if off_magnets:
            print('  Set kickers, septa, bumpers OFF')
            self.kicker.Off()
            self.magnets.command_inout('Off', forward=True)  # danger
        wait_for(partial(is_notmoving, self.kicker), partial(all_notmoving, self.magnets, forward=True), timeout=8)

    def query(self):
        """Check the extraction state. Return value:
            DevState.ON
            DevState.RUNNING :  Bumpers and septa ON, kicker STANDBY
            DevState.STANDBY
            DevState.OFF
            ...
        """
        states = (self.kicker.state(),) + self.magnets.state(forward=True)
        print(states)
        state = self.summarize.get(states, DevState.UNKNOWN)  # list is unhashable
        if state == DevState.UNKNOWN:
            state = max(states)
        return state

    def getstatus(self):
        states = (self.kicker.state(),) + self.magnets.state(forward=True)
        names = (self.kicker.devname,) + tuple(self.magnets.get_device_list(forward=True))
        state = self.summarize.get(states, DevState.UNKNOWN)
        if state == DevState.UNKNOWN:
            state = max(states)
            statuses = (self.kicker.status(),) + self.magnets.status(forward=True)
            detail = ''.join(['{0:>16}: {1} : {2}\n'.format(n, s, st) for n, s, st in zip(names, states,statuses)])
        else:
            detail = ''
        status = '{0}SY Extraction is {1}'.format(detail, state)
        return state, status


if __name__ == "__main__":
    # import argparse
    import optparse

    parser = optparse.OptionParser(usage="%prog [options] ON|OFF|QUERY",
                                   version="%prog 1.0", description="Sets the Booster extraction ON or OFF")
    parser.add_option('-s', '--standby', action="store_false", default=True, dest='off_magnets',
                      help='Keep magnets in STANDBY')
    options, args = parser.parse_args()
    if len(args) != 1:
        parser.error("incorrect number of arguments")
    action = args[0].upper()
    if action == 'ON':
        SYExt().on()
        print('Extraction ON')
        code = 0
    elif action == 'OFF':
        SYExt().off(options.off_magnets)
        print('Extraction OFF')
        code = 0
    elif action == 'QUERY':
        code = SYExt.returncode.get(SYExt().query(), 4)
    else:
        code = 1
    sys.exit(code)
