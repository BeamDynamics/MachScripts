from __future__ import print_function       # for python2
from builtins import chr, range, object     # for python2
import sys
from ctypes import *
import numpy

__author__ = 'L. Farvacque'
__all__ = ['mxArray', 'mxNumericArray', 'mxComplexArray', 'mxLogicalArray',
           'mxCharArray', 'mxCellArray', 'mxStructArray']

suffix = dict(darwin='.dylib', linux2='.so')[sys.platform]
prefix = dict(darwin='/opt/matlab/bin/maci64/', linux2='/opt/matlab/bin/glnxa64/')[sys.platform]
try:
    libmx = CDLL(''.join(('libmx', suffix)))
except OSError:
    libmx = CDLL(''.join((prefix, 'libmx', suffix)))


class Utf8ifier(object):
    @classmethod
    def from_param(cls, value):
        return value.encode('utf-8', 'surrogateescape')


def utf8decoder(value, func, args):
    return value.decode('utf-8', 'surrogateescape')


def decoder(value, func, args):
    return value


try:
    chr = chr
except NameError:   # Python 3
    field_type = utf8decoder
else:               # Python 2
    field_type = decoder
try:
    range = xrange
except NameError:
    pass

c_mwSize = c_size_t
c_mxClassID = c_uint8
c_mxComplexity = c_uint8
c_mxArray_p = c_void_p

mxGetNumberOfDimensions = libmx.mxGetNumberOfDimensions_730
mxGetNumberOfDimensions.argtypes = (c_mxArray_p,)
mxGetNumberOfDimensions.restype = c_mwSize
mxGetDimensions = libmx.mxGetDimensions_730
mxGetDimensions.argtypes = (c_mxArray_p,)
mxGetDimensions.restype = POINTER(c_mwSize)
mxGetClassID = libmx.mxGetClassID
mxGetClassID.argtypes = (c_mxArray_p,)
mxGetClassID.restype = c_mxClassID
mxGetClassName = libmx.mxGetClassName
mxGetClassName.argtypes = (c_mxArray_p,)
mxGetClassName.restype = c_char_p
mxGetClassName.errcheck = utf8decoder
mxIsComplex = libmx.mxIsComplex
mxIsComplex.argtypes = (c_mxArray_p,)
mxIsComplex.restype = c_bool

mxGetData = libmx.mxGetData
mxGetData.argtypes = (c_mxArray_p,)
mxGetData.restype = c_void_p
mxGetImagData = libmx.mxGetImagData
mxGetImagData.argtypes = (c_mxArray_p,)
mxGetImagData.restype = c_void_p
mxGetLogicals = libmx.mxGetLogicals
mxGetLogicals.argtypes = (c_mxArray_p,)
mxGetLogicals.restype = c_void_p
mxGetChars = libmx.mxGetChars
mxGetChars.argtypes = (c_mxArray_p,)
mxGetChars.restype = POINTER(c_uint16)
# mxArrayToString = libmx.mxArrayToString
# mxArrayToString.argtypes = (c_mxArray_p,)
# mxArrayToString.restype = c_void_p
mxGetCell = libmx.mxGetCell_730
mxGetCell.argtypes = (c_mxArray_p, c_mwSize)
mxGetCell.restype = c_void_p
mxSetCell = libmx.mxSetCell_730
mxSetCell.argtypes = (c_mxArray_p, c_mwSize, c_mxArray_p)
mxSetCell.restype = None

mxGetNumberOfFields = libmx.mxGetNumberOfFields
mxGetNumberOfFields.argtypes = (c_mxArray_p,)
mxGetNumberOfFields.restype = c_int
mxGetFieldNameByNumber = libmx.mxGetFieldNameByNumber
mxGetFieldNameByNumber.argtypes = (c_mxArray_p, c_int)
mxGetFieldNameByNumber.restype = c_char_p
mxGetFieldNameByNumber.errcheck = field_type
mxGetField = libmx.mxGetField_730
mxGetField.argtypes = (c_mxArray_p, c_mwSize, Utf8ifier)
mxGetField.restype = c_void_p
mxSetField = libmx.mxSetField_730
mxSetField.argtypes = (c_mxArray_p, c_mwSize, Utf8ifier, c_mxArray_p)
mxSetField.restype = None

mxCreateNumericArray = libmx.mxCreateNumericArray_730
mxCreateNumericArray.argtypes = [c_mwSize, POINTER(c_mwSize), c_mxClassID, c_mxComplexity]
mxCreateNumericArray.restype = c_mxArray_p
mxCreateLogicalArray = libmx.mxCreateLogicalArray_730
mxCreateLogicalArray.argtypes = [c_mwSize, POINTER(c_mwSize)]
mxCreateLogicalArray.restype = c_mxArray_p
mxCreateCharArray = libmx.mxCreateCharArray_730
mxCreateCharArray.argtypes = [c_mwSize, POINTER(c_mwSize)]
mxCreateCharArray.restype = c_mxArray_p
mxCreateCellArray = libmx.mxCreateCellArray_730
mxCreateCellArray.argtypes = [c_mwSize, POINTER(c_mwSize)]
mxCreateCellArray.restype = c_mxArray_p
mxCreateStructArray = libmx.mxCreateStructArray_730
mxCreateStructArray.argtypes = [c_mwSize, POINTER(c_mwSize), c_int, POINTER(c_char_p)]
mxCreateStructArray.restype = c_mxArray_p
mxDestroyArray = libmx.mxDestroyArray
mxDestroyArray.argtypes = [c_mxArray_p]
mxDestroyArray.restype = None
# mxFree = libmx.mxFree
# mxFree.argtypes = [c_void_p]
# mxFree.restype = None


class mxArray(object):
    pydescr = dict((
        (0, ('logical', 0)),  # bool
        (1, ('int8', 0)),  # int8
        (2, ('uint8', 0)),  # uint8
        (3, ('int16', 0)),  # int16
        (4, ('uint16', 0)),  # uint16
        (5, ('int32', 0)),  # int32
        (6, ('uint32', 0)),  # uint32
        (7, ('int64', 0)),  # int64
        (8, ('uint64', 0)),  # uint64
        (11, ('single', 0)),  # single
        (12, ('double', 0)),  # double
        (14, ('single', 1)),  # single complex
        (15, ('double', 1)),  # double complex
        (17, ('cell', 0)),  # cell
        (20, ('struct', 0))  # struct
    ))

    @staticmethod
    def getshape(mxarr):
        ndims = mxGetNumberOfDimensions(mxarr)
        mxdims = mxGetDimensions(mxarr)
        shape = tuple(mxdims[i] for i in range(ndims) if mxdims[i] > 1)
        if len(shape) == 0:
            shape = (mxdims[0],)
        return shape

    @staticmethod
    def setshape(shape, oned_as='row'):
        if len(shape) == 1 and oned_as == 'row':
            shape = (1,) + shape
        ndims = len(shape)
        dims = (c_mwSize * ndims)()
        for i in range(ndims):
            dims[i] = shape[i]
        return ndims, dims

    @staticmethod
    def getstrides(dtype, shape):
        def its(itemsize, sh):
            l = itemsize
            for d in sh:
                yield (l)
                l *= d

        return tuple(s for s in its(dtype.itemsize, shape))

    @classmethod
    def fromfile(cls, mxarray, **kwargs):
        classname = mxGetClassName(mxarray)
        cmplx = mxIsComplex(mxarray)
        descr = mxdescr[classname][cmplx]
        return descr.mxclass(descr=descr, mxpointer=mxarray, **kwargs)

    @classmethod
    def fromobject(cls, pyobj, **kwargs):
        cmplx = 0
        if isinstance(pyobj, numpy.ndarray):
            mxclass, cmplx = cls.pydescr[pyobj.dtype.num]
            shape = pyobj.shape
            if mxclass == 'struct':
                keys = [v[0] for v in pyobj.dtype.descr]
                pyiter = ((k, pyobj[k]) for k in keys)
                print([(k, pyobj[k]) for k in keys])
                kwargs['keys'] = keys
            else:
                pyiter = enumerate(pyobj.T.flat)
        elif isinstance(pyobj, str):
            mxclass = 'char'
            shape = (1, len(pyobj))
            pyiter = enumerate(pyobj)
        elif isinstance(pyobj, list):
            mxclass = 'cell'
            shape = (1, len(pyobj))
            pyiter = enumerate(pyobj)
        elif isinstance(pyobj, dict):
            mxclass = 'struct'
            shape = (1,)
            pyiter = ((k,(v,)) for k,v in pyobj.items())
            kwargs['keys'] = list(pyobj.keys())
        elif isinstance(pyobj, numpy.generic):
            return mxArray.fromobject(numpy.array([pyobj], dtype=pyobj.dtype), **kwargs)
        elif isinstance(pyobj, (float, int, bool)):
            return mxArray.fromobject(numpy.array([pyobj]), **kwargs)
        else:
            raise TypeError('Cannot convert {0}'.format(type(pyobj)))
        descr = mxdescr[mxclass][cmplx]
        print(descr.mxclass)
        return descr.mxclass(shape=shape, descr=descr, init=pyiter, **kwargs)

    def __init__(self, mxfunc, descr, shape=(1,), mxpointer=None, oned_as='row', owndata=True, init=None):
        if mxpointer is not None:
            shape = self.getshape(mxpointer)
        else:
            ndims, dims = self.setshape(shape, oned_as=oned_as)
            mxpointer = mxfunc(ndims, dims)
        self.mxarray = mxpointer
        self.owndata = owndata
        self.shape = shape
        self.rdata = cast(mxGetData(mxpointer), descr.ctype)
        self.dtype = descr.dtype
        sz = 1
        for s in shape:
            sz *= s
        self.size = sz
        if init is not None:
            for key, value in init:
                self[key] = value

    def __del__(self):
        if self.mxarray is not None and self.owndata:
            print('mxDestroyArray', self)
            mxDestroyArray(self.mxarray)

    def __getitem__(self, key):
        return self.rdata[key]

    def __setitem__(self, key, value):
        self.rdata[key] = value

    def __repr__(self):
        return '{0}({1}, mxclass={2})'.format(self.__class__.__name__, self.shape, mxGetClassName(self.mxarray))

    def __len__(self):
        return self.size

    def _iti(self):
        for i in range(self.size):
            yield self[i]

    def __iter__(self):
        def iti():
            for i in range(self.size):
                yield self[i]
        return iti()


class mxNumericArray(mxArray):
    def __init__(self, **kwargs):

        def mxcreate(ndims, dims):
            return mxCreateNumericArray(ndims, dims, descr.mxID, 0)

        descr = kwargs.pop('descr', mxdescr['double'][0])
        super(mxNumericArray, self).__init__(mxcreate, descr, **kwargs)
        self.__array_interface__ = dict(shape=self.shape, data=(mxGetData(self.mxarray), False), version=3,
                                        typestr=self.dtype.str, strides=self.getstrides(self.dtype, self.shape))

    def __repr__(self):
        return '{0}({1}, dtype={2}, mxclass={3})'.format(self.__class__.__name__, self.shape, self.dtype,
                                                         mxGetClassName(self.mxarray))

    def topy(self):
        # pyobj = numpy.array(tuple(self), dtype=self.dtype, copy=True).reshape(self.shape, order='F')
        pyobj = numpy.array(self, copy=True)
        if self.size == 1:
            return pyobj[0]
        return pyobj


class mxComplexArray(mxArray):
    def __init__(self, **kwargs):

        def mxcreate(ndims, dims):
            return mxCreateNumericArray(ndims, dims, descr.mxID, 1)

        descr = kwargs.pop('descr', mxdescr['double'][1])
        super(mxComplexArray, self).__init__(mxcreate, descr, **kwargs)
        self.idata = cast(mxGetImagData(self.mxarray), descr.ctype)

    def __repr__(self):
        return '{0}({1}, dtype={2}, mxclass={3})'.format(self.__class__.__name__, self.shape, self.dtype,
                                                         mxGetClassName(self.mxarray))

    def __getitem__(self, key):
        return complex(self.rdata[key], self.idata[key])

    def __setitem__(self, key, value):
        self.rdata[key] = value.real
        self.idata[key] = value.imag

    def __array__(self, dtype=None):
        arr = numpy.empty((self.size,), dtype=(dtype or self.dtype))
        for i, v in enumerate(self):
            arr[i] = v
        return arr.reshape(self.shape, order='F')

    def topy(self):
        # pyobj = numpy.array(tuple(self), dtype=self.dtype, copy=True).reshape(self.shape, order='F')
        pyobj = numpy.array(self)
        if self.size == 1:
            return pyobj[0]
        return pyobj


class mxLogicalArray(mxArray):
    def __init__(self, **kwargs):
        descr = kwargs.pop('descr', mxdescr['logical'][0])
        super(mxLogicalArray, self).__init__(mxCreateLogicalArray, descr, **kwargs)
        self.__array_interface__ = dict(shape=self.shape, data=(mxGetLogicals(self.mxarray), False), version=3,
                                        typestr=self.dtype.str, strides=self.getstrides(self.dtype, self.shape))

    def topy(self):
        pyobj = numpy.array(self, copy=True)
        if self.size == 1:
            return pyobj[0]
        return pyobj


class mxCharArray(mxArray):
    def __init__(self, **kwargs):
        descr = kwargs.pop('descr', mxdescr['char'][0])
        super(mxCharArray, self).__init__(mxCreateCharArray, descr, **kwargs)

    def __getitem__(self, key):
        return chr(self.rdata[key])

    def __setitem__(self, key, value):
        self.rdata[key] = ord(value)

    def topy(self):
        obj = u''
        for i in range(self.size):
            obj += chr(self.rdata[i])
        return obj
        #
        # def topy2(self):
        # res = mxArrayToString(self.mxarray)
        #     value = cast(res, c_char_p).value.decode('utf-8', 'surrogateescape')
        #     mxFree(res)
        #     return cast(res, c_char_p).value


class mxCellArray(mxArray):
    def __init__(self, **kwargs):
        descr = kwargs.pop('descr', mxdescr['cell'][0])
        super(mxCellArray, self).__init__(mxCreateCellArray, descr, **kwargs)

    def __getitem__(self, key):
        return mxArray.fromfile(mxGetCell(self.mxarray, key), owndata=False).topy()

    def __setitem__(self, key, value):
        mxSetCell(self.mxarray, key, mxArray.fromobject(value, owndata=False).mxarray)

    def __array__(self, dtype=None):
        arr = numpy.empty((self.size,), dtype=(dtype or self.dtype))
        for i, v in enumerate(self):
            arr[i] = v
        return arr.reshape(self.shape, order='F')

    def topy(self):
        return numpy.array(self)


class mxStructArray(mxArray):
    def __init__(self, keys=(), mxpointer=None, **kwargs):

        def mxcreate(ndims, dims):
            nfields = len(keys)
            fields = (c_char_p * nfields)()
            for i, k in enumerate(keys):
                fields[i] = k
            return mxCreateStructArray(ndims, dims, nfields, fields)

        descr = kwargs.pop('descr', mxdescr['struct'][0])
        if mxpointer is not None:
            nf = mxGetNumberOfFields(mxpointer)
            keys = [mxGetFieldNameByNumber(mxpointer, i) for i in range(nf)]

        super(mxStructArray, self).__init__(mxcreate, descr, mxpointer=mxpointer, **kwargs)
        self.fields = set(keys)

    def __iter__(self):
        """x.__iter__() <==> iter(x)"""
        return self.fields.__iter__()

    def __contains__(self, key):
        """x.__contains__(k) -> True if x has a key k, else False"""
        return self.fields.__contains__(key)

    def __getitem__(self, key):
        l = [mxArray.fromfile(mxGetField(self.mxarray, i, key), owndata=False).topy() for i in range(self.size)]
        return numpy.array(l)

    def __setitem__(self, key, value):
        for i in range(self.size):
            mxSetField(self.mxarray, i, key, mxArray.fromobject(value[i], owndata=False).mxarray)

    def _iti(self):
        for i in self.fields:
            yield (i, self[i])

    def _ita(self):
        for i in self.fields:
            yield self[i]

    def iterkeys(self):
        """x.iterkeys() -> an iterator over the keys of x"""
        return self.fields.__iter__()

    def iteritems(self):
        """x.iteritems() -> an iterator over the (key, value) items of x"""
        return self._iti()

    def itervalues(self):
        """x.itervalues() -> an iterator over the values of x"""
        return self._ita()

    def __array__(self, dtype=None):
        d = dict(iter(self.items()))
        dt = numpy.dtype([(key, value[0].dtype.str, value.shape[1:] or 1) for key, value in d.items()])
        arr = numpy.empty((self.size,), dtype=dt)
        for k, value in d.items():
            arr[k][:] = value
        return arr.reshape(self.shape, order='F')

    def topy(self):
        pyobj = numpy.array(self)
        if self.size == 1:
            return pyobj[0]
        return pyobj


class mxDescr(object):
    def __init__(self, mxID, ctype, dtype, mxclass=None):
        self.mxID = mxID
        self.ctype = ctype
        self.dtype = dtype
        self.mxclass = mxclass


mxdescr = dict(
    cell=(mxDescr(1, c_void_p, numpy.dtype(object), mxCellArray),),
    struct=(mxDescr(2, c_void_p, numpy.dtype(object), mxStructArray),),
    logical=(mxDescr(3, POINTER(c_bool), numpy.dtype('b1'), mxLogicalArray),),
    char=(mxDescr(4, POINTER(c_uint16), None, mxCharArray),),
    double=(mxDescr(6, POINTER(c_double), numpy.dtype(float), mxNumericArray),
            mxDescr(6, POINTER(c_double), numpy.dtype(complex), mxComplexArray)),
    single=(mxDescr(7, POINTER(c_float), numpy.dtype('f4'), mxNumericArray),
            mxDescr(6, POINTER(c_float), numpy.dtype(complex), mxComplexArray)),
    int8=(mxDescr(8, POINTER(c_int8), numpy.dtype('i1'), mxNumericArray),
          mxDescr(6, POINTER(c_int8), numpy.dtype(complex), mxComplexArray)),
    uint8=(mxDescr(9, POINTER(c_uint8), numpy.dtype('u1'), mxNumericArray),
           mxDescr(6, POINTER(c_uint8), numpy.dtype(complex), mxComplexArray)),
    int16=(mxDescr(10, POINTER(c_int16), numpy.dtype('i2'), mxNumericArray),
           mxDescr(6, POINTER(c_int16), numpy.dtype(complex), mxComplexArray)),
    uint16=(mxDescr(11, POINTER(c_uint16), numpy.dtype('u2'), mxNumericArray),
            mxDescr(6, POINTER(c_uint16), numpy.dtype(complex), mxComplexArray)),
    int32=(mxDescr(12, POINTER(c_int32), numpy.dtype('i4'), mxNumericArray),
           mxDescr(6, POINTER(c_int32), numpy.dtype(complex), mxComplexArray)),
    uint32=(mxDescr(13, POINTER(c_uint32), numpy.dtype('u4'), mxNumericArray),
            mxDescr(6, POINTER(c_uint32), numpy.dtype(complex), mxComplexArray)),
    int64=(mxDescr(14, POINTER(c_int64), numpy.dtype('i8'), mxNumericArray),
           mxDescr(6, POINTER(c_int64), numpy.dtype(complex), mxComplexArray)),
    uint64=(mxDescr(15, POINTER(c_uint64), numpy.dtype('u8'), mxNumericArray),
            mxDescr(6, POINTER(c_uint64), numpy.dtype(complex), mxComplexArray)),
)
