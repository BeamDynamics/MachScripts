from __future__ import print_function, division
from builtins import zip, map
from abc import ABCMeta
from matengine import start_engine, get_engine
import matlab
import numpy as np

start_engine()


class ATModel(object):
    """Access to optical values
    
    Available locations:
    
    bpm:   All BPMs
    qp:    All quadrupoles
    sx:    All sextpoles
    steerh:    Horizontal steerers
    steerv:    Vertical steerers
    
    Available parameters:
    
    0:     index in the AT structure
    1:     Integrated strength
    2:     theta/2pi
    3:     betax
    4:     alphax
    5:     phix/2pi/nux
    6:     etax
    7:     eta'x
    8:     betaz
    9:     alphaz
    10:    phiz/2pi/nux
    11:    etaz
    12:    eta'z
    
    Default parameters:
    
    [   2         3         5        6      8       10      ]
    [theta/2Pi, betax, Phix/2PiNux, etax, betaz, Phiz/2PiNux]
    """
    __metaclass__ = ABCMeta

    @staticmethod
    def mat2py(arg):
        """Convert a matlab.double into a numpy array"""
        code = isinstance(arg, str)
        return arg if code else matlab.double(arg)

    @staticmethod
    def py2mat(arg, oned_as='row'):
        """Convert a numpy array into a matlab.double"""
        if isinstance(arg, np.ndarray):
            shp = arg.shape
            if len(shp) == 1:
                shp = shp + (1,) if oned_as == 'column' else (1,) + shp
            v = matlab.double(list(arg.ravel(order='F')))
            v.reshape(shp)
            return v
        else:
            try:
                return matlab.double(arg)
            except ValueError:
                return arg

    def __init__(self, *args):
        self.eng = get_engine()
        self.model = None

    def get(self, *args):
        """val2, val2,... = get(code1,code2,...)
            vali: optical values at locations specified by codei
        """
        def marg(arg):
            code = isinstance(arg, str)
            return code, arg if code else matlab.double(arg)

        iscode, matargs = zip(*map(marg, args))
        nargout = sum(iscode)
        if nargout == 1:
            return np.array(self.eng.get(self.model, *matargs))
        else:
            return map(np.array, self.eng.get(self.model, *matargs, nargout=nargout))

    def name(self, *args):
        """nm2, nm2,... = name(code1,code2,...)
            nmi: name of elements specified by codei
        """
        return self.eng.name(self.model, *args, nargout=len(args))

    def select(self, *args):
        """id1, id2,... = select(code1, code2,...)
            id1: mask of selected elements in the lattice
        """
        def boolvect(matarg):
            return np.array(matarg).ravel()

        nargout = len(args)
        if nargout == 1:
            return boolvect(self.eng.select(self.model, *args))
        else:
            return map(boolvect, self.eng.select(self.model, *args, nargout=nargout))

    def plot(self, *args):
        return self.eng.plot(self.model, *map(self.py2mat, args))

    def retune(self, tunes):
        """retune(tunes)
            Retune the ring

        tunes:  array of desired tunes (nuh, nuv)
        """
        self.eng.retune(self.model, matlab.double(tunes), nargout=0)

    def getfieldvalue(self, code, *args):
        """value=getfieldvalue(code,field)
            for each selected element, return elem.field
        code:   one of the available locations
        field:  field name

        value=getfieldvalue(code,fieldname,[m,n],...)
            for each selected element, return elem.field[m,n]
        code:   one of the available locations
        field:  field name
        m,n:    MATLAB indices (starting at 1)

        Example:
        strength=model.getfieldvalue('qp','PolynomB',[2]) will return the strength
        of all quadrupoles
        """
        vals = self.eng.getfieldvalue(self.model, code, *args)
        return vals if (len(vals) == 0) or isinstance(vals[0], str) else np.array(vals).squeeze()

    def setfieldvalue(self, code, *args):
        """setfieldvalue(code,field,value)
            for each selected element, set elem.field=value
        code:   one of the available locations
        field:  field name

        setfieldvalue(code,field,[m,n],value)
            for each selected element, set elem.field[m,n]=value
        code:   one of the available locations
        field:  field name
        m,n:    MATLAB indices (starting at 1)

        Value format:
        1x1 array or scalar: the value is affected to all elements
        Mx1 array or vector:  one value per element
        MxN array:  one row per element

        Example:
        model.setfieldvalue('qp','PolynomB',[2],strength) will set the strength
        of all quadrupoles
        """
        vals = self.py2mat(args[-1], oned_as='column')
        self.eng.setfieldvalue(self.model, code, *(args[:-1]+(vals,)), nargout=0)

    @property
    def ring(self):
        return self.eng.getfield(self.model, 'ring')

    @property
    def ll(self):
        """Ring circumference"""
        return self.eng.getfield(self.model, 'll')

    @property
    def alpha(self):
        """Momentum compaction factor"""
        return self.eng.getfield(self.model, 'alpha')

    @property
    def nuh(self):
        """Horizontal tune"""
        return self.eng.getfield(self.model, 'nuh')

    @property
    def nuv(self):
        """Vertical tune"""
        return self.eng.getfield(self.model, 'nuv')

    def _getsname(self):
        """Lattice name"""
        return self.eng.getfield(self.model, 'strname')

    def _setsname(self, name):
        self.eng.setfield(self.model, 'strname', name)

    strname = property(_getsname, _setsname)

    @property
    def emith(self):
        """Horizontal emittance"""
        return self.eng.getfield(self.model, 'emith')

    @property
    def emitv(self):
        """Vertical emittance"""
        return self.eng.getfield(self.model, 'emitv')

    @property
    def energy_spread(self):
        """Energy spread"""
        return self.eng.getfield(self.model, 'energy_spread')

    @property
    def bunch_length(self):
        """Bunch length"""
        return self.eng.getfield(self.model, 'bunch_length')
