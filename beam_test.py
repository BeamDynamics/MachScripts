"""test beam status
module used among others by ebs/sr_test.py and sr/sr_test.py
"""

# import
from __future__ import print_function, division     # for python2
from pymach.tango import Device, Attribute
from pymach.tango import DevState, DevFailed
from contextlib import contextmanager


__author__ = 'L Farvacque', 'S Liuzzo'
__version__ = '2.0'


@contextmanager
def trycatcherr(attribute_name, error=False):
    try:
        att = Attribute(attribute_name)
        yield att
    except DevFailed:
        print('Could not reach {}'.format(attribute_name))
        if error:
            raise


def sr_test(current=None,
            lifetime=None,
            emittance_v=None,
            orbit=False,
            usm=False,
            verbose=False,
            current_attribute='sr/d-ct/1/Current',
            lifetime_attribute='sr/d-ct/1/Lifetime',
            emittance_vertical_attribute='sr/d-emit/average/zemittance',
            orbit_device='sr/d-orbit/1',
            usm_attribute='sys/machstat/tango/sr_mode'):
    """
    test SR status

    if not None, tested
    :param current: min, max (default:0.5,260) range of accepted currents
    :param lifetime: min,max (default:0,720000)] range of accepted lifetimes
    :param emittance_v: min,max(default:0,Inf)] range of accepted vertical emittance
    :param orbit: test orbit is ok
    :param usm: test if in usm mode
    :param verbose: vebose
    :param current_attribute: attribute to use for current reading
    :param lifetime_attribute: attribute to use for lifetime reading
    :param emittance_vertical_attribute: attribute to use for vertical emittance reading
    :param orbit_device: attribute to use for orbit
    :param usm_attribute: attribute to use for USM machine state

    :return:  0 if all test ok or not tested
              1 10000 if  current test failed
              2 01000 if  lifetime test failed
              4 00100 if  emittance test failed
              8 00010 if  orbit test failed
             16 00001 if  USM test failed
              3 11000 if current and lifetime test failed
             ...
             31 11111 if all test failed
    """

    sr_failed = [None, None, None, None, None]

    # test lifetime
    if lifetime:

        # transform to list, to accept tuples as input
        lifetime = list(lifetime)

        sr_failed[0] = True  # intialize to failed if tested

        # set default max limits
        if len(lifetime) == 1:
            lifetime.append(720000)  # 200h

        with trycatcherr(lifetime_attribute, error=False) as attr:
            t = attr.value
            tok = lifetime[0] <= t <= lifetime[1]
            if verbose:
                print('lifetime: {1} < {0} < {2}, is ok: {3} '.format(t, lifetime[0], lifetime[1], tok))
            sr_failed[0] = not tok

    # test current
    if current:
        # transform to list, to accept tuples as input
        current = list(current)

        sr_failed[1] = True  # intialize to failed if tested

        # set default max limits
        if len(current) == 1:
            current.append(260)  # mA

        with trycatcherr(current_attribute, error=False) as attr:
            i = attr.value
            iok = current[0] <= i <= current[1]
            if verbose:
                print('Total current: {1} < {0} < {2}, is ok: {3} '.format(i, current[0], current[1], iok))
            sr_failed[1] = not iok

    # test orbit
    if orbit:
        sr_failed[2] = True  # intialize to failed if tested
        try:
            ook = Device(orbit_device).state() == DevState.ON
            if verbose:
                print('orbit: {} '.format(ook))
            sr_failed[2] = not ook
        except DevFailed:
            print('Could not reach {}'.format(orbit_device))

    # test USM mode
    if usm:
        sr_failed[3] = True  # intialize to failed if tested
        with trycatcherr(usm_attribute, error=False) as attr:
            uval = attr.value
            mode_sr = ['USM', 'MDT', 'SHUTDOWN', 'SAFETY_TEST', 'ID_TEST']
            if verbose:
                print('usm: {} '.format(mode_sr[uval-1]))
            sr_failed[3] = not uval == 1

    # test emittance
    if emittance_v:

        # transform to list, to accept tuples as input
        emittance_v = list(emittance_v)

        sr_failed[4] = True  # intialize to failed if tested

        if len(emittance_v) == 1:
            emittance_v.append(float('inf'))

        with trycatcherr(emittance_vertical_attribute, error=False) as attr:
            try:
                ev = attr.value * 1e3
            except Exception:
                print('Vertical Emittance found {} pm, set to -1'.format(attr.value))
                ev = -1

            evok = emittance_v[0] <= ev <= emittance_v[1]
            if verbose:
                print('emittance: {1} < {0} < {2}, is ok: {3} '.format(ev, emittance_v[0], emittance_v[1], evok))
            sr_failed[4] = not evok

    tfstr = ['0', '0', '0', '0', '0']

    for itf, tf in enumerate(sr_failed):
        if tf is not None:
            tfstr[itf] = str(int(tf))

    biterrorcode = int(''.join(tfstr[::-1]), 2)

    dispstr = ['-', '-', '-', '-', '-']

    for itf, tf in enumerate(sr_failed):
        if tf is not None:
            dispstr[itf] = str(int(tf))
        else:
            dispstr[itf] = '-'

    if verbose:
        print('status\t' + '\t'.join(['lt', 'I', 'O', 'USM', 'Ev', ]))
        print(str(biterrorcode) + '\t' + '\t'.join(dispstr))

    return biterrorcode

#
# if __name__ == "__main__":
#     import argparse
#     parser = argparse.ArgumentParser(description="test the SR status")
#
#     parser.add_argument('-v', '--version', action='version', version=__version__)
#
#     parser.add_argument('-i', '--intensity', type=str,
#                         help='total current in given range default 0.5:260 [mA]')
#
#     parser.add_argument('-t', '--lifetime', type=str,
#                         help='e- beam lifetime in given range default 0.:720000 [h]')
#
#     parser.add_argument('-z', '--verticalemittance', type=str,
#                         help='e- beam vertical emittance in given range default 0:Inf [nm] ')
#
#     parser.add_argument('-o', '--orbit', action='store_true',
#                         help='e- beam orbit is ok', dest='orbit')
#
#     parser.add_argument('-u', '--user_service_mode', action='store_true',
#                         help='UserServiceMode', dest='usm')
#
#     parser.add_argument('--verbose', action='store_true', default='False',
#                         help='Verbose mode')
#
#     arguments = parser.parse_args()
#
#     no_arguments = arguments.intensity is None and \
#                    arguments.lifetime is None and \
#                    arguments.verticalemittance is None and \
#                    arguments.orbit is False and \
#                    arguments.usm is False
#
#     if no_arguments:
#         arguments.intensity = "0.5:260"
#         arguments.lifetime = "0:720000"
#         arguments.orbit = True
#
#
#     # parse input strings (as in original C++ code) to floats
#     if arguments.intensity == '':
#         arguments.intensity = "0.5:260"
#
#     if arguments.lifetime == '':
#         arguments.lifetime = "0:720000"
#
#     if arguments.verticalemittance == '':
#         arguments.verticalemittance = "0.0:1000000"
#
#     if arguments.intensity is not None:
#         # accepted range of current, if only one value, maximum is infinity
#         I_r = [float(v) for v in arguments.intensity.split(':')]
#     else:
#         I_r = None
#
#     if arguments.lifetime is not None:
#         # accepted range of lifetime, if only one value, maximum is infinity
#         T_r = [float(v) for v in arguments.lifetime.split(':')]
#     else:
#         T_r = None
#
#     if arguments.verticalemittance is not None:
#         # accepted range of vertical emittance, if only one value, maximum is infinity
#         emit_v_r = [float(v) for v in arguments.verticalemittance.split(':')]
#     else:
#         emit_v_r = None
#
#     srok = sr_test(I_r, T_r, emit_v_r, arguments.orbit, arguments.usm, arguments.verbose)
#
#     print('sr status is : {}'.format(srok))
#
#     exit(srok)
