from builtins import object
import_matfile

__author__ = 'L. Farvacque'


class MatFile(object):
    _oned = dict(column=1, row=2)
    _matversion = {'4': 'w4', '5': 'w'}

    def __init__(self, fname, mode='r', oned_as='column', appendmat=True):
        """Open a .mat file.
        mode may be 'r', 'u', 'w', 'w4', 'wz', 'w7.3'
        """
        if appendmat and not fname.endswith('.mat'):
            fname += '.mat'
        self.fname = fname
        self.variables = set()
        self.mfile = None
        self.open(mode)
        self.oned_as = oned_as

    def __iter__(self):
        """x.__iter__() <==> iter(x)"""
        return self.variables.__iter__()

    def __contains__(self, item):
        """x.__contains__(k) -> True if x has a key k, else False"""
        return self.variables.__contains__(item)

    def __getitem__(self, item):
        """x.__getitem__(y) <==> x[y]"""
        return _matfile.matread(self.mfile, item)

    def __setitem__(self, item, value):
        """x.__setitem__(i, y) <==> x[i]=y"""
        self.update(((item, value),))

    def __delitem__(self, item):
        """x.__delitem__(y) <==> del x[y]"""
        _matfile.matdelete(self.mfile, item)
        self.variables.remove(item)

    def get(self, *args):
        try:
           return self[args[0]]
        except KeyError:
            if len(args) >= 2:
                return args[1]
            else:
                raise

    def keys(self):
        """x.keys() -> a stp-like object providing a view on D's keys"""
        return set(self)

    def update(self, *args, **kwargs):
        """x.update([E, ]**F) -> None.  Update x from dict/iterable E and F.
        If E is present and has a .keys() method, then does:  for k in E: x[k] = E[k]
        If E is present and lacks a .keys() method, then does:  for k, v in E: x[k] = v
        In either case, this is followed by: for k in F:  x[k] = F[k]
        """
        oned_as = self._oned[kwargs.pop('oned_as', self.oned_as)]
        if len(args) > 1:
            raise TypeError("MatFile.update() takes at most 1 argument ({0} given)".format(len(args)))
        elif len(args) == 1:
            kwargs.update(args[0])
        for item in kwargs:
            _matfile.matwritearray(self.mfile, item, kwargs[item], oned_as)
            self.variables.add(item)

    def _iti(self):
        for i in self.variables:
            yield (i, self[i])

    def _ita(self):
        for i in self.variables:
            yield self[i]

    def iterkeys(self):
        """x.iterkeys() -> an iterator over the keys of x"""
        return self.variables.__iter__()

    def iteritems(self):
        """x.iteritems() -> an iterator over the (key, value) items of x"""
        return self._iti()

    def itervalues(self):
        """x.itervalues() -> an iterator over the values of x"""
        return self._ita()

    def open(self, mode='r'):
        self.mfile = _matfile.open(self.fname, mode)
        self.variables.update(_matfile.directory(self.mfile))

    def close(self):
        self.mfile = None
        self.variables.clear()


def loadmat(file_name, m_dict=None, appendmat=True):
    """loadmat(file_name, mdict=None, appendmat=True, **keywords)
    Load Matlab(tm) file

    file_name : string
       Name of the mat file (do not need .mat extension if
       appendmat==True) If name not a full path name, search for the
       file on the sys.path list and use the first one found (the
       current directory is searched first).  Can also pass open
       file-like object
    m_dict : dict, optional
        dictionary in which to insert matfile variables
    appendmat : {True, False} optional
       True to append the .mat extension to the end of the given
       filename, if not already present
    """
    m = MatFile(file_name, appendmat=appendmat)
    if m_dict is None:
        return dict(iter(m.items()))
    else:
        m_dict.update(m)


def savemat(file_name, mdict, appendmat=True, format='5', do_compression=False, oned_as='column', **kwargs):
    """Save a dictionary of names and arrays into a MATLAB-style .mat file.

    This saves the array objects in the given dictionary to a MATLAB-
    style .mat file.

    Parameters
    ----------
    file_name : str or file-like object
        Name of the .mat file (.mat extension not needed if ``appendmat ==
        True``).
        Can also pass open file_like object.
    mdict : dict
        Dictionary from which to save matfile variables.
    appendmat : bool, optional
        True (the default) to append the .mat extension to the end of the
        given filename, if not already present.
    format : {'5', '4'}, string, optional
        '5' (the default) for MATLAB 5 and up (to 7.2),
        '4' for MATLAB 4 .mat files
    long_field_names : bool, optional
        False (the default) - maximum field name length in a structure is
        31 characters which is the documented maximum length.
        True - maximum field name length in a structure is 63 characters
        which works for MATLAB 7.6+
    do_compression : bool, optional
        Whether or not to compress matrices on write.  Default is False.
    oned_as : {'row', 'column'}, optional
        If 'column', write 1-D numpy arrays as column vectors.
        If 'row', write 1-D numpy arrays as row vectors.
    """
    mode = MatFile._matversion[format] + 'z'[not do_compression:]
    m = MatFile(file_name, mode=mode, oned_as=oned_as, appendmat=appendmat)
    m.update(mdict, **kwargs)