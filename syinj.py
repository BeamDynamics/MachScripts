#!/usr/bin/env python
"""Control of the Booster injection
Usage: syinj.py [options] ON|OFF|QUERY

Options:
--version             show program's version number and exit
-h, --help            show this help message and exit
-w, --store-settings  Store the present settings when switching OFF
"""

from __future__ import print_function   # for python2
from builtins import object             # for python2
import time
import sys
from functools import partial
from pymach.tango import DeviceGroup, DevState, DevSource
from pymach.devutil import all_notmoving
from pymach.timing import wait_for

__author__ = 'L Farvacque'
SEPTUM_NAME = 'sy/ps-si/1'
_SEPTUM_DELAY = 2
KICKER_NAME = 'sy/ps-ki/1'
RESET_DELAY = 10


class SYInj(object):
    kON = (DevState.ON, DevState.ON)
    kSTANDBY = (DevState.STANDBY, DevState.STANDBY)
    kOFF = (DevState.OFF, DevState.OFF)
    summarize = dict(((kON, DevState.ON), (kSTANDBY, DevState.STANDBY), (kOFF, DevState.OFF)))
    returncode = dict(((DevState.ON, 2), (DevState.STANDBY, 3), (DevState.OFF, 3), (DevState.UNKNOWN, 4)))

    def __init__(self):
        self.magnets = DeviceGroup('magnets', SEPTUM_NAME, KICKER_NAME)
        # noinspection PyUnresolvedReferences
        for d in self.magnets.get_device_list():
            self.magnets.get_device(d).set_source(DevSource.DEV)

    def on(self):
        """Start the injection"""
        if any(st > DevState.STANDBY for st in self.magnets.state()):
            print('  Set magnets STANDBY')
            # noinspection PyUnusedLocal
            result = self.magnets.command_inout('Standby')  # danger
            time.sleep(_SEPTUM_DELAY)
        print('  Set magnets ON')
        # noinspection PyUnusedLocal
        result = self.magnets.command_inout('On')  # danger
        wait_for(partial(all_notmoving, self.magnets), timeout=8)

    def off(self, off_magnets=True):
        """Stop the injection"""
        print('  Set magnets STANDBY')
        self.magnets.command_inout('Standby')  # danger
        if off_magnets:
            time.sleep(_SEPTUM_DELAY)
            print('  Set magnets OFF')
            self.magnets.command_inout('Off')  # danger
        wait_for(partial(all_notmoving, self.magnets), timeout=8)

    def query(self):
        """Check the injection state. Return value:
            DevState.ON :
            DevState.STANDBY
            DevState.OFF
            DevState.UNKNOWN : incoherent state
        """
        states = self.magnets.state()
        print(states)
        state = self.summarize.get(states, DevState.UNKNOWN)
        if state == DevState.UNKNOWN:
            state = max(states)
        return state

    def getstatus(self):
        states = self.magnets.state()
        names = self.magnets.get_device_list()
        state = self.summarize.get(states, DevState.UNKNOWN)
        if state == DevState.UNKNOWN:
            state = max(states)
            statuses = self.magnets.status(forward=True)
            detail = ''.join(['{0:>16}: {1} : {2}\n'.format(n, s, st) for n, s, st in zip(names, states,statuses)])
        else:
            detail = ''
        status = '{0}SY Injection is {1}'.format(detail, state)
        return state, status


if __name__ == "__main__":
    # import argparse
    import optparse

    parser = optparse.OptionParser(usage="%prog [options] ON|OFF|QUERY",
                                   version="%prog 1.0", description="Sets the Booster injection ON or OFF")
    parser.add_option('-s', '--standby', action="store_false", default=True, dest='off_magnets',
                      help='Keep magnets in STANDBY')
    options, args = parser.parse_args()
    if len(args) != 1:
        parser.error("incorrect number of arguments")
    action = args[0].upper()
    if action == 'ON':
        SYInj().on()
        print('Injection ON')
        code = 0
    elif action == 'OFF':
        SYInj().off(options.off_magnets)
        print('Injection OFF')
        code = 0
    elif action == 'QUERY':
        code = SYInj.returncode[SYInj().query()]
    else:
        code = 1
    sys.exit(code)
