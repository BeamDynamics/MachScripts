#!/usr/bin/env python
"""Fills the SR in hybrid mode

Usage: fillhybrid.py [options] total_current sb_current

Options:
  --version             show program's version number and exit
  -h, --help            show this help message and exit
  -t TIMEOUT, --timeout=TIMEOUT
                        time-out for refill [s]
  -n BUCKET_NUMBER, --bucket_number=BUCKET_NUMBER
                        Position of the single bunch [default: 0]
  -v, --verbose         Verbose mode (default: False)
"""

from __future__ import print_function, division     # for python2
import sys
# noinspection PyUnresolvedReferences
from srfill import SRFill, __author__, __version__
import srinj
from pymach.tango import Device, DevFailed
from pymach.timing import XThread, TimeoutError


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Fills the SR in hybrid mode")
    parser.add_argument('-v', '--version', action='version', version=__version__)
    parser.add_argument('total_current', type=float, help='Total current [mA]')
    parser.add_argument('sb_current', type=float, nargs='?', default=0,
                        help='Single bunch current [mA] (default: %(default)s)')
    parser.add_argument('-t', '--timeout', type=float, default=0,
                        help='time-out for refill [s] (default: automatically computed)')
    parser.add_argument('-n', '--bucket-number', type=int, default=0,
                        help='Position of the single bunch (default: %(default)s)')
    parser.add_argument('--verbose', action='store_true', default=False,
                        help='Verbose mode')
    parser.add_argument('-s', '--fill_single', action='store_true',
                        help='Fill in single pulse based on buch current measurements')
    arguments = parser.parse_args()

    print('Total current: {0}, single bunch current: {1}'.format(arguments.total_current, arguments.sb_current))
    srf = SRFill()
    keywords = {}
    keywords.update(sb_current=arguments.sb_current, sb_position=arguments.bucket_number, timeout=arguments.timeout)
    if arguments.verbose:
        keywords['info'] = sys.stdout
    if not arguments.fill_single:
        th = XThread(name='Refill', target=srf, args=('hybrid', arguments.total_current,), kwargs=keywords)
    else:
        th = XThread(name='Refill', target=srf, args=('topup', arguments.total_current,), kwargs=keywords)
    print()
    try:
        th.start()
        th.join_poll(period=1, fail=True)
    except (KeyboardInterrupt, TimeoutError) as err:
        print('Refill failed: {0}'.format(err), file=sys.stderr)
    finally:
        srinj.SRInj().off()
        if Device('sy/ps-rips-switch/src').Source == 2:
            try:
                Device('sy/ps-rips/manager').StopRamping()
                print('RIPS ramp stopped')
            except DevFailed:
                print("Command StopRamping failed", file=sys.stderr)
